dashboard = angular.module('siva', ['siva.directives','siva.services', 'ui.event','pasvaz.bindonce'])

dashboard.config(['$routeProvider', ($routeProvider) ->
	$routeProvider
	.when( '/variations/new', {"templateUrl":"/template/dashboard/variation-new.html", "controller":'VariationsCtrl',"pageClass":"dashboard", "title":"Variaciones"})
	.when( '/corpses', {"templateUrl":"/template/dashboard/corpses.html", "controller":"CorpsesCtrl","pageClass":"dashboard", "title":"Cadáveres", reloadOnSearch:false})
	.when( '/founds', {"templateUrl":"/template/dashboard/founds.html", "controller":'FoundsCtrl',"pageClass":"dashboard", "title":"Variaciones reportadas", reloadOnSearch:false})
	.when( '/profile', {"templateUrl":"/template/dashboard/user-edit.html", "controller":'UserCtrl',"pageClass":"", "title":"Mi cuenta"})
	.when( '/reports', {"templateUrl":"/template/dashboard/reports.html", "controller":'ReportsCtrl',"pageClass":"dashboard", "title":"Reportes"})
	.when( '/', {"templateUrl":"/template/home.html","pageClass":"dashboard", "title":"Reportes"})
]);
dashboard.run ['$rootScope','$location', '$http', fn = ($rootScope,$location, $http)->
	$rootScope.location = $location
]
dashboard.controller 'CorpsesCtrl', [ '$scope', 'Corpse', '$filter', '$routeParams','$location', '$http', '$window', fn = ($scope, Corpse, $filter, $routeParams, $location, $http, $window)->
	$scope.$watch ()->
		$location.search().area
	, (value)->
		$scope.area = value
		if value is 'edit' or value is 'new'
			$http.get('/ethnicity').success (data)->
				$scope.ethnicities = data

	$scope.$watch ()->
		$location.search().corpse
	, (corpse_id)->
		if corpse_id
			$scope.corpse_id = corpse_id
			Corpse.get id:$scope.corpse_id, (success)->
				if success.death_at
					t = success.death_at.split(/[- :]/);
					success.death_at = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
				$scope.corpse = success
				$scope.originalCorpse = angular.copy $scope.corpse
		else
			$scope.corpse_id = false
			$scope.corpse = {}

	getPage = (pageNumber = 1)->
		params = {}
		$scope.loadingList = true
		if pageNumber
			params.page = pageNumber
		else
			delete params.page
		if $scope.term
			params.search = $scope.term
		else
			delete params.search
		if $scope.order
			params.order = $scope.order
		else
			delete params.order
		Corpse.query params, (success)->
			$scope.loadingList = false
			for corpse in success.results
				corpse.fullname = corpse.name + ' ' + corpse.lastname
			$scope.corpses = success.results
			$scope.pagination = success
	getPage()
	$scope.$on 'paginate', (e, page)->
		getPage(page)

	$scope.options = Corpse.query {report:true}
	$scope.search = ()->
		console.dir $scope
		getPage()

	$scope.update = (corpse)->
		if $scope.area is 'edit'
			Corpse.update {id:$scope.corpse_id}, $scope.corpse, (success)->
				$scope.$emit 'notification', {t: 'success', m: 'Datos actualizados.'}
				$location.search('area','detail')
			, (error)->
				if error.status is 400
					$scope.errors = error.data
				else
					$scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}
	$scope.create = (corpse)->
		$scope.corpse.death_at = $scope.corpse.death_at
		Corpse.save $scope.corpse, (success)->
			$scope.$emit 'notification', {t: 'success', m: 'El cadaver ha sido registrado.'}
			success.fullname = success.name + ' ' + success.lastname
			$scope.corpses.splice $scope.corpses.length,0, success
			$location.search().area = 'detail'
			$location.search().corpse = success.id
		, (error)->
			if error.status is 400
				$scope.errors = error.data
				console.dir error.data
			else
				$scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}
	$scope.delete = (corpse, index)->
		confirm = $window.confirm '¿Está seguro que desea eliminar el cadáver?'
		if confirm
			Corpse.delete {id:corpse.id}, (response)->
				$scope.corpses.splice(index, 1)
				$location.search('corpse', null)
				$location.search('area', null)
				$scope.$emit 'notification', {t: 'neutral', m: 'El cadaver ha sido eliminado.'}
			,(error)->
				$scope.$emit 'notification', {t: 'error', m: error.data.message}

	$scope.togglePicker = ()->
		$scope.datePicker = !$scope.datePicker

	$scope.cancel = ()->
		if $location.search().area is 'edit'
			$location.search 'area', 'detail'
			if $scope.corpse
				$scope.corpse = angular.copy $scope.originalCorpse
		else
			$location.search 'area', null
]

dashboard.controller 'FoundsCtrl', ['$scope', '$location', 'Found','Variation','$http', 'Structure','Corpse', 'Memory', 'Label', '$window', fn = ($scope, $location, Found, Variation, $http, Structure, Corpse, Memory, Label, $window)->
	$scope.regions = ['Cabeza','Abdomen','Cuello','Dorso','Miembro inferior','Miembro superior','Pelvis', 'Torax']
	$scope.fields = [{value:'diameter',label:'Diámetro'},{value:'perimeter', label:'Perímetro'}]
	params = {}
	
	$scope.filters = [
		{value:'diameter',label:'Diámetro'},
		{value:'perimeter', label:'Perímetro'},
		{value:'width', label:'Ancho'},
		{value:'length', label:'Longitud'}
		{value:'form', label:'Forma'}
		{value:'thickness', label:'Grosor'}]

	$scope.showOptions = [
		{value:'all',label:'Todas'},
		{value:'mine', label:'Propias'},
		{value:'published', label:'Publicadas'}]

	$scope.filterSelected = [];
	$scope.toggleSelection = (filterName)->
		idx = $scope.filterSelected.indexOf(filterName);
		# is currently selected
		if (idx > -1)
			$scope.filterSelected.splice(idx, 1);
		# is newly selected
		else
			$scope.filterSelected.push(filterName);

		$scope.search()

	getPage = (pageNumber = 1)->
		params = {}
		$scope.loadingList = true
		if pageNumber
			params.page = pageNumber
		else
			delete params.page
		if $scope.term
			params.search = $scope.term
		else
			delete params.search
		if $scope.show
			params.show = $scope.show
		else
			delete params.show
		if $scope.order
			params.order = $scope.order
		else
			delete params.order
		if $scope.filterSelected.length
			params.filter = $scope.filterSelected
		else
			delete params.filter
		Found.query params, (success)->
			$scope.loadingList = false
			$scope.founds = success.results
			$scope.pagination = success


	getPage()

	$scope.$on 'paginate', (e, page)->
		getPage(page)

	$scope.search = ()->
		getPage()

	$scope.removeFilters = ()->
		$scope.term = null
		$scope.filterSelected = []
		return getPage()
	$scope.filterActive = (value)->
		$scope.filterSelected.indexOf(value) > -1

	$scope.publish = ()->
		$scope.found.is_pending = true
		Found.update $scope.found , (success)->
			$scope.$emit 'notification', {m:'La variación quedará pendiente de la aprobación de un administrador.', t:'success'}
		, (error)->
			$scope.$emit 'notification', {m:'No se pudo publicar, intentelo más tarde.', t:'error'}
	# Cargar la variación, si es que se muestran detalles o se edita
	getVariationName = ()->
		for variation in $scope.variations
			if variation.id is $scope.found.variation_id
				$scope.found.variation_obj = variation

	updateStructures = ()->
		$scope.$watch 'found.body_region'
		, (region)->
			$http.get('/rdf/'+region).success (data)->
				$scope.structures = data

	# Observar qué página se va a mostrar
	$scope.$watch ()->
		$location.search().area
	, (value)->
		$scope.area = value
		if value is 'edit' or value is 'new'
			updateStructures()
			Variation.query null, (success)->
				$scope.variations = success
				variations = 1
		if value is 'new'
			$scope.field = 'diameter'

	# Cargar el cadaver, si es que reporta una variación nueva
	$scope.$watch ()->
		$location.search().corpse
	, (corpse_id)->
		if corpse_id
			Corpse.get id:corpse_id, (success)->
				$scope.corpse = success

	# Cargar la variación, si es que se muestran detalles o se edita
	$scope.$watch ()->
		$location.search().found
	, (found_id)->
		if found_id
			$scope.found_id = found_id
			$scope.loadingFound = true
			Found.get id:found_id, (success)->
				$scope.$broadcast 'uploader', 'enable'
				$scope.loadingFound = false
				Memory.found = success.found
				Memory.found.owner = success.owner
				Memory.found.structure_name = Memory.found.structure.name
				Memory.found.death_at = new Date(Memory.found.death_at)
				$scope.found = Memory.found
				$scope.related = success.related
				if $scope.variations
						getVariationName()
						$scope.originalFound = angular.copy $scope.found
				else
					$scope.$watch 'variations'
					, (value)->
						if value
							getVariationName()
							$scope.originalFound = angular.copy $scope.found
				if parseInt($scope.found.diameter) > 0
					$scope.field = 'diameter'
				else
					$scope.field = 'perimeter'
		else
			$scope.found_id = false
			$scope.found = {}


	$scope.update = ()->
		$scope.found.variation_id = $scope.found.variation_obj.id
		Found.update {id:$scope.found_id}, $scope.found, (success)->
			getVariationName()
			$scope.$emit 'notification', {t: 'success', m: 'Has actualizado la variación correctamente.'}
			$location.search('area','detail')
			$location.search('found',success.id)
		, (error)->
			if error.status is 400
				$scope.errors = error.data
			else
				$scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}
	$scope.create = ()->
		$scope.found.corpse_id = $location.search().corpse
		$scope.found.variation_id = $scope.found.variation_obj.id
		Found.save $scope.found, (success)->
			$scope.$emit 'notification', {t: 'success', m: 'La variación ha sido registrada.'}
			$location.search 'area', 'detail'
			$location.search 'found', success.id
		, (error)->
			if error.status is 400
				$scope.errors = error.data
				console.dir error.data
			else
				$scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}

	$scope.delete = (found, index)->
		confirm = $window.confirm '¿Está seguro que desea eliminar el reporte?'
		if confirm
			Found.delete {id:found.id}, (response)->
				$scope.founds.splice(index, 1)
				$location.search('found', null)
				$location.search('area', null)
				$scope.$emit 'notification', {t:'neutral', m:'El reporte ha sido borrado.'}
			,(error)->
				$scope.$emit 'notification', {t:'error', m:error.data.message}

	$scope.cancel = ()->
		if $scope.found
			$scope.found = angular.copy $scope.originalFound
		if $location.search().area is 'new'
			$location.path('/corpses').search('area','detail')
		else
			$location.search('area','detail')
		
	$scope.openEditor = (image)->
		$scope.editImage = true
		$scope.image = image
		Label.query {image:image.id}, (labels)->
			$scope.tags = labels
	
	$scope.tags = []
	$scope.$on 'update-tag', (event, tag)->
		tag.$update()
	$scope.$on 'create-tag', (event, tag)->
		tag.image_id = $scope.image.id
		tag.description = 'Descripción'
		$scope.tags.push Label.save(tag)
	$scope.closeEditor = ()->
		$scope.editImage = false
]

dashboard.controller 'VariationsCtrl', ['$scope', '$routeParams', '$location', 'Variation',fn = ($scope, $routeParams, $location, Variation)->
	if $routeParams.found_id
		Variation.get id:$routeParams.found_id, (success)->
			$scope.variation = success
		$scope.save = (variation)->
			Variation.update {id:$routeParams.variation_id}, $scope.variation, (success)->
				$scope.$emit 'notification', {t: 'success', m: '<b>Exito</b>: Datos actualizados.'}
				$location.path('/variaciones/'+success.id)
			, (error)->
				if error.status is 400
					$scope.errors = error.data
				else
					$scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}
	else
		Variation.query null , (success)->
			$scope.variations = success
		$scope.variation = {}
		$scope.save = (variation)->
			Variation.save $scope.variation, (success)->
				$scope.variation = success
				$scope.$emit 'notification', {t: 'success', m: 'El nombre de variación ha sido registrado.'}
				$location.path('/founds').search('area','new')
			, (error)->
				if error.status is 400
					$scope.errors = error.data
				else
					$scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}
]

dashboard.controller 'ImagesCtrl', ['$scope', '$http', '$location','formDataObject','Memory','$window', fn = ($scope, $http, $location, formDataObject, Memory,$window)->
	found_id = ''
	sendFile = (file) ->
		found_id = $location.search().found
		# Iniciar mensaje de cargando
		$scope.$broadcast 'uploader', 'disable'
		$http
			method: 'POST'
			url: '/image'
			data: {
				file: file
				variationfound_id: found_id
			}
			headers: {'Content-Type': undefined}
			transformRequest: formDataObject
		.success (data)->
			$scope.$broadcast 'uploader', 'enable'
			$scope.$emit 'notification', {t:'success', m:'Se ha subido una imagen.'}
			$scope.disable = false
			if !Memory.found.images.length
				Memory.found.images = []
			Memory.found.images.push data
		.error (error)->
			$scope.$broadcast 'uploader', 'enable'
			if error.message
				$scope.$emit 'notification', {t:'error', m:error.message}
			else
				$scope.$emit 'notification', {t:'error', m:'No se pudo subir la imagen'}

	$scope.handleFiles = (files) ->
		if $location.search().found
			$scope.disable = true
			for file in files
				mimeType = /image.*/
				if not file.type.match(mimeType)
					continue
				sendFile file
	$scope.remove = (image, index)->
		confirm = $window.confirm '¿Está seguro que desea eliminar la imagen?'
		if confirm
			$http.delete('/image/'+image.id)
			.success (success)->
				Memory.found.images.splice index, 1
				$scope.$emit 'notification', {t:'neutral', m:'Imagen eliminada'}
			.error (error)->
				$scope.$emit 'notification', {t:'neutral', m:error.data.error}
			
]
dashboard.controller 'UserCtrl', fn = ($scope, $http, User, Sesion)->
	$scope.disabled = true
	$http.get('/location').success (success)->
		$scope.locations = success
		checkLocation()
	$http.get('/area').success (success)->
		$scope.areas = success
	checkLocation = ()->
		if $scope.user && $scope.locations
			$scope.user.location = getLocation()
	getLocation = ()->
		for loc in $scope.locations
			if loc.id is $scope.user.location_id
				return loc
	$scope.update = ()->
		location = $scope.user.location.id
		$scope.user.location_id = location
		usuario = $scope.user
		User.update id: $scope.user.id, usuario, (success)->
			$scope.location.path('/')
			$scope.$emit 'notification', {t:'success', m:'Perfil actualizado correctamente.'}
		, (error)->
			$scope.$emit 'notification', {t:'error', m:error.data.message}
	$scope.cancel = ()->
		$scope.location.path('/')			

dashboard.controller 'ReportsCtrl' , fn = ($scope, $http)->
  console.dir $scope
  $scope.query = {}
  $http.get('/found', {params: {report:true}}).success (success)->
    $scope.variations = success.variation_names
    $scope.structures = success.structures
    $scope.body_regions = success.body_regions
  $http.get('/corpse',{params:{report:true}}).success (success)->
    ages = []
    for age in success.age_ranges
      ages.push age.age_range
    $scope.age_ranges = ages
    $scope.cities = success.locations
    success.regions.splice 0, 0, {region:'Todas'}
    regiones = []
    for region in success.regions
      regiones.push region.region
    $scope.regions = regiones

  $scope.options =
    geographic:[
      {value:'city',label:'Ciudad'},
      {value:'region',label:'Región'}
    ]
    anatomic:[
      {value:'variation',label:'Variación'},
      {value:'region',label:'Región corporal'}
      {value:'structure',label:'Estructura corporal'},
    ]
  $scope.clearGeographicFilter = ()->
    delete $scope.query.city
    delete $scope.query.natural_region
    if $scope.geographic is 'region'
      $scope.query.natural_region = 'Todas'
  $scope.clearAnatomicFilter = ()->
    delete $scope.query.variation
    delete $scope.query.body_region
    delete $scope.query.structure

  $scope.anatomic = 'variation'
  $scope.geographic = 'city'
  $scope.generate = ()->
    for op in $scope.query
      if op is false or op is ''
        op = undefined
    $http.get('/report?',{params:$scope.query}).success (success)->
      $scope.report = success
    .error (error)->
      $scope.$emit 'notify', {m: error.message, t:'error'} 
      $scope.repot = null
      $scope.error = error.message