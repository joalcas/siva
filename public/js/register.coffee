# Controlador principal
app = angular.module('siva', ['siva.directives','siva.services'])
app.config(['$routeProvider', ($routeProvider) ->
  $routeProvider
  .when( '/', {"template":"/template/dashboard/user-form.html","controller":'CuentaCtrl', "pageClass":"account-create", "title":"Crear cuenta"})
  # .when( '/solicitud-enviada', {"templateUrl":"template/cuenta/solicitud-enviada.html", "pageClass":"account-create", "title":"Solicitud enviada"})
]);
app.run ['$rootScope','$location', fn = ($rootScope,$location)->
  $rootScope.location = $location
]
app.controller 'CuentaCtrl', fn = ($scope, User, $location, $http ) ->
  console.log 'cuenta'
  $scope.opciones = 
    typeid:
      [ {id:1, nombre: 'Cédula de ciudadanía'}
        ,{id:2, nombre: 'Cédula de extranjería'}
        ,{id:3, nombre: 'Tarjeta de Identidad'}
        ,{id:4, nombre: 'Pasaporte'}
        ,{id:5, nombre: 'NIT'}]
    profiles:
        [{id:1, nombre: 'Administrador'}
        ,{id:2, nombre: 'Investigador'}
        ,{id:3, nombre: 'Público'}]

  $http.get('/location').success (data)->
    $scope.locations = data
  
  getAreas = ()->
    $http.get('/area').success (data)->
      $scope.areas = data
      $scope.areas.splice 0, 0, {id:0, name:'Otro area'}
  
  getAreas()

  $scope.create = ()->
    $scope.user.location_id = $scope.user.location.id
    User.save $scope.user ,(success)->
      $location.search('sent', true)
    , (error)->
      $scope.error = error.data
      console.dir $scope.error