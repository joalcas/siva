servicios = angular.module('siva.services', ['ngResource'])
servicios.factory 'formDataObject', ()->
	(data)->
		fd = new FormData()
		angular.forEach data, (value, key)->
			fd.append key, value
		return fd

servicios.factory 'Memory', ()->
	return @

servicios.factory 'User',($resource)->
	$resource '/user/:id', id:"@id", {
		update:{method:'PUT'}
		get:{ method:'GET', cache:true }
		query:{method:'GET', isArray:false}
	}
servicios.factory 'Corpse',($resource)->
	$resource '/corpse/:id', id:"@id", {
		update:{method:'PUT'}
		get:{method:'GET', cache:true}
		query:{method:'GET', isArray:false}
	}

servicios.factory 'Sesion',($resource)->
	$resource '/sesion/:id', id:"@id"

servicios.factory 'Variation',($resource)->
	$resource '/variation/:id', id:"@id", {
		update:{method:'PUT'}
		get:{method:'GET', cache:true}

	}

servicios.factory 'Found',($resource)->
	$resource '/found/:id', id:"@id", {
		update:{method:'PUT'}
		get:{method:'GET', cache:true}
		query:{method:'GET', isArray:false}
	}

servicios.factory 'Structure',($resource)->
	$resource '/structure/:id', id:"@id", {
		update:{method:'PUT'}
		get:{method:'GET', cache:true}
	}

servicios.factory 'Label',($resource)->
	$resource '/label/:id', id:"@id", {
		update:{method:'PUT'}
		get:{method:'GET', cache:true}
	}

servicios.factory 'Location', ($resource)->
	$resource '/location/:id', id:"@id"

servicios.factory 'Rdf', ($resource)->
	$resource '/rdf/:area'