# Controlador principal
app = angular.module('siva', ['siva.directives','siva.services','pasvaz.bindonce'])
app.config(['$routeProvider', ($routeProvider) ->
  $routeProvider
  .when( '/registrarme', {"template":"","controller":'CuentaCtrl', "pageClass":"account-create", "title":"Crear cuenta"})
  .when( '/founds', {"templateUrl":"/template/dashboard/founds.html", "controller":'FoundsCtrl',"pageClass":"dashboard", "title":"Variaciones reportadas", reloadOnSearch:false})
  .when( '/reports', {"templateUrl":"/template/dashboard/reports.html", "controller":'ReportsCtrl',"pageClass":"dashboard", "title":"Reportes"})
  .when( '/', {"templateUrl":"/template/home.html","pageClass":"dashboard", "title":"Reportes"})
]);

app.run ['$rootScope','$location', fn = ($rootScope,$location)->
  $rootScope.location = $location
]
app.controller 'FoundsCtrl', ['$scope', '$location', 'Found','Variation','$http', 'Structure','Corpse', 'Memory', 'Label', '$window', fn = ($scope, $location, Found, Variation, $http, Structure, Corpse, Memory, Label, $window)->
  $scope.regions = ['Cabeza','Abdomen']
  $scope.fields = [{value:'diameter',label:'Diámetro'},{value:'perimeter', label:'Perímetro'}]
  params = {}
  
  $scope.filters = [
    {value:'diameter',label:'Diámetro'},
    {value:'perimeter', label:'Perímetro'},
    {value:'width', label:'Ancho'},
    {value:'length', label:'Longitud'}
    {value:'form', label:'Forma'}
    {value:'thickness', label:'Grosor'}]

  $scope.showOptions = [
    {value:'all',label:'Todas'},
    {value:'published', label:'Publicas'}]

  $scope.filterSelected = [];
  $scope.toggleSelection = (filterName)->
    idx = $scope.filterSelected.indexOf(filterName);
    # is currently selected
    if (idx > -1)
      $scope.filterSelected.splice(idx, 1);
    # is newly selected
    else
      $scope.filterSelected.push(filterName);

    $scope.search()

  getPage = (pageNumber = 1)->
    params = {}
    $scope.loadingList = true
    if pageNumber
      params.page = pageNumber
    else
      delete params.page
    if $scope.term
      params.search = $scope.term
    else
      delete params.search
    if $scope.show
      params.show = $scope.show
    else
      delete params.show
    if $scope.order
      params.order = $scope.order
    else
      delete params.order
    if $scope.filterSelected.length
      params.filter = $scope.filterSelected
    else
      delete params.filter
    Found.query params, (success)->
      $scope.loadingList = false
      $scope.founds = success.results
      $scope.pagination = success

  getPage()

  $scope.$on 'paginate', (e, page)->
    getPage(page)

  $scope.search = ()->
    getPage()

  $scope.removeFilters = ()->
    $scope.term = null
    $scope.filterSelected = []
    return getPage()
  $scope.filterActive = (value)->
    $scope.filterSelected.indexOf(value) > -1

  # Cargar la variación, si es que se muestran detalles o se edita
  getVariationName = ()->
    for variation in $scope.variations
      if variation.id is $scope.found.variation_id
        $scope.found.variation_obj = variation

  updateStructures = ()->
    $scope.$watch 'found.body_region'
    , (region)->
      $http.get('/rdf/'+region).success (data)->
        $scope.structures = data

  # Observar qué página se va a mostrar
  $scope.$watch ()->
    $location.search().area
  , (value)->
    $scope.area = value
    if value is 'edit' or value is 'new'
      updateStructures()
      Variation.query null, (success)->
        $scope.variations = success
        variations = 1
    if value is 'new'
      $scope.field = 'diameter'

  # Cargar el cadaver, si es que reporta una variación nueva
  $scope.$watch ()->
    $location.search().corpse
  , (corpse_id)->
    if corpse_id
      Corpse.get id:corpse_id, (success)->
        $scope.corpse = success

  # Cargar la variación, si es que se muestran detalles o se edita
  $scope.$watch ()->
    $location.search().found
  , (found_id)->
    if found_id
      $scope.found_id = found_id
      $scope.loadingFound = true
      Found.get id:found_id, (success)->
        $scope.$broadcast 'uploader', 'enable'
        $scope.loadingFound = false
        Memory.found = success.found
        Memory.found.owner = success.owner
        Memory.found.structure_name = Memory.found.structure.name
        Memory.found.death_at = new Date(Memory.found.death_at)
        $scope.found = Memory.found
        $scope.related = success.related
        if $scope.variations
            getVariationName()
            $scope.originalFound = angular.copy $scope.found
        else
          $scope.$watch 'variations'
          , (value)->
            if value
              getVariationName()
              $scope.originalFound = angular.copy $scope.found
        if parseInt($scope.found.diameter) > 0
          $scope.field = 'diameter'
        else
          $scope.field = 'perimeter'
    else
      $scope.found_id = false
      $scope.found = {}


  $scope.update = ()->
    $scope.found.variation_id = $scope.found.variation_obj.id
    Found.update {id:$scope.found_id}, $scope.found, (success)->
      getVariationName()
      $scope.$emit 'notification', {t: 'success', m: 'Has actualizado la variación correctamente.'}
      $location.search('area','detail')
      $location.search('found',success.id)
    , (error)->
      if error.status is 400
        $scope.errors = error.data
      else
        $scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}
  $scope.create = ()->
    $scope.found.corpse_id = $location.search().corpse
    $scope.found.variation_id = $scope.found.variation_obj.id
    Found.save $scope.found, (success)->
      $scope.$emit 'notification', {t: 'success', m: 'La variación ha sido registrada.'}
      $location.search 'area', 'detail'
      $location.search 'found', success.id
    , (error)->
      if error.status is 400
        $scope.errors = error.data
        console.dir error.data
      else
        $scope.$emit 'notification', {t: 'error', m: 'Error al procesar su solicitud'}

  $scope.delete = (found, index)->
    confirm = $window.confirm '¿Está seguro que desea eliminar el reporte?'
    if confirm
      Found.delete {id:found.id}, (response)->
        $scope.founds.splice(index, 1)
        $location.search('found', null)
        $location.search('area', null)
        $scope.$emit 'notification', {t:'neutral', m:'El reporte ha sido borrado.'}
      ,(error)->
        $scope.$emit 'notification', {t:'error', m:error.data.message}

  $scope.cancel = ()->
    if $scope.found
      $scope.found = angular.copy $scope.originalFound
    if $location.search().area is 'new'
      $location.path('/corpses').search('area','detail')
    else
      $location.search('area','detail')
    
  $scope.openEditor = (image)->
    $scope.editImage = true
    $scope.image = image
    Label.query {image:image.id}, (labels)->
      $scope.tags = labels
  
  $scope.tags = []
  $scope.$on 'update-tag', (event, tag)->
    tag.$update()
  $scope.$on 'create-tag', (event, tag)->
    tag.image_id = $scope.image.id
    tag.description = 'Descripción'
    $scope.tags.push Label.save(tag)
  $scope.closeEditor = ()->
    $scope.editImage = false
]
app.controller 'ImagesCtrl', ['$scope', '$http', '$location','formDataObject','Memory', fn = ($scope, $http, $location, formDataObject, Memory)->
  found_id = ''
  sendFile = (file) ->
    found_id = $location.search().found
    # Iniciar mensaje de cargando
    $http
      method: 'POST'
      url: '/image'
      data: {
        file: file
        variationfound_id: found_id
      }
      headers: {'Content-Type': undefined}
      transformRequest: formDataObject
    .success (data)->
      $scope.disable = false
      Memory.found.images.push data
  $scope.handleFiles = (files) ->
    if $location.search().found
      $scope.disable = true
      for file in files
        mimeType = /image.*/
        if not file.type.match(mimeType)
          continue
        sendFile file
  $scope.remove = (image, index)->
    $http.delete('/image/'+image.id)
    .success (success)->
      Memory.found.images.splice index, 1
      $scope.$emit 'notification', {t:'neutral', m:'Imagen eliminada'}
]
app.controller 'ReportsCtrl', ['$scope', '$http', fn = ($scope, $http)->
  console.dir $scope
  $scope.query = {}
  $http.get('/found', {params: {report:true}}).success (success)->
    $scope.variations = success.variation_names
    $scope.structures = success.structures
    $scope.body_regions = success.body_regions
  $http.get('/corpse',{params:{report:true}}).success (success)->
    ages = []
    for age in success.age_ranges
      ages.push age.age_range
    $scope.age_ranges = ages
    $scope.cities = success.locations
    success.regions.splice 0, 0,{region:'Todas'}
    regiones = []
    for region in success.regions
      regiones.push region.region
    $scope.regions = regiones

  $scope.options =
    geographic:[
      {value:'city',label:'Ciudad'},
      {value:'region',label:'Región'}
    ]
    anatomic:[
      {value:'variation',label:'Variación'},
      {value:'region',label:'Región corporal'}
      {value:'structure',label:'Estructura corporal'},
    ]
  $scope.clearGeographicFilter = ()->
    delete $scope.query.city
    delete $scope.query.natural_region
    if $scope.geographic is 'region'
      $scope.query.natural_region = 'Todas'
  $scope.clearAnatomicFilter = ()->
    delete $scope.query.variation
    delete $scope.query.body_region
    delete $scope.query.structure

  $scope.anatomic = 'variation'
  $scope.geographic = 'city'
  $scope.generate = ()->
    for op in $scope.query
      if op is false or op is ''
        op = undefined
    $http.get('/report?',{params:$scope.query}).success (success)->
      $scope.report = success
    .error (error)->
      $scope.$emit 'notify', {m: error.message, t:'error'} 
      $scope.repot = null
      $scope.error = error.message
]