sivaui = angular.module 'siva.directives', ['ui.bootstrap']
sivaui.directive 'pageClass', ($route, $timeout)->
	restrict: 'A'
	link:(scope, element, attributes)->
		$timeout ()->
			scope.$watch ()->
				$route.current
			, (current)->
				element.removeClass()
				if current.$$route
					if current.$$route.pageClass
						element.addClass current.$$route.pageClass
		, 0 , false
sivaui.directive 'pageTitle', ($route, $timeout)->
	restrict: 'A'
	link:(scope, element, attributes)->
		$timeout ()->
			scope.$watch ()->
				$route.current
			, (current)->
				element.html ''
				if current.$$route
					if current.$$route.title
						element.html 'Siva: ' + current.$$route.title
		, 0, false

sivaui.filter 'genre',  () ->
	(text) ->
		if text
			if text.toLowerCase() is 'm'
				return 'Masculino'
			else
				return 'Femenino'
sivaui.filter 'laterality',  () ->
	(text) ->
		if text
			if text.toLowerCase() is 'i'
				return 'Izquierda'
			else
				return 'Derecha'
sivaui.filter 'pre',  () ->
	(text) ->
		if text
			element = text.replace /[\r\n]/g, '<br/>'
			return element

sivaui.filter 'truncate', ()->
	(text, length, end)->
		if (isNaN(length))
			length = 10
		if (end is undefined)
			end = "..."
		if (text.length <= length || text.length - end.length <= length)
			text
		else
			return String(text).substring(0, length-end.length) + end

sivaui.filter 'properties',  () ->
	(listado, propiedades) ->
		if propiedades
			opciones = []
			nuevo = []
			angular.forEach propiedades, (value, key)->
				if value is true
					opciones.push key
			for item in listado
				for opt in opciones
					item_op = item[opt]
					if item_op is "0" or item_op is "N/A"
						item = false
				if item
					nuevo.push item
			return nuevo
		return listado

sivaui.filter 'filterUser',  () ->
	(listado, argumento) ->
		nuevo = []
		if argumento
			switch argumento
				when 'new'
					# ...
					for user in listado
						if user.is_new is '1' and user.deleted_at == null
							nuevo.push user

				when 'active'
					for user in listado
						if user.is_new != '1' and user.deleted_at == null
							nuevo.push user

				when 'deleted'
					for user in listado
						if user.deleted_at != null
							nuevo.push user
			console.dir nuevo
			return nuevo
		else
			return listado

sivaui.filter 'findcorpse',  () ->
	(listado, argumento) ->
		if argumento
			argumento = argumento.toLowerCase()
			nuevo = []
			for corpse in listado
				certificate = corpse.num_death_certificate.toLowerCase()
				fullname = corpse.fullname.toLowerCase()
				if certificate.search(argumento) != -1 or fullname.search(argumento) != -1
					nuevo.push corpse
			return nuevo
		return listado

sivaui.filter 'findvariation',  () ->
	(listado, argumento) ->
		if argumento
			argumento = argumento.toLowerCase()
			nuevo = []
			for variation in listado
				certificate = variation.corpse.num_death_certificate.toLowerCase()
				corpsename = (variation.corpse.name + ' ' + variation.corpse.lastname).toLowerCase()
				name = variation.variation.name.toLowerCase()
				if certificate.search(argumento) != -1 or name.search(argumento) != -1 or corpsename.search(argumento) != -1
					nuevo.push variation
			return nuevo
		return listado

sivaui.filter 'startFrom', ()->
	return (input, start)->
		start = +start; #parse to int
		return input.slice(start);

sivaui.filter 'svDate', ($filter)->
	standardDateFilterFn = $filter('date');
	format = (dateToFormat)->
		t = dateToFormat.split(/[- :]/);
		d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
		return standardDateFilterFn(d, 'MMM d, y');
	return format

sivaui.directive 'fullscreenBox', ($compile, $timeout) ->
	restrict: 'C'
	link: (scope, element, attributes)->
		message = {}
		container = angular.element('<div class="container"></div>')
		format = (container)->
			element.append container
			if message.close is true
				closeButton = '<a href="" title="Cerrar" ng-click="functions.hide()" class="close"></a>'
				container.append $compile(closeButton)(scope);
			element.append container
			element.addClass 'show'

		functions =
			show: ()->
				if message.scope
					container.append $compile(message.content)(message.scope)
				else
					container.append message.content
				format container
			spinner: ()->
				content = """
					<div class="spinner">
						#{message.content}
						<span class="ball first"></span>
						<span class="ball"></span>
						<span class="ball last"></span>
					</div>
				"""
				container.append content
				format container
			hide: ()->
				element.removeClass 'show'
				element.html ''
		scope.$on 'fullscreen-box', (event, message)->
			message = message
			functions.hide()
			fn = functions[message.action]
			fn()

sivaui.directive 'contenteditable', ()->
	require: 'ngModel'
	link: (scope, elm, attrs, ctrl)->
		elm.bind 'blur', ()->
			scope.$apply ()->
				ctrl.$setViewValue elm.html()
		ctrl.$render = (value)->
			elm.html value
		ctrl.$setViewValue elm.html()

sivaui.directive 'photoTag', ($compile, $timeout)->
	restrict: 'A'
	link: (scope, element, attrs)->
		scope.editarLabel = true
		$timeout ()->
			tagElm = {}
			tag = {}
			imageContainer = angular.element '<div class="photo-container"></div>'
			labelsBox = """
			<div class="labels-container" ng-show="!editarLabel">
				<div class="label" ng-repeat="tag in tags" style="top:{{tag.top}}px;left:{{tag.left}}px;width:{{tag.width}}px;height:{{tag.height}}px;">
					<div class="text">{{tag.description}}</div>
				</div>
			</div>
			"""
			labelsContainer = angular.element $compile(labelsBox)(scope)
			overlay = angular.element $compile('<div class="overlay" ng-show="editarLabel && found.owner">
							<div class="tag" title="{{tag.description}}" ng-repeat="tag in tags" tag="tag">
							<span class="delete" ng-click="delete()"> <i class="icon-trash"></i> Eliminar</span>

							</div>
							</div>')(scope)
			img = element[0]
			checksize = ->
				overlay.css 'height', "#{img.height}px"
				overlay.css 'width', "#{img.width}px"
				labelsContainer.css 'height', "#{img.height}px"
				labelsContainer.css 'width', "#{img.width}px"
			img.onload = ()->
				element.wrap imageContainer
				element.after labelsContainer
				element.after overlay
				checksize()
				
			addTag = (event)->
				y = if event.offsetY is undefined then  event.originalEvent.layerY else event.offsetY
				x = if event.offsetX is undefined then event.originalEvent.layerX else event.offsetX
				tag =
					top: y
					left: x
					height: 120
					width: 120
				console.dir event
				scope.$emit 'create-tag', tag
				overlay.unbind 'click'
				overlay.toggleClass 'mark-tag'
				scope.$apply()

			scope.newTag = (e)->
				e.preventDefault()
				e.stopPropagation()
				overlay.bind 'click', addTag
				checksize()
				overlay.toggleClass 'mark-tag'
			scope.finishTag = ()->
				overlay.remove()
		,0,false
sivaui.directive 'tag', ($compile)->
	restrict: 'A'
	scope:
		tag:"="
	link: (scope, element, attributes)->
		label = """
			<div class="pure-form">
				<textarea ng-model="tag.description" ui-event="{ blur : 'updateTag(tag)'}"></textarea>
			</div>
		"""
		element.append $compile(label)(scope)
		element.css 'height', scope.tag.height + 'px'
		element.css 'width', scope.tag.width + 'px'
		element.css 'top', scope.tag.top + 'px'
		element.css 'left', scope.tag.left + 'px'
		element.css 'position', 'absolute'
		scope.editable = false
		overlay = angular.element '<div class="overlay-tag"></div>'
		overlay.css 'z-index', '1000'
		scope.delete = ()->
			confirmado = window.confirm '¿Esta seguro de eliminar la etiqueta?'
			if confirmado
				scope.tag.$delete (success)->
					console.dir success
					scope.$emit 'notification', {t:'neutral',m:success.message}
					element.remove()
					disable()
				, (error)->
					console.dir error.data
		enable = ()->
			if !scope.editable
				element.css 'z-index', '1001'
				element.addClass 'editable'
				element.after overlay
				scope.editable = true
				element.draggable
					containment: "parent"
					stop: (e, el)->
						element.css 'position', 'absolute'
						scope.tag.top = el.position.top
						scope.tag.left = el.position.left
						scope.$emit 'update-tag', scope.tag
						scope.$apply()
				element.resizable
					handles: "n, ne, nw, e, s, se, sw, w"
					containment: "parent"
					stop: (e, el)->
						element.css 'position', 'absolute'
						scope.tag.top = el.position.top
						scope.tag.left = el.position.left
						scope.tag.height = el.size.height
						scope.tag.width = el.size.width
						scope.$emit 'update-tag', scope.tag
						scope.$apply()
	
			overlay.bind 'click', (e)->
				disable()
				scope.editable = false
	
		disable = ()->
			overlay.remove()
			element.css 'z-index', 'auto'
			element.draggable("destroy")
			element.resizable("destroy")
			element.removeClass 'editable'

		
		element.bind 'click', (e)->
			e.stopPropagation()
			e.preventDefault()
			enable()
			scope.editable = true
		

		scope.updateTag = (tag)->
			scope.$emit 'update-tag', tag

sivaui.directive 'uploader', ($compile, $timeout)->
	restrict:'A'
	scope:
		uploader:'='
	link: (scope, element, attributes) ->
		scope.disable = false
		# Layout creation
		wrapper = document.createElement('div')
		wrapper.className = 'uploader-container'
		element.after wrapper
		spinner = """
		<div class="spinner" ng-show="disable">
			<img src="/img/spinner.gif" alt="spinner">
			<span class="message">Cargando imagen</span>
		</div>
		"""
		element.after $compile(spinner)(scope)


		angular.element(wrapper).append element
		
		input = document.createElement('input')
		input.type = 'file'
		element.after input
		# Fire file dialog on button click
		triggerDialog = (e)->
			e.preventDefault()
			e.stopPropagation()
			input.click()

		element.bind 'click', triggerDialog
		# Fire upload function when file selected
		input.onchange = (value)->
			scope.uploader(input.files)
		scope.$on 'uploader', (e, action)->
			if action is 'enable'
				$timeout ()->
					scope.disable = false
				, 150
			else
				scope.disable = true

sivaui.directive 'onBottom', ($timeout)->
	restrict:'A'
	link: (scope, element, attr) ->
		lista = element[0]
		checktop = (event)->
			position = (@scrollHeight - @scrollTop) - @clientHeight
			$timeout ()->
				if position <= 50
					scope.loadMore()
			,0 , false
		lista.onscroll = checktop

sivaui.directive 'notification', ($compile, $timeout)->
	restrict:'C'
	link: (scope, elm, attr)->
		scope.alerts = []
		lista = angular.element "<ul ng-show='alerts'></ul>"
		elm.append lista
		remove = (item)->
			$timeout ()->
				item.remove()
			, 3500

		scope.$on 'notification', (ev, item)->
			item = angular.element "<li class='#{item.t}'>#{item.m}</li>"
			lista.append item
			item.bind 'ready', remove(item)

sivaui.directive 'pie', ($timeout)->
	restrict: 'A',
	scope:
		pie:"="
	link: ($scope, $elm, $attr)->
		ctx = $elm[0].getContext("2d")
		options =
			scaleOverlay : true
			scaleOverride : true,
			scaleSteps : 10,
			scaleStepWidth : 10,
			scaleStartValue : 0

		legendContainer = angular.element '<div class="report-legend-container"></div>'
		$elm.after legendContainer
		title = angular.element '<h2 class="report-title"></h2>'
		$elm.before title
		$scope.$watch 'pie', (report)->
			if report
				legendContainer.html ''
				title.text report.title
			if report.datasets.hombres && report.datasets.mujeres
				data =
					labels: report.labels,
					datasets: [report.datasets.hombres,report.datasets.mujeres]
				result = new Chart(ctx).Bar(data, options)
				addLabel report.datasets.hombres
				addLabel report.datasets.mujeres
			if report.datasets.hombres && !report.datasets.mujeres
				data =
					labels: report.labels,
					datasets: [report.datasets.hombres]
				result = new Chart(ctx).Bar(data, options)
				addLabel report.datasets.hombres
			if report.datasets.mujeres && !report.datasets.hombres
				data =
					labels: report.labels,
					datasets: [report.datasets.mujeres]
				result = new Chart(ctx).Bar(data, options)
				addLabel report.datasets.mujeres

		addLabel = (d)->
			legend = """
			<div class="report-legend">
				<span class="color" style="background:#{d.fillColor}"></span>
				<span class="title">#{d.title}</span>
			</div>
			"""
			legendContainer.append legend

sivaui.directive 'loader', ($compile)->
	restrict: 'A',
	scope:
		loader: "="
	link: (scope, elm, attr)->
		template = """
			<div class="loading-message" ng-show="loader">
				<div class="center">
					<img src="/img/spinner.gif" alt="spinner">
					Cargando
				</div>
			</div>
		"""
		elm.append $compile(template)(scope)
sivaui.directive 'paginate', ($compile)->
	restrict: 'A'
	scope: { options: '=' }
	template:"""
		<div class="paginate" ng-show="total > 1">
			<a href="" ng-click="previous()">&#171;</a>
			<a href="" ng-click="setPage(1)" ng-class="{'active':1 == page}">1</a>
			<a ng-repeat="link in links" href="" ng-click="setPage(link)" ng-class="{'active':link == page}">{{link}}</a>
			<a href="" ng-click="setPage(total)" ng-class="{'active':total == page}">{{total}}</a>
			<a href="" ng-click="next()">&#187;</a>
		</div>
	"""
	link: (scope, element, attributes)->
		scope.$watch 'options',(value)->
			if value
				scope.paginator = value
				scope.total = scope.paginator.last
				scope.page = parseInt scope.paginator.page
				makeLinks()
		scope.next = ()->
			if scope.page < scope.total
				scope.page++
				scope.$emit 'paginate', scope.page
				makeLinks()

		scope.previous = ()->
			if scope.page > 1
				scope.page = scope.page - 1
				scope.$emit 'paginate', scope.page
				makeLinks()

		scope.setPage = (page)->
			if (page >= 1) && (page <= scope.total)
				scope.page = page
				scope.$emit 'paginate', scope.page
				makeLinks()

		scope.newPAge = ()->
			algo = 'funciona mierda'

		makeLinks = ()->
			scope.links = []
			page = scope.page
			total = scope.total
			if total > 5
				if page > (total-3)
					addLink(num) for num in [total-5..total-1]
					return
				if page > 3
					addLink(num) for num in [page-2..page+2]
					return
				addLink(num) for num in [2..6]
				return
			else
				if total > 2
					addLink(num) for num in [2..total-1]
		scope.links = []
		addLink = (number)->
			scope.links.push number