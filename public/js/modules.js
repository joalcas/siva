'use strict';
/**
 * Bindonce - Zero watches binding for AngularJs
 * @version v0.1.1 - 2013-05-07
 * @link https://github.com/Pasvaz/bindonce
 * @author Pasquale Vazzana <pasqualevazzana@gmail.com>
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

 angular.module('pasvaz.bindonce', [])

 .directive('bindonce', function() 
 {
	var toBoolean = function(value) 
	{
		if (value && value.length !== 0) 
		{
			var v = angular.lowercase("" + value);
			value = !(v == 'f' || v == '0' || v == 'false' || v == 'no' || v == 'n' || v == '[]');
		}
		else 
		{
			value = false;
		}
		return value;
	}

	var msie = parseInt((/msie (\d+)/.exec(angular.lowercase(navigator.userAgent)) || [])[1], 10);
	if (isNaN(msie)) 
	{
		msie = parseInt((/trident\/.*; rv:(\d+)/.exec(angular.lowercase(navigator.userAgent)) || [])[1], 10);
	}

	var bindonceDirective =
	{
		restrict: "AM",
		controller: ['$scope', '$element', '$attrs', '$interpolate', function($scope, $element, $attrs, $interpolate) 
		{
			var showHideBinder = function(elm, attr, value) 
			{
				var show = (attr == 'show') ? '' : 'none';
				var hide = (attr == 'hide') ? '' : 'none';
				elm.css('display', toBoolean(value) ? show : hide);
			}
			var classBinder = function(elm, value)
			{
				if (angular.isObject(value) && !angular.isArray(value)) 
				{
					var results = [];
					angular.forEach(value, function(value, index) 
					{
						if (value) results.push(index);
					});
					value = results;
				}
				if (value) 
				{
					elm.addClass(angular.isArray(value) ? value.join(' ') : value);
				}
			}

			var ctrl =
			{
				watcherRemover : undefined,
				binders : [],
				group : $attrs.boName,
				element : $element,
				ran : false,

				addBinder : function(binder) 
				{
					this.binders.push(binder);

					// In case of late binding (when using the directive bo-name/bo-parent)
					// it happens only when you use nested bindonce, if the bo-children
					// are not dom children the linking can follow another order
					if (this.ran)
					{
						this.runBinders();
					}
				},

				setupWatcher : function(bindonceValue) 
				{
					var that = this;
					this.watcherRemover = $scope.$watch(bindonceValue, function(newValue) 
					{
						if (newValue == undefined) return;
						that.removeWatcher();
						that.runBinders();
					}, true);
				},

				removeWatcher : function() 
				{
					if (this.watcherRemover != undefined)
					{
						this.watcherRemover();
						this.watcherRemover = undefined;
					}
				},

				runBinders : function()
				{
					var i, max;
					for (i = 0, max = this.binders.length; i < max; i ++)
					{
						var binder = this.binders[i];
						if (this.group && this.group != binder.group ) continue;
						var value = binder.scope.$eval((binder.interpolate) ? $interpolate(binder.value) : binder.value);
						switch(binder.attr)
						{
							case 'if':
								if (toBoolean(value)) 
								{
									binder.transclude(binder.scope.$new(), function (clone) 
									{
										var parent = binder.element.parent();
										var afterNode = binder.element && binder.element[binder.element.length - 1];
										var parentNode = parent && parent[0] || afterNode && afterNode.parentNode;
										var afterNextSibling = (afterNode && afterNode.nextSibling) || null;
										angular.forEach(clone, function(node) 
										{
											parentNode.insertBefore(node, afterNextSibling);
										});
									});
								}
								break;
							case 'hide':
							case 'show':
								showHideBinder(binder.element, binder.attr, value);
								break;
							case 'class':
								classBinder(binder.element, value);
								break;
							case 'text':
								binder.element.text(value);
								break;
							case 'html':
								binder.element.html(value);
								break;
							case 'style':
								binder.element.css(value);
								break;
							case 'src':
								binder.element.attr(binder.attr, value);
								if (msie) binder.element.prop('src', value);
							case 'attr':
								angular.forEach(binder.attrs, function(attrValue, attrKey) 
								{
									var newAttr, newValue;
									if (attrKey.match(/^boAttr./) && binder.attrs[attrKey]) 
									{
										newAttr = attrKey.replace(/^boAttr/, '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
										newValue = binder.scope.$eval(binder.attrs[attrKey]);
										binder.element.attr(newAttr, newValue);
									}
								});
								break;
							case 'href':
							case 'alt':
							case 'title':
							case 'id':
							case 'value':
								binder.element.attr(binder.attr, value);
								break;
						}
					}
					this.ran = true;
					this.binders = [];
				}
			}

			return ctrl;
		}],

		link: function(scope, elm, attrs, bindonceController) 
		{
			var value = (attrs.bindonce) ? scope.$eval(attrs.bindonce) : true;
			if (value != undefined)
			{
				bindonceController.runBinders();
			}
			else
			{
				bindonceController.setupWatcher(attrs.bindonce);
				elm.bind("$destroy", bindonceController.removeWatcher);
			}
		}
	};

	return bindonceDirective;
});

angular.forEach(
[
	{directiveName:'boShow', attribute: 'show'},
	{directiveName:'boIf', attribute: 'if', transclude: 'element', terminal: true, priority:1000},
	{directiveName:'boHide', attribute:'hide'},
	{directiveName:'boClass', attribute:'class'},
	{directiveName:'boText', attribute:'text'},
	{directiveName:'boHtml', attribute:'html'},
	{directiveName:'boSrcI', attribute:'src', interpolate:true},
	{directiveName:'boSrc', attribute:'src'},
	{directiveName:'boHrefI', attribute:'href', interpolate:true},
	{directiveName:'boHref', attribute:'href'},
	{directiveName:'boAlt', attribute:'alt'},
	{directiveName:'boTitle', attribute:'title'},
	{directiveName:'boId', attribute:'id'},
	{directiveName:'boStyle', attribute:'style'},
	{directiveName:'boValue', attribute:'value'},
	{directiveName:'boAttr', attribute:'attr'}
],
function(boDirective)
{
	var childPriority = 200;
	return angular.module('pasvaz.bindonce').directive(boDirective.directiveName, function() 
	{
		var bindonceDirective =
		{ 
			priority: boDirective.priority || childPriority,
			transclude: boDirective.transclude || false, 
			terminal: boDirective.terminal || false, 
			require: '^bindonce', 
			compile: function (tElement, tAttrs, transclude) 
			{
				return function(scope, elm, attrs, bindonceController)
				{
					var name = attrs.boParent;
					if (name && bindonceController.group != name)
					{
						var element = bindonceController.element.parent();
						bindonceController = undefined;
						var parentValue;

						while (element[0].nodeType != 9 && element.length)
						{
							if ((parentValue = element.data('$bindonceController')) 
								&& parentValue.group == name)
							{
								bindonceController = parentValue
								break;
							}
							element = element.parent();
						}
						if (!bindonceController)
						{
							throw Error("No bindonce controller: " + name);
						}
					}

					bindonceController.addBinder(
					{
						element		: 	elm,
						attr		: 	boDirective.attribute,
						attrs 		: 	attrs,
						value		: 	attrs[boDirective.directiveName],
						interpolate	: 	boDirective.interpolate,
						group		: 	name,
						transclude	: 	transclude,
						scope		: 	scope
					});
				}
			}
		}

		return bindonceDirective;
	});
});/**
 * General-purpose Event binding. Bind any event not natively supported by Angular
 * Pass an object with keynames for events to ui-event
 * Allows $event object and $params object to be passed
 *
 * @example <input ui-event="{ focus : 'counter++', blur : 'someCallback()' }">
 * @example <input ui-event="{ myCustomEvent : 'myEventHandler($event, $params)'}">
 *
 * @param ui-event {string|object literal} The event to bind to as a string or a hash of events with their callbacks
 */
angular.module('ui.event',[]).directive('uiEvent', ['$parse',
  function ($parse) {
    return function ($scope, elm, attrs) {
      var events = $scope.$eval(attrs.uiEvent);
      angular.forEach(events, function (uiEvent, eventName) {
        var fn = $parse(uiEvent);
        elm.bind(eventName, function (evt) {
          var params = Array.prototype.slice.call(arguments);
          //Take out first paramater (event object);
          params = params.splice(1);
          fn($scope, {$event: evt, $params: params});
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        });
      });
    };
  }]);/*global angular */
/*
 jQuery UI Datepicker plugin wrapper

 @note If ≤ IE8 make sure you have a polyfill for Date.toISOString()
 @param [ui-date] {object} Options to pass to $.fn.datepicker() merged onto uiDateConfig
 */

angular.module('ui.date', [])

.constant('uiDateConfig', {})

.directive('uiDate', ['uiDateConfig', '$timeout', function (uiDateConfig, $timeout) {
  'use strict';
  var options;
  options = {};
  angular.extend(options, uiDateConfig);
  return {
    require:'?ngModel',
    link:function (scope, element, attrs, controller) {
      var getOptions = function () {
        return angular.extend({}, uiDateConfig, scope.$eval(attrs.uiDate));
      };
      var initDateWidget = function () {
        var showing = false;
        var opts = getOptions();

        // If we have a controller (i.e. ngModelController) then wire it up
        if (controller) {

          // Set the view value in a $apply block when users selects
          // (calling directive user's function too if provided)
          var _onSelect = opts.onSelect || angular.noop;
          opts.onSelect = function (value, picker) {
            scope.$apply(function() {
              showing = true;
              controller.$setViewValue(element.datepicker("getDate"));
              _onSelect(value, picker);
              element.blur();
            });
          };
          opts.beforeShow = function() {
            showing = true;
          };
          opts.onClose = function(value, picker) {
            showing = false;
          };
          element.on('blur', function() {
            if ( !showing ) {
              scope.$apply(function() {
                element.datepicker("setDate", element.datepicker("getDate"));
                controller.$setViewValue(element.datepicker("getDate"));
              });
            }
          });

          // Update the date picker when the model changes
          controller.$render = function () {
            var date = controller.$viewValue;
            if ( angular.isDefined(date) && date !== null && !angular.isDate(date) ) {
              throw new Error('ng-Model value must be a Date object - currently it is a ' + typeof date + ' - use ui-date-format to convert it from a string');
            }
            element.datepicker("setDate", date);
          };
        }
        // If we don't destroy the old one it doesn't update properly when the config changes
        element.datepicker('destroy');
        // Create the new datepicker widget
        element.datepicker(opts);
        if ( controller ) {
          // Force a render to override whatever is in the input text box
          controller.$render();
        }
      };
      // Watch for changes to the directives options
      scope.$watch(getOptions, initDateWidget, true);
    }
  };
}
])

.constant('uiDateFormatConfig', '')

.directive('uiDateFormat', ['uiDateFormatConfig', function(uiDateFormatConfig) {
  var directive = {
    require:'ngModel',
    link: function(scope, element, attrs, modelCtrl) {
      var dateFormat = attrs.uiDateFormat || uiDateFormatConfig;
      if ( dateFormat ) {
        // Use the datepicker with the attribute value as the dateFormat string to convert to and from a string
        modelCtrl.$formatters.push(function(value) {
          if (angular.isString(value) ) {
            return jQuery.datepicker.parseDate(dateFormat, value);
          }
          return null;
        });
        modelCtrl.$parsers.push(function(value){
          if (value) {
            return jQuery.datepicker.formatDate(dateFormat, value);
          }
          return null;
        });
      } else {
        // Default to ISO formatting
        modelCtrl.$formatters.push(function(value) {
          if (angular.isString(value) ) {
            return new Date(value);
          }
          return null;
        });
        modelCtrl.$parsers.push(function(value){
          if (value) {
            return value.toISOString();
          }
          return null;
        });
      }
    }
  };
  return directive;
}]);
/**
 * @license AngularJS v1.0.8
 * (c) 2010-2012 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, angular, undefined) {
'use strict';

/**
 * @ngdoc overview
 * @name ngResource
 * @description
 */

/**
 * @ngdoc object
 * @name ngResource.$resource
 * @requires $http
 *
 * @description
 * A factory which creates a resource object that lets you interact with
 * [RESTful](http://en.wikipedia.org/wiki/Representational_State_Transfer) server-side data sources.
 *
 * The returned resource object has action methods which provide high-level behaviors without
 * the need to interact with the low level {@link ng.$http $http} service.
 *
 * # Installation
 * To use $resource make sure you have included the `angular-resource.js` that comes in Angular 
 * package. You can also find this file on Google CDN, bower as well as at
 * {@link http://code.angularjs.org/ code.angularjs.org}.
 *
 * Finally load the module in your application:
 *
 *        angular.module('app', ['ngResource']);
 *
 * and you are ready to get started!
 *
 * @param {string} url A parameterized URL template with parameters prefixed by `:` as in
 *   `/user/:username`. If you are using a URL with a port number (e.g. 
 *   `http://example.com:8080/api`), you'll need to escape the colon character before the port
 *   number, like this: `$resource('http://example.com\\:8080/api')`.
 *
 * @param {Object=} paramDefaults Default values for `url` parameters. These can be overridden in
 *   `actions` methods.
 *
 *   Each key value in the parameter object is first bound to url template if present and then any
 *   excess keys are appended to the url search query after the `?`.
 *
 *   Given a template `/path/:verb` and parameter `{verb:'greet', salutation:'Hello'}` results in
 *   URL `/path/greet?salutation=Hello`.
 *
 *   If the parameter value is prefixed with `@` then the value of that parameter is extracted from
 *   the data object (useful for non-GET operations).
 *
 * @param {Object.<Object>=} actions Hash with declaration of custom action that should extend the
 *   default set of resource actions. The declaration should be created in the following format:
 *
 *       {action1: {method:?, params:?, isArray:?},
 *        action2: {method:?, params:?, isArray:?},
 *        ...}
 *
 *   Where:
 *
 *   - `action` – {string} – The name of action. This name becomes the name of the method on your
 *     resource object.
 *   - `method` – {string} – HTTP request method. Valid methods are: `GET`, `POST`, `PUT`, `DELETE`,
 *     and `JSONP`
 *   - `params` – {object=} – Optional set of pre-bound parameters for this action.
 *   - isArray – {boolean=} – If true then the returned object for this action is an array, see
 *     `returns` section.
 *
 * @returns {Object} A resource "class" object with methods for the default set of resource actions
 *   optionally extended with custom `actions`. The default set contains these actions:
 *
 *       { 'get':    {method:'GET'},
 *         'save':   {method:'POST'},
 *         'query':  {method:'GET', isArray:true},
 *         'remove': {method:'DELETE'},
 *         'delete': {method:'DELETE'} };
 *
 *   Calling these methods invoke an {@link ng.$http} with the specified http method,
 *   destination and parameters. When the data is returned from the server then the object is an
 *   instance of the resource class. The actions `save`, `remove` and `delete` are available on it
 *   as  methods with the `$` prefix. This allows you to easily perform CRUD operations (create,
 *   read, update, delete) on server-side data like this:
 *   <pre>
        var User = $resource('/user/:userId', {userId:'@id'});
        var user = User.get({userId:123}, function() {
          user.abc = true;
          user.$save();
        });
     </pre>
 *
 *   It is important to realize that invoking a $resource object method immediately returns an
 *   empty reference (object or array depending on `isArray`). Once the data is returned from the
 *   server the existing reference is populated with the actual data. This is a useful trick since
 *   usually the resource is assigned to a model which is then rendered by the view. Having an empty
 *   object results in no rendering, once the data arrives from the server then the object is
 *   populated with the data and the view automatically re-renders itself showing the new data. This
 *   means that in most case one never has to write a callback function for the action methods.
 *
 *   The action methods on the class object or instance object can be invoked with the following
 *   parameters:
 *
 *   - HTTP GET "class" actions: `Resource.action([parameters], [success], [error])`
 *   - non-GET "class" actions: `Resource.action([parameters], postData, [success], [error])`
 *   - non-GET instance actions:  `instance.$action([parameters], [success], [error])`
 *
 *
 * @example
 *
 * # Credit card resource
 *
 * <pre>
     // Define CreditCard class
     var CreditCard = $resource('/user/:userId/card/:cardId',
      {userId:123, cardId:'@id'}, {
       charge: {method:'POST', params:{charge:true}}
      });

     // We can retrieve a collection from the server
     var cards = CreditCard.query(function() {
       // GET: /user/123/card
       // server returns: [ {id:456, number:'1234', name:'Smith'} ];

       var card = cards[0];
       // each item is an instance of CreditCard
       expect(card instanceof CreditCard).toEqual(true);
       card.name = "J. Smith";
       // non GET methods are mapped onto the instances
       card.$save();
       // POST: /user/123/card/456 {id:456, number:'1234', name:'J. Smith'}
       // server returns: {id:456, number:'1234', name: 'J. Smith'};

       // our custom method is mapped as well.
       card.$charge({amount:9.99});
       // POST: /user/123/card/456?amount=9.99&charge=true {id:456, number:'1234', name:'J. Smith'}
     });

     // we can create an instance as well
     var newCard = new CreditCard({number:'0123'});
     newCard.name = "Mike Smith";
     newCard.$save();
     // POST: /user/123/card {number:'0123', name:'Mike Smith'}
     // server returns: {id:789, number:'01234', name: 'Mike Smith'};
     expect(newCard.id).toEqual(789);
 * </pre>
 *
 * The object returned from this function execution is a resource "class" which has "static" method
 * for each action in the definition.
 *
 * Calling these methods invoke `$http` on the `url` template with the given `method` and `params`.
 * When the data is returned from the server then the object is an instance of the resource type and
 * all of the non-GET methods are available with `$` prefix. This allows you to easily support CRUD
 * operations (create, read, update, delete) on server-side data.

   <pre>
     var User = $resource('/user/:userId', {userId:'@id'});
     var user = User.get({userId:123}, function() {
       user.abc = true;
       user.$save();
     });
   </pre>
 *
 * It's worth noting that the success callback for `get`, `query` and other method gets passed
 * in the response that came from the server as well as $http header getter function, so one
 * could rewrite the above example and get access to http headers as:
 *
   <pre>
     var User = $resource('/user/:userId', {userId:'@id'});
     User.get({userId:123}, function(u, getResponseHeaders){
       u.abc = true;
       u.$save(function(u, putResponseHeaders) {
         //u => saved user object
         //putResponseHeaders => $http header getter
       });
     });
   </pre>

 * # Buzz client

   Let's look at what a buzz client created with the `$resource` service looks like:
    <doc:example>
      <doc:source jsfiddle="false">
       <script>
         function BuzzController($resource) {
           this.userId = 'googlebuzz';
           this.Activity = $resource(
             'https://www.googleapis.com/buzz/v1/activities/:userId/:visibility/:activityId/:comments',
             {alt:'json', callback:'JSON_CALLBACK'},
             {get:{method:'JSONP', params:{visibility:'@self'}}, replies: {method:'JSONP', params:{visibility:'@self', comments:'@comments'}}}
           );
         }

         BuzzController.prototype = {
           fetch: function() {
             this.activities = this.Activity.get({userId:this.userId});
           },
           expandReplies: function(activity) {
             activity.replies = this.Activity.replies({userId:this.userId, activityId:activity.id});
           }
         };
         BuzzController.$inject = ['$resource'];
       </script>

       <div ng-controller="BuzzController">
         <input ng-model="userId"/>
         <button ng-click="fetch()">fetch</button>
         <hr/>
         <div ng-repeat="item in activities.data.items">
           <h1 style="font-size: 15px;">
             <img src="{{item.actor.thumbnailUrl}}" style="max-height:30px;max-width:30px;"/>
             <a href="{{item.actor.profileUrl}}">{{item.actor.name}}</a>
             <a href ng-click="expandReplies(item)" style="float: right;">Expand replies: {{item.links.replies[0].count}}</a>
           </h1>
           {{item.object.content | html}}
           <div ng-repeat="reply in item.replies.data.items" style="margin-left: 20px;">
             <img src="{{reply.actor.thumbnailUrl}}" style="max-height:30px;max-width:30px;"/>
             <a href="{{reply.actor.profileUrl}}">{{reply.actor.name}}</a>: {{reply.content | html}}
           </div>
         </div>
       </div>
      </doc:source>
      <doc:scenario>
      </doc:scenario>
    </doc:example>
 */
angular.module('ngResource', ['ng']).
  factory('$resource', ['$http', '$parse', function($http, $parse) {
    var DEFAULT_ACTIONS = {
      'get':    {method:'GET'},
      'save':   {method:'POST'},
      'query':  {method:'GET', isArray:true},
      'remove': {method:'DELETE'},
      'delete': {method:'DELETE'}
    };
    var noop = angular.noop,
        forEach = angular.forEach,
        extend = angular.extend,
        copy = angular.copy,
        isFunction = angular.isFunction,
        getter = function(obj, path) {
          return $parse(path)(obj);
        };

    /**
     * We need our custom method because encodeURIComponent is too aggressive and doesn't follow
     * http://www.ietf.org/rfc/rfc3986.txt with regards to the character set (pchar) allowed in path
     * segments:
     *    segment       = *pchar
     *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
     *    pct-encoded   = "%" HEXDIG HEXDIG
     *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
     *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
     *                     / "*" / "+" / "," / ";" / "="
     */
    function encodeUriSegment(val) {
      return encodeUriQuery(val, true).
        replace(/%26/gi, '&').
        replace(/%3D/gi, '=').
        replace(/%2B/gi, '+');
    }


    /**
     * This method is intended for encoding *key* or *value* parts of query component. We need a custom
     * method becuase encodeURIComponent is too agressive and encodes stuff that doesn't have to be
     * encoded per http://tools.ietf.org/html/rfc3986:
     *    query       = *( pchar / "/" / "?" )
     *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
     *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
     *    pct-encoded   = "%" HEXDIG HEXDIG
     *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
     *                     / "*" / "+" / "," / ";" / "="
     */
    function encodeUriQuery(val, pctEncodeSpaces) {
      return encodeURIComponent(val).
        replace(/%40/gi, '@').
        replace(/%3A/gi, ':').
        replace(/%24/g, '$').
        replace(/%2C/gi, ',').
        replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
    }

    function Route(template, defaults) {
      this.template = template = template + '#';
      this.defaults = defaults || {};
      var urlParams = this.urlParams = {};
      forEach(template.split(/\W/), function(param){
        if (param && (new RegExp("(^|[^\\\\]):" + param + "\\W").test(template))) {
          urlParams[param] = true;
        }
      });
      this.template = template.replace(/\\:/g, ':');
    }

    Route.prototype = {
      url: function(params) {
        var self = this,
            url = this.template,
            val,
            encodedVal;

        params = params || {};
        forEach(this.urlParams, function(_, urlParam){
          val = params.hasOwnProperty(urlParam) ? params[urlParam] : self.defaults[urlParam];
          if (angular.isDefined(val) && val !== null) {
            encodedVal = encodeUriSegment(val);
            url = url.replace(new RegExp(":" + urlParam + "(\\W)", "g"), encodedVal + "$1");
          } else {
            url = url.replace(new RegExp("(\/?):" + urlParam + "(\\W)", "g"), function(match,
                leadingSlashes, tail) {
              if (tail.charAt(0) == '/') {
                return tail;
              } else {
                return leadingSlashes + tail;
              }
            });
          }
        });
        url = url.replace(/\/?#$/, '');
        var query = [];
        forEach(params, function(value, key){
          if (!self.urlParams[key]) {
            query.push(encodeUriQuery(key) + '=' + encodeUriQuery(value));
          }
        });
        query.sort();
        url = url.replace(/\/*$/, '');
        return url + (query.length ? '?' + query.join('&') : '');
      }
    };


    function ResourceFactory(url, paramDefaults, actions) {
      var route = new Route(url);

      actions = extend({}, DEFAULT_ACTIONS, actions);

      function extractParams(data, actionParams){
        var ids = {};
        actionParams = extend({}, paramDefaults, actionParams);
        forEach(actionParams, function(value, key){
          ids[key] = value.charAt && value.charAt(0) == '@' ? getter(data, value.substr(1)) : value;
        });
        return ids;
      }

      function Resource(value){
        copy(value || {}, this);
      }

      forEach(actions, function(action, name) {
        action.method = angular.uppercase(action.method);
        var hasBody = action.method == 'POST' || action.method == 'PUT' || action.method == 'PATCH';
        Resource[name] = function(a1, a2, a3, a4) {
          var params = {};
          var data;
          var success = noop;
          var error = null;
          switch(arguments.length) {
          case 4:
            error = a4;
            success = a3;
            //fallthrough
          case 3:
          case 2:
            if (isFunction(a2)) {
              if (isFunction(a1)) {
                success = a1;
                error = a2;
                break;
              }

              success = a2;
              error = a3;
              //fallthrough
            } else {
              params = a1;
              data = a2;
              success = a3;
              break;
            }
          case 1:
            if (isFunction(a1)) success = a1;
            else if (hasBody) data = a1;
            else params = a1;
            break;
          case 0: break;
          default:
            throw "Expected between 0-4 arguments [params, data, success, error], got " +
              arguments.length + " arguments.";
          }

          var value = this instanceof Resource ? this : (action.isArray ? [] : new Resource(data));
          $http({
            method: action.method,
            url: route.url(extend({}, extractParams(data, action.params || {}), params)),
            data: data
          }).then(function(response) {
              var data = response.data;

              if (data) {
                if (action.isArray) {
                  value.length = 0;
                  forEach(data, function(item) {
                    value.push(new Resource(item));
                  });
                } else {
                  copy(data, value);
                }
              }
              (success||noop)(value, response.headers);
            }, error);

          return value;
        };


        Resource.prototype['$' + name] = function(a1, a2, a3) {
          var params = extractParams(this),
              success = noop,
              error;

          switch(arguments.length) {
          case 3: params = a1; success = a2; error = a3; break;
          case 2:
          case 1:
            if (isFunction(a1)) {
              success = a1;
              error = a2;
            } else {
              params = a1;
              success = a2 || noop;
            }
          case 0: break;
          default:
            throw "Expected between 1-3 arguments [params, success, error], got " +
              arguments.length + " arguments.";
          }
          var data = hasBody ? this : undefined;
          Resource[name].call(this, params, data, success, error);
        };
      });

      Resource.bind = function(additionalParamDefaults){
        return ResourceFactory(url, extend({}, paramDefaults, additionalParamDefaults), actions);
      };

      return Resource;
    }

    return ResourceFactory;
  }]);


})(window, window.angular);
angular.module("ngLocale", [], ["$provide", function($provide) {
var PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
$provide.value("$locale", {
  "DATETIME_FORMATS": {
    "AMPMS": [
      "a.m.",
      "p.m."
    ],
    "DAY": [
      "domingo",
      "lunes",
      "martes",
      "mi\u00e9rcoles",
      "jueves",
      "viernes",
      "s\u00e1bado"
    ],
    "MONTH": [
      "enero",
      "febrero",
      "marzo",
      "abril",
      "mayo",
      "junio",
      "julio",
      "agosto",
      "septiembre",
      "octubre",
      "noviembre",
      "diciembre"
    ],
    "SHORTDAY": [
      "dom",
      "lun",
      "mar",
      "mi\u00e9",
      "jue",
      "vie",
      "s\u00e1b"
    ],
    "SHORTMONTH": [
      "ene",
      "feb",
      "mar",
      "abr",
      "may",
      "jun",
      "jul",
      "ago",
      "sep",
      "oct",
      "nov",
      "dic"
    ],
    "fullDate": "EEEE, d 'de' MMMM 'de' y",
    "longDate": "d 'de' MMMM 'de' y",
    "medium": "d/MM/yyyy H:mm:ss",
    "mediumDate": "d/MM/yyyy",
    "mediumTime": "H:mm:ss",
    "short": "d/MM/yy H:mm",
    "shortDate": "d/MM/yy",
    "shortTime": "H:mm"
  },
  "NUMBER_FORMATS": {
    "CURRENCY_SYM": "\u20ac",
    "DECIMAL_SEP": ",",
    "GROUP_SEP": ".",
    "PATTERNS": [
      {
        "gSize": 3,
        "lgSize": 3,
        "macFrac": 0,
        "maxFrac": 3,
        "minFrac": 0,
        "minInt": 1,
        "negPre": "-",
        "negSuf": "",
        "posPre": "",
        "posSuf": ""
      },
      {
        "gSize": 3,
        "lgSize": 3,
        "macFrac": 0,
        "maxFrac": 2,
        "minFrac": 2,
        "minInt": 1,
        "negPre": "-",
        "negSuf": "\u00a0\u00a4",
        "posPre": "",
        "posSuf": "\u00a0\u00a4"
      }
    ]
  },
  "id": "es-co",
  "pluralCat": function (n) {  if (n == 1) {   return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;}
});
}]);angular.module("ui.bootstrap",["ui.bootstrap.tpls","ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.position","ui.bootstrap.datepicker","ui.bootstrap.dialog","ui.bootstrap.dropdownToggle","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.timepicker","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.tpls",["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/carousel/carousel.html","template/carousel/slide.html","template/datepicker/datepicker.html","template/datepicker/popup.html","template/dialog/message.html","template/pagination/pager.html","template/pagination/pagination.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/rating/rating.html","template/tabs/tab.html","template/tabs/tabset-titles.html","template/tabs/tabset.html","template/timepicker/timepicker.html","template/typeahead/typeahead-match.html","template/typeahead/typeahead-popup.html"]),angular.module("ui.bootstrap.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(e,t,n){function a(e){for(var t in e)if(void 0!==o.style[t])return e[t]}var i=function(a,o,r){r=r||{};var l=e.defer(),s=i[r.animation?"animationEndEventName":"transitionEndEventName"],c=function(){n.$apply(function(){a.unbind(s,c),l.resolve(a)})};return s&&a.bind(s,c),t(function(){angular.isString(o)?a.addClass(o):angular.isFunction(o)?o(a):angular.isObject(o)&&a.css(o),s||l.resolve(a)}),l.promise.cancel=function(){s&&a.unbind(s,c),l.reject("Transition cancelled")},l.promise},o=document.createElement("trans"),r={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},l={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return i.transitionEndEventName=a(r),i.animationEndEventName=a(l),i}]),angular.module("ui.bootstrap.collapse",["ui.bootstrap.transition"]).directive("collapse",["$transition",function(e){var t=function(e,t,n){t.removeClass("collapse"),t.css({height:n}),t[0].offsetWidth,t.addClass("collapse")};return{link:function(n,a,i){var o,r=!0;n.$watch(function(){return a[0].scrollHeight},function(){0!==a[0].scrollHeight&&(o||(r?t(n,a,a[0].scrollHeight+"px"):t(n,a,"auto")))}),n.$watch(i.collapse,function(e){e?u():c()});var l,s=function(t){return l&&l.cancel(),l=e(a,t),l.then(function(){l=void 0},function(){l=void 0}),l},c=function(){r?(r=!1,o||t(n,a,"auto")):s({height:a[0].scrollHeight+"px"}).then(function(){o||t(n,a,"auto")}),o=!1},u=function(){o=!0,r?(r=!1,t(n,a,0)):(t(n,a,a[0].scrollHeight+"px"),s({height:"0"}))}}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(e,t,n){this.groups=[],this.closeOthers=function(a){var i=angular.isDefined(t.closeOthers)?e.$eval(t.closeOthers):n.closeOthers;i&&angular.forEach(this.groups,function(e){e!==a&&(e.isOpen=!1)})},this.addGroup=function(e){var t=this;this.groups.push(e),e.$on("$destroy",function(){t.removeGroup(e)})},this.removeGroup=function(e){var t=this.groups.indexOf(e);-1!==t&&this.groups.splice(this.groups.indexOf(e),1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",["$parse","$transition","$timeout",function(e){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@"},controller:["$scope",function(){this.setHeading=function(e){this.heading=e}}],link:function(t,n,a,i){var o,r;i.addGroup(t),t.isOpen=!1,a.isOpen&&(o=e(a.isOpen),r=o.assign,t.$watch(function(){return o(t.$parent)},function(e){t.isOpen=e}),t.isOpen=o?o(t.$parent):!1),t.$watch("isOpen",function(e){e&&i.closeOthers(t),r&&r(t.$parent,e)})}}}]).directive("accordionHeading",function(){return{restrict:"EA",transclude:!0,template:"",replace:!0,require:"^accordionGroup",compile:function(e,t,n){return function(e,t,a,i){i.setHeading(n(e,function(){}))}}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(e,t,n,a){e.$watch(function(){return a[n.accordionTransclude]},function(e){e&&(t.html(""),t.append(e))})}}}),angular.module("ui.bootstrap.alert",[]).directive("alert",function(){return{restrict:"EA",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"=",close:"&"},link:function(e,t,n){e.closeable="close"in n}}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).directive("btnRadio",["buttonConfig",function(e){var t=e.activeClass||"active",n=e.toggleEvent||"click";return{require:"ngModel",link:function(e,a,i,o){o.$render=function(){a.toggleClass(t,angular.equals(o.$modelValue,e.$eval(i.btnRadio)))},a.bind(n,function(){a.hasClass(t)||e.$apply(function(){o.$setViewValue(e.$eval(i.btnRadio)),o.$render()})})}}}]).directive("btnCheckbox",["buttonConfig",function(e){var t=e.activeClass||"active",n=e.toggleEvent||"click";return{require:"ngModel",link:function(e,a,i,o){function r(){var t=e.$eval(i.btnCheckboxTrue);return angular.isDefined(t)?t:!0}function l(){var t=e.$eval(i.btnCheckboxFalse);return angular.isDefined(t)?t:!1}o.$render=function(){a.toggleClass(t,angular.equals(o.$modelValue,r()))},a.bind(n,function(){e.$apply(function(){o.$setViewValue(a.hasClass(t)?l():r()),o.$render()})})}}}]),angular.module("ui.bootstrap.carousel",["ui.bootstrap.transition"]).controller("CarouselController",["$scope","$timeout","$transition","$q",function(e,t,n){function a(){function n(){o?(e.next(),a()):e.pause()}i&&t.cancel(i);var r=+e.interval;!isNaN(r)&&r>=0&&(i=t(n,r))}var i,o,r=this,l=r.slides=[],s=-1;r.currentSlide=null,r.select=function(i,o){function c(){r.currentSlide&&angular.isString(o)&&!e.noTransition&&i.$element?(i.$element.addClass(o),i.$element[0].offsetWidth=i.$element[0].offsetWidth,angular.forEach(l,function(e){angular.extend(e,{direction:"",entering:!1,leaving:!1,active:!1})}),angular.extend(i,{direction:o,active:!0,entering:!0}),angular.extend(r.currentSlide||{},{direction:o,leaving:!0}),e.$currentTransition=n(i.$element,{}),function(t,n){e.$currentTransition.then(function(){u(t,n)},function(){u(t,n)})}(i,r.currentSlide)):u(i,r.currentSlide),r.currentSlide=i,s=p,a()}function u(t,n){angular.extend(t,{direction:"",active:!0,leaving:!1,entering:!1}),angular.extend(n||{},{direction:"",active:!1,leaving:!1,entering:!1}),e.$currentTransition=null}var p=l.indexOf(i);void 0===o&&(o=p>s?"next":"prev"),i&&i!==r.currentSlide&&(e.$currentTransition?(e.$currentTransition.cancel(),t(c)):c())},r.indexOfSlide=function(e){return l.indexOf(e)},e.next=function(){var t=(s+1)%l.length;return e.$currentTransition?void 0:r.select(l[t],"next")},e.prev=function(){var t=0>s-1?l.length-1:s-1;return e.$currentTransition?void 0:r.select(l[t],"prev")},e.select=function(e){r.select(e)},e.isActive=function(e){return r.currentSlide===e},e.slides=function(){return l},e.$watch("interval",a),e.play=function(){o||(o=!0,a())},e.pause=function(){e.noPause||(o=!1,i&&t.cancel(i))},r.addSlide=function(t,n){t.$element=n,l.push(t),1===l.length||t.active?(r.select(l[l.length-1]),1==l.length&&e.play()):t.active=!1},r.removeSlide=function(e){var t=l.indexOf(e);l.splice(t,1),l.length>0&&e.active?t>=l.length?r.select(l[t-1]):r.select(l[t]):s>t&&s--}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"=",noPause:"="}}}]).directive("slide",["$parse",function(e){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{},link:function(t,n,a,i){if(a.active){var o=e(a.active),r=o.assign,l=t.active=o(t.$parent);t.$watch(function(){var e=o(t.$parent);return e!==t.active&&(e!==l?l=t.active=e:r(t.$parent,e=l=t.active)),e})}i.addSlide(t,n),t.$on("$destroy",function(){i.removeSlide(t)}),t.$watch("active",function(e){e&&i.select(t)})}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(e,t){function n(e,n){return e.currentStyle?e.currentStyle[n]:t.getComputedStyle?t.getComputedStyle(e)[n]:e.style[n]}function a(e){return"static"===(n(e,"position")||"static")}var i,o;e.bind("mousemove",function(e){i=e.pageX,o=e.pageY});var r=function(t){for(var n=e[0],i=t.offsetParent||n;i&&i!==n&&a(i);)i=i.offsetParent;return i||n};return{position:function(t){var n=this.offset(t),a={top:0,left:0},i=r(t[0]);return i!=e[0]&&(a=this.offset(angular.element(i)),a.top+=i.clientTop-i.scrollTop,a.left+=i.clientLeft-i.scrollLeft),{width:t.prop("offsetWidth"),height:t.prop("offsetHeight"),top:n.top-a.top,left:n.left-a.left}},offset:function(n){var a=n[0].getBoundingClientRect();return{width:n.prop("offsetWidth"),height:n.prop("offsetHeight"),top:a.top+(t.pageYOffset||e[0].body.scrollTop),left:a.left+(t.pageXOffset||e[0].body.scrollLeft)}},mouse:function(){return{x:i,y:o}}}}]),angular.module("ui.bootstrap.datepicker",["ui.bootstrap.position"]).constant("datepickerConfig",{dayFormat:"dd",monthFormat:"MMMM",yearFormat:"yyyy",dayHeaderFormat:"EEE",dayTitleFormat:"MMMM yyyy",monthTitleFormat:"yyyy",showWeeks:!0,startingDay:0,yearRange:20,minDate:null,maxDate:null}).controller("DatepickerController",["$scope","$attrs","dateFilter","datepickerConfig",function(e,t,n,a){function i(t,n){return angular.isDefined(t)?e.$parent.$eval(t):n}function o(e,t){return new Date(e,t,0).getDate()}function r(e,t){for(var n=Array(t),a=e,i=0;t>i;)n[i++]=new Date(a),a.setDate(a.getDate()+1);return n}function l(e,t,a,i){return{date:e,label:n(e,t),selected:!!a,secondary:!!i}}var s={day:i(t.dayFormat,a.dayFormat),month:i(t.monthFormat,a.monthFormat),year:i(t.yearFormat,a.yearFormat),dayHeader:i(t.dayHeaderFormat,a.dayHeaderFormat),dayTitle:i(t.dayTitleFormat,a.dayTitleFormat),monthTitle:i(t.monthTitleFormat,a.monthTitleFormat)},c=i(t.startingDay,a.startingDay),u=i(t.yearRange,a.yearRange);this.minDate=a.minDate?new Date(a.minDate):null,this.maxDate=a.maxDate?new Date(a.maxDate):null,this.modes=[{name:"day",getVisibleDates:function(e,t){var a=e.getFullYear(),i=e.getMonth(),u=new Date(a,i,1),p=c-u.getDay(),d=p>0?7-p:-p,m=new Date(u),g=0;d>0&&(m.setDate(-d+1),g+=d),g+=o(a,i+1),g+=(7-g%7)%7;for(var f=r(m,g),h=Array(7),v=0;g>v;v++){var b=new Date(f[v]);f[v]=l(b,s.day,t&&t.getDate()===b.getDate()&&t.getMonth()===b.getMonth()&&t.getFullYear()===b.getFullYear(),b.getMonth()!==i)}for(var $=0;7>$;$++)h[$]=n(f[$].date,s.dayHeader);return{objects:f,title:n(e,s.dayTitle),labels:h}},compare:function(e,t){return new Date(e.getFullYear(),e.getMonth(),e.getDate())-new Date(t.getFullYear(),t.getMonth(),t.getDate())},split:7,step:{months:1}},{name:"month",getVisibleDates:function(e,t){for(var a=Array(12),i=e.getFullYear(),o=0;12>o;o++){var r=new Date(i,o,1);a[o]=l(r,s.month,t&&t.getMonth()===o&&t.getFullYear()===i)}return{objects:a,title:n(e,s.monthTitle)}},compare:function(e,t){return new Date(e.getFullYear(),e.getMonth())-new Date(t.getFullYear(),t.getMonth())},split:3,step:{years:1}},{name:"year",getVisibleDates:function(e,t){for(var n=Array(u),a=e.getFullYear(),i=parseInt((a-1)/u,10)*u+1,o=0;u>o;o++){var r=new Date(i+o,0,1);n[o]=l(r,s.year,t&&t.getFullYear()===r.getFullYear())}return{objects:n,title:[n[0].label,n[u-1].label].join(" - ")}},compare:function(e,t){return e.getFullYear()-t.getFullYear()},split:5,step:{years:u}}],this.isDisabled=function(t,n){var a=this.modes[n||0];return this.minDate&&0>a.compare(t,this.minDate)||this.maxDate&&a.compare(t,this.maxDate)>0||e.dateDisabled&&e.dateDisabled({date:t,mode:a.name})}}]).directive("datepicker",["dateFilter","$parse","datepickerConfig","$log",function(e,t,n,a){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/datepicker.html",scope:{dateDisabled:"&"},require:["datepicker","?^ngModel"],controller:"DatepickerController",link:function(e,i,o,r){function l(){e.showWeekNumbers=0===g&&h}function s(e,t){for(var n=[];e.length>0;)n.push(e.splice(0,t));return n}function c(t){var n=null,i=!0;m.$modelValue&&(n=new Date(m.$modelValue),isNaN(n)?(i=!1,a.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')):t&&(f=n)),m.$setValidity("date",i);var o=d.modes[g],r=o.getVisibleDates(f,n);angular.forEach(r.objects,function(e){e.disabled=d.isDisabled(e.date,g)}),m.$setValidity("date-disabled",!n||!d.isDisabled(n)),e.rows=s(r.objects,o.split),e.labels=r.labels||[],e.title=r.title}function u(e){g=e,l(),c()}function p(e){var t=new Date(e);t.setDate(t.getDate()+4-(t.getDay()||7));var n=t.getTime();return t.setMonth(0),t.setDate(1),Math.floor(Math.round((n-t)/864e5)/7)+1}var d=r[0],m=r[1];if(m){var g=0,f=new Date,h=n.showWeeks;o.showWeeks?e.$parent.$watch(t(o.showWeeks),function(e){h=!!e,l()}):l(),o.min&&e.$parent.$watch(t(o.min),function(e){d.minDate=e?new Date(e):null,c()}),o.max&&e.$parent.$watch(t(o.max),function(e){d.maxDate=e?new Date(e):null,c()}),m.$render=function(){c(!0)},e.select=function(e){if(0===g){var t=new Date(m.$modelValue);t.setFullYear(e.getFullYear(),e.getMonth(),e.getDate()),m.$setViewValue(t),c(!0)}else f=e,u(g-1)},e.move=function(e){var t=d.modes[g].step;f.setMonth(f.getMonth()+e*(t.months||0)),f.setFullYear(f.getFullYear()+e*(t.years||0)),c()},e.toggleMode=function(){u((g+1)%d.modes.length)},e.getWeekNumber=function(t){return 0===g&&e.showWeekNumbers&&7===t.length?p(t[0].date):null}}}}}]).constant("datepickerPopupConfig",{dateFormat:"yyyy-MM-dd",closeOnDateSelection:!0}).directive("datepickerPopup",["$compile","$parse","$document","$position","dateFilter","datepickerPopupConfig",function(e,t,n,a,i,o){return{restrict:"EA",require:"ngModel",link:function(r,l,s,c){function u(e){return e?i(e,h):null}function p(e){if(e){var t=new Date(e);if(!isNaN(t))return t}return e}function d(e){$?$(r,!!e):v.isOpen=!!e}function m(e,n,a){e&&(r.$watch(t(e),function(e){v[n]=e}),D.attr(a||n,n))}function g(){v.position=a.position(l),v.position.top=v.position.top+l.prop("offsetHeight")}var f=angular.isDefined(s.closeOnDateSelection)?v.$eval(s.closeOnDateSelection):o.closeOnDateSelection,h=s.datepickerPopup||o.dateFormat,v=r.$new();r.$on("$destroy",function(){v.$destroy()}),c.$formatters.push(u),c.$parsers.push(p);var b,$;s.open&&(b=t(s.open),$=b.assign,r.$watch(b,function(e){v.isOpen=!!e})),v.isOpen=b?b(r):!1;var y=function(e){v.isOpen&&e.target!==l[0]&&v.$apply(function(){d(!1)})},w=function(){v.$apply(function(){d(!0)})},k=angular.element("<datepicker-popup-wrap><datepicker></datepicker></datepicker-popup-wrap>");k.attr({"ng-model":"date","ng-change":"dateSelection()"});var D=k.find("datepicker");s.datepickerOptions&&D.attr(angular.extend({},r.$eval(s.datepickerOptions)));var x=t(s.ngModel).assign;v.dateSelection=function(){x(r,v.date),f&&d(!1)},v.$watch(function(){return c.$modelValue},function(e){if(angular.isString(e)){var t=p(e);if(e&&!t)throw x(r,null),Error(e+" cannot be parsed to a date object.");e=t}v.date=e,g()}),m(s.min,"min"),m(s.max,"max"),s.showWeeks?m(s.showWeeks,"showWeeks","show-weeks"):(v.showWeeks=!0,D.attr("show-weeks","showWeeks")),s.dateDisabled&&D.attr("date-disabled",s.dateDisabled),v.$watch("isOpen",function(e){e?(g(),n.bind("click",y),l.unbind("focus",w),l.focus()):(n.unbind("click",y),l.bind("focus",w)),$&&$(r,e)}),v.today=function(){x(r,new Date)},v.clear=function(){x(r,null)},l.after(e(k)(v))}}}]).directive("datepickerPopupWrap",[function(){return{restrict:"E",replace:!0,transclude:!0,templateUrl:"template/datepicker/popup.html",link:function(e,t){t.bind("click",function(e){e.preventDefault(),e.stopPropagation()})}}}]);var dialogModule=angular.module("ui.bootstrap.dialog",["ui.bootstrap.transition"]);dialogModule.controller("MessageBoxController",["$scope","dialog","model",function(e,t,n){e.title=n.title,e.message=n.message,e.buttons=n.buttons,e.close=function(e){t.close(e)}}]),dialogModule.provider("$dialog",function(){var e={backdrop:!0,dialogClass:"modal",backdropClass:"modal-backdrop",transitionClass:"fade",triggerClass:"in",resolve:{},backdropFade:!1,dialogFade:!1,keyboard:!0,backdropClick:!0},t={},n={value:0};this.options=function(e){t=e},this.$get=["$http","$document","$compile","$rootScope","$controller","$templateCache","$q","$transition","$injector",function(a,i,o,r,l,s,c,u,p){function d(e){var t=angular.element("<div>");return t.addClass(e),t}function m(n){var a=this,i=this.options=angular.extend({},e,t,n);this._open=!1,this.backdropEl=d(i.backdropClass),i.backdropFade&&(this.backdropEl.addClass(i.transitionClass),this.backdropEl.removeClass(i.triggerClass)),this.modalEl=d(i.dialogClass),i.dialogFade&&(this.modalEl.addClass(i.transitionClass),this.modalEl.removeClass(i.triggerClass)),this.handledEscapeKey=function(e){27===e.which&&(a.close(),e.preventDefault(),a.$scope.$apply())},this.handleBackDropClick=function(e){a.close(),e.preventDefault(),a.$scope.$apply()}}var g=i.find("body");return m.prototype.isOpen=function(){return this._open},m.prototype.open=function(e,t){var n=this,a=this.options;if(e&&(a.templateUrl=e),t&&(a.controller=t),!a.template&&!a.templateUrl)throw Error("Dialog.open expected template or templateUrl, neither found. Use options or open method to specify them.");return this._loadResolves().then(function(e){var t=e.$scope=n.$scope=e.$scope?e.$scope:r.$new();if(n.modalEl.html(e.$template),n.options.controller){var a=l(n.options.controller,e);n.modalEl.children().data("ngControllerController",a)}o(n.modalEl)(t),n._addElementsToDom(),setTimeout(function(){n.options.dialogFade&&n.modalEl.addClass(n.options.triggerClass),n.options.backdropFade&&n.backdropEl.addClass(n.options.triggerClass)}),n._bindEvents()}),this.deferred=c.defer(),this.deferred.promise},m.prototype.close=function(e){function t(e){e.removeClass(a.options.triggerClass)}function n(){a._open&&a._onCloseComplete(e)}var a=this,i=this._getFadingElements();if(i.length>0)for(var o=i.length-1;o>=0;o--)u(i[o],t).then(n);else this._onCloseComplete(e)},m.prototype._getFadingElements=function(){var e=[];return this.options.dialogFade&&e.push(this.modalEl),this.options.backdropFade&&e.push(this.backdropEl),e},m.prototype._bindEvents=function(){this.options.keyboard&&g.bind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.bind("click",this.handleBackDropClick)},m.prototype._unbindEvents=function(){this.options.keyboard&&g.unbind("keydown",this.handledEscapeKey),this.options.backdrop&&this.options.backdropClick&&this.backdropEl.unbind("click",this.handleBackDropClick)},m.prototype._onCloseComplete=function(e){this._removeElementsFromDom(),this._unbindEvents(),this.deferred.resolve(e)},m.prototype._addElementsToDom=function(){g.append(this.modalEl),this.options.backdrop&&(0===n.value&&g.append(this.backdropEl),n.value++),this._open=!0},m.prototype._removeElementsFromDom=function(){this.modalEl.remove(),this.options.backdrop&&(n.value--,0===n.value&&this.backdropEl.remove()),this._open=!1},m.prototype._loadResolves=function(){var e,t=[],n=[],i=this;return this.options.template?e=c.when(this.options.template):this.options.templateUrl&&(e=a.get(this.options.templateUrl,{cache:s}).then(function(e){return e.data})),angular.forEach(this.options.resolve||[],function(e,a){n.push(a),t.push(angular.isString(e)?p.get(e):p.invoke(e))}),n.push("$template"),t.push(e),c.all(t).then(function(e){var t={};return angular.forEach(e,function(e,a){t[n[a]]=e}),t.dialog=i,t})},{dialog:function(e){return new m(e)},messageBox:function(e,t,n){return new m({templateUrl:"template/dialog/message.html",controller:"MessageBoxController",resolve:{model:function(){return{title:e,message:t,buttons:n}}}})}}}]}),angular.module("ui.bootstrap.dropdownToggle",[]).directive("dropdownToggle",["$document","$location",function(e){var t=null,n=angular.noop;return{restrict:"CA",link:function(a,i){a.$watch("$location.path",function(){n()}),i.parent().bind("click",function(){n()}),i.bind("click",function(a){var o=i===t;a.preventDefault(),a.stopPropagation(),t&&n(),o||(i.parent().addClass("open"),t=i,n=function(a){a&&(a.preventDefault(),a.stopPropagation()),e.unbind("click",n),i.parent().removeClass("open"),n=angular.noop,t=null},e.bind("click",n))})}}}]),angular.module("ui.bootstrap.modal",["ui.bootstrap.dialog"]).directive("modal",["$parse","$dialog",function(e,t){return{restrict:"EA",terminal:!0,link:function(n,a,i){var o,r=angular.extend({},n.$eval(i.uiOptions||i.bsOptions||i.options)),l=i.modal||i.show;r=angular.extend(r,{template:a.html(),resolve:{$scope:function(){return n}}});var s=t.dialog(r);a.remove(),o=i.close?function(){e(i.close)(n)}:function(){angular.isFunction(e(l).assign)&&e(l).assign(n,!1)},n.$watch(l,function(e){e?s.open().then(function(){o()}):s.isOpen()&&s.close()})}}}]),angular.module("ui.bootstrap.pagination",[]).controller("PaginationController",["$scope","$interpolate",function(e,t){this.currentPage=1,this.noPrevious=function(){return 1===this.currentPage},this.noNext=function(){return this.currentPage===e.numPages},this.isActive=function(e){return this.currentPage===e},this.reset=function(){e.pages=[],this.currentPage=parseInt(e.currentPage,10),this.currentPage>e.numPages&&e.selectPage(e.numPages)};var n=this;e.selectPage=function(t){!n.isActive(t)&&t>0&&e.numPages>=t&&(e.currentPage=t,e.onSelectPage({page:t}))},this.getAttributeValue=function(n,a,i){return angular.isDefined(n)?i?t(n)(e.$parent):e.$parent.$eval(n):a}}]).constant("paginationConfig",{boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last",rotate:!0}).directive("pagination",["paginationConfig",function(e){return{restrict:"EA",scope:{numPages:"=",currentPage:"=",maxSize:"=",onSelectPage:"&"},controller:"PaginationController",templateUrl:"template/pagination/pagination.html",replace:!0,link:function(t,n,a,i){function o(e,t,n,a){return{number:e,text:t,active:n,disabled:a}}var r=i.getAttributeValue(a.boundaryLinks,e.boundaryLinks),l=i.getAttributeValue(a.directionLinks,e.directionLinks),s=i.getAttributeValue(a.firstText,e.firstText,!0),c=i.getAttributeValue(a.previousText,e.previousText,!0),u=i.getAttributeValue(a.nextText,e.nextText,!0),p=i.getAttributeValue(a.lastText,e.lastText,!0),d=i.getAttributeValue(a.rotate,e.rotate);t.$watch("numPages + currentPage + maxSize",function(){i.reset();var e=1,n=t.numPages,a=angular.isDefined(t.maxSize)&&t.maxSize<t.numPages;a&&(d?(e=Math.max(i.currentPage-Math.floor(t.maxSize/2),1),n=e+t.maxSize-1,n>t.numPages&&(n=t.numPages,e=n-t.maxSize+1)):(e=(Math.ceil(i.currentPage/t.maxSize)-1)*t.maxSize+1,n=Math.min(e+t.maxSize-1,t.numPages)));for(var m=e;n>=m;m++){var g=o(m,m,i.isActive(m),!1);t.pages.push(g)}if(a&&!d){if(e>1){var f=o(e-1,"...",!1,!1);t.pages.unshift(f)}if(t.numPages>n){var h=o(n+1,"...",!1,!1);t.pages.push(h)}}if(l){var v=o(i.currentPage-1,c,!1,i.noPrevious());t.pages.unshift(v);var b=o(i.currentPage+1,u,!1,i.noNext());t.pages.push(b)}if(r){var $=o(1,s,!1,i.noPrevious());t.pages.unshift($);var y=o(t.numPages,p,!1,i.noNext());t.pages.push(y)}})}}}]).constant("pagerConfig",{previousText:"« Previous",nextText:"Next »",align:!0}).directive("pager",["pagerConfig",function(e){return{restrict:"EA",scope:{numPages:"=",currentPage:"=",onSelectPage:"&"},controller:"PaginationController",templateUrl:"template/pagination/pager.html",replace:!0,link:function(t,n,a,i){function o(e,t,n,a,i){return{number:e,text:t,disabled:n,previous:s&&a,next:s&&i}}var r=i.getAttributeValue(a.previousText,e.previousText,!0),l=i.getAttributeValue(a.nextText,e.nextText,!0),s=i.getAttributeValue(a.align,e.align);t.$watch("numPages + currentPage",function(){i.reset();var e=o(i.currentPage-1,r,i.noPrevious(),!0,!1);t.pages.unshift(e);var n=o(i.currentPage+1,l,i.noNext(),!1,!0);t.pages.push(n)})}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position"]).provider("$tooltip",function(){function e(e){var t=/[A-Z]/g,n="-";return e.replace(t,function(e,t){return(t?n:"")+e.toLowerCase()})}var t={placement:"top",animation:!0,popupDelay:0},n={mouseenter:"mouseleave",click:"click",focus:"blur"},a={};this.options=function(e){angular.extend(a,e)},this.setTriggers=function(e){angular.extend(n,e)},this.$get=["$window","$compile","$timeout","$parse","$document","$position","$interpolate",function(i,o,r,l,s,c,u){return function(i,p,d){function m(e){var t=e||g.trigger||d,a=n[t]||t;return{show:t,hide:a}}var g=angular.extend({},t,a),f=e(i),h=u.startSymbol(),v=u.endSymbol(),b="<"+f+"-popup "+'title="'+h+"tt_title"+v+'" '+'content="'+h+"tt_content"+v+'" '+'placement="'+h+"tt_placement"+v+'" '+'animation="tt_animation()" '+'is-open="tt_isOpen"'+">"+"</"+f+"-popup>";return{restrict:"EA",scope:!0,link:function(e,t,n){function a(){e.tt_isOpen?d():u()}function u(){e.tt_popupDelay?$=r(f,e.tt_popupDelay):e.$apply(f)}function d(){e.$apply(function(){h()})}function f(){var n,a,i,o;if(e.tt_content){switch(v&&r.cancel(v),w.css({top:0,left:0,display:"block"}),k?(y=y||s.find("body"),y.append(w)):t.after(w),n=k?c.offset(t):c.position(t),a=w.prop("offsetWidth"),i=w.prop("offsetHeight"),e.tt_placement){case"mouse":var l=c.mouse();o={top:l.y,left:l.x};break;case"right":o={top:n.top+n.height/2-i/2,left:n.left+n.width};break;case"bottom":o={top:n.top+n.height,left:n.left+n.width/2-a/2};break;case"left":o={top:n.top+n.height/2-i/2,left:n.left-a};break;default:o={top:n.top-i,left:n.left+n.width/2-a/2}}o.top+="px",o.left+="px",w.css(o),e.tt_isOpen=!0}}function h(){e.tt_isOpen=!1,r.cancel($),angular.isDefined(e.tt_animation)&&e.tt_animation()?v=r(function(){w.remove()},500):w.remove()}var v,$,y,w=o(b)(e),k=angular.isDefined(g.appendToBody)?g.appendToBody:!1,D=m(void 0),x=!1;e.tt_isOpen=!1,n.$observe(i,function(t){e.tt_content=t}),n.$observe(p+"Title",function(t){e.tt_title=t}),n.$observe(p+"Placement",function(t){e.tt_placement=angular.isDefined(t)?t:g.placement}),n.$observe(p+"Animation",function(t){e.tt_animation=angular.isDefined(t)?l(t):function(){return g.animation}}),n.$observe(p+"PopupDelay",function(t){var n=parseInt(t,10);e.tt_popupDelay=isNaN(n)?g.popupDelay:n}),n.$observe(p+"Trigger",function(e){x&&(t.unbind(D.show,u),t.unbind(D.hide,d)),D=m(e),D.show===D.hide?t.bind(D.show,a):(t.bind(D.show,u),t.bind(D.hide,d)),x=!0}),n.$observe(p+"AppendToBody",function(t){k=angular.isDefined(t)?l(t)(e):k}),k&&e.$on("$locationChangeSuccess",function(){e.tt_isOpen&&h()}),e.$on("$destroy",function(){e.tt_isOpen?h():w.remove()})}}}}]}).directive("tooltipPopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(e){return e("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"E",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(e){return e("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$compile","$timeout","$parse","$window","$tooltip",function(e,t,n,a,i){return i("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",["ui.bootstrap.transition"]).constant("progressConfig",{animate:!0,autoType:!1,stackedTypes:["success","info","warning","danger"]}).controller("ProgressBarController",["$scope","$attrs","progressConfig",function(e,t,n){function a(e){return r[e]}var i=angular.isDefined(t.animate)?e.$eval(t.animate):n.animate,o=angular.isDefined(t.autoType)?e.$eval(t.autoType):n.autoType,r=angular.isDefined(t.stackedTypes)?e.$eval("["+t.stackedTypes+"]"):n.stackedTypes;this.makeBar=function(e,t,n){var r=angular.isObject(e)?e.value:e||0,l=angular.isObject(t)?t.value:t||0,s=angular.isObject(e)&&angular.isDefined(e.type)?e.type:o?a(n||0):null;return{from:l,to:r,type:s,animate:i}},this.addBar=function(t){e.bars.push(t),e.totalPercent+=t.to},this.clearBars=function(){e.bars=[],e.totalPercent=0},this.clearBars()}]).directive("progress",function(){return{restrict:"EA",replace:!0,controller:"ProgressBarController",scope:{value:"=percent",onFull:"&",onEmpty:"&"},templateUrl:"template/progressbar/progress.html",link:function(e,t,n,a){e.$watch("value",function(e,t){if(a.clearBars(),angular.isArray(e))for(var n=0,i=e.length;i>n;n++)a.addBar(a.makeBar(e[n],t[n],n));else a.addBar(a.makeBar(e,t))},!0),e.$watch("totalPercent",function(t){t>=100?e.onFull():0>=t&&e.onEmpty()},!0)}}}).directive("progressbar",["$transition",function(e){return{restrict:"EA",replace:!0,scope:{width:"=",old:"=",type:"=",animate:"="},templateUrl:"template/progressbar/bar.html",link:function(t,n){t.$watch("width",function(a){t.animate?(n.css("width",t.old+"%"),e(n,{width:a+"%"})):n.css("width",a+"%")})}}}]),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5}).directive("rating",["ratingConfig","$parse",function(e,t){return{restrict:"EA",scope:{value:"=",onHover:"&",onLeave:"&"},templateUrl:"template/rating/rating.html",replace:!0,link:function(n,a,i){var o=angular.isDefined(i.max)?n.$parent.$eval(i.max):e.max;n.range=[];for(var r=1;o>=r;r++)n.range.push(r);n.rate=function(e){n.readonly||(n.value=e)},n.enter=function(e){n.readonly||(n.val=e),n.onHover({value:e})},n.reset=function(){n.val=angular.copy(n.value),n.onLeave()},n.reset(),n.$watch("value",function(e){n.val=e}),n.readonly=!1,i.readonly&&n.$parent.$watch(t(i.readonly),function(e){n.readonly=!!e})}}}]),angular.module("ui.bootstrap.tabs",[]).directive("tabs",function(){return function(){throw Error("The `tabs` directive is deprecated, please migrate to `tabset`. Instructions can be found at http://github.com/angular-ui/bootstrap/tree/master/CHANGELOG.md")}}).controller("TabsetController",["$scope","$element",function(e){var t=this,n=t.tabs=e.tabs=[];t.select=function(e){angular.forEach(n,function(e){e.active=!1}),e.active=!0},t.addTab=function(e){n.push(e),(1===n.length||e.active)&&t.select(e)},t.removeTab=function(e){var a=n.indexOf(e);if(e.active&&n.length>1){var i=a==n.length-1?a-1:a+1;t.select(n[i])}n.splice(a,1)}}]).directive("tabset",function(){return{restrict:"EA",transclude:!0,replace:!0,require:"^tabset",scope:{},controller:"TabsetController",templateUrl:"template/tabs/tabset.html",compile:function(e,t,n){return function(e,t,a,i){e.vertical=angular.isDefined(a.vertical)?e.$eval(a.vertical):!1,e.type=angular.isDefined(a.type)?e.$parent.$eval(a.type):"tabs",e.direction=angular.isDefined(a.direction)?e.$parent.$eval(a.direction):"top",e.tabsAbove="below"!=e.direction,i.$scope=e,i.$transcludeFn=n}}}}).directive("tab",["$parse","$http","$templateCache","$compile",function(e){return{require:"^tabset",restrict:"EA",replace:!0,templateUrl:"template/tabs/tab.html",transclude:!0,scope:{heading:"@",onSelect:"&select",onDeselect:"&deselect"},controller:function(){},compile:function(t,n,a){return function(t,n,i,o){var r,l;i.active?(r=e(i.active),l=r.assign,t.$parent.$watch(r,function(e){t.active=!!e}),t.active=r(t.$parent)):l=r=angular.noop,t.$watch("active",function(e){l(t.$parent,e),e?(o.select(t),t.onSelect()):t.onDeselect()}),t.disabled=!1,i.disabled&&t.$parent.$watch(e(i.disabled),function(e){t.disabled=!!e
}),t.select=function(){t.disabled||(t.active=!0)},o.addTab(t),t.$on("$destroy",function(){o.removeTab(t)}),t.active&&l(t.$parent,!0),t.$transcludeFn=a}}}}]).directive("tabHeadingTransclude",[function(){return{restrict:"A",require:"^tab",link:function(e,t){e.$watch("headingElement",function(e){e&&(t.html(""),t.append(e))})}}}]).directive("tabContentTransclude",["$compile","$parse",function(){function e(e){return e.tagName&&(e.hasAttribute("tab-heading")||e.hasAttribute("data-tab-heading")||"tab-heading"===e.tagName.toLowerCase()||"data-tab-heading"===e.tagName.toLowerCase())}return{restrict:"A",require:"^tabset",link:function(t,n,a){var i=t.$eval(a.tabContentTransclude);i.$transcludeFn(i.$parent,function(t){angular.forEach(t,function(t){e(t)?i.headingElement=t:n.append(t)})})}}}]).directive("tabsetTitles",function(){return{restrict:"A",require:"^tabset",templateUrl:"template/tabs/tabset-titles.html",replace:!0,link:function(e,t,n,a){e.$eval(n.tabsetTitles)?a.$transcludeFn(a.$scope.$parent,function(e){t.append(e)}):t.remove()}}}),angular.module("ui.bootstrap.timepicker",[]).filter("pad",function(){return function(e){return angular.isDefined(e)&&2>(""+e).length&&(e="0"+e),e}}).constant("timepickerConfig",{hourStep:1,minuteStep:1,showMeridian:!0,meridians:["AM","PM"],readonlyInput:!1,mousewheel:!0}).directive("timepicker",["padFilter","$parse","timepickerConfig",function(e,t,n){return{restrict:"EA",require:"ngModel",replace:!0,templateUrl:"template/timepicker/timepicker.html",scope:{model:"=ngModel"},link:function(a,i,o){function r(){var e=parseInt(a.hours,10),t=a.showMeridian?e>0&&13>e:e>=0&&24>e;return t?(a.showMeridian&&(12===e&&(e=0),a.meridian===u[1]&&(e+=12)),e):void 0}function l(){var t=c.getHours();a.showMeridian&&(t=0===t||12===t?12:t%12),a.hours="h"===b?t:e(t),a.validHours=!0;var n=c.getMinutes();a.minutes="m"===b?n:e(n),a.validMinutes=!0,a.meridian=a.showMeridian?12>c.getHours()?u[0]:u[1]:"",b=!1}function s(e){var t=new Date(c.getTime()+6e4*e);c.setHours(t.getHours()),c.setMinutes(t.getMinutes()),a.model=new Date(c)}var c=new Date,u=n.meridians,p=n.hourStep;o.hourStep&&a.$parent.$watch(t(o.hourStep),function(e){p=parseInt(e,10)});var d=n.minuteStep;o.minuteStep&&a.$parent.$watch(t(o.minuteStep),function(e){d=parseInt(e,10)}),a.showMeridian=n.showMeridian,o.showMeridian&&a.$parent.$watch(t(o.showMeridian),function(e){if(a.showMeridian=!!e,a.model)l();else{var t=new Date(c),n=r();angular.isDefined(n)&&t.setHours(n),a.model=new Date(t)}});var m=i.find("input"),g=m.eq(0),f=m.eq(1),h=angular.isDefined(o.mousewheel)?a.$eval(o.mousewheel):n.mousewheel;if(h){var v=function(e){e.originalEvent&&(e=e.originalEvent);var t=e.wheelDelta?e.wheelDelta:-e.deltaY;return e.detail||t>0};g.bind("mousewheel wheel",function(e){a.$apply(v(e)?a.incrementHours():a.decrementHours()),e.preventDefault()}),f.bind("mousewheel wheel",function(e){a.$apply(v(e)?a.incrementMinutes():a.decrementMinutes()),e.preventDefault()})}var b=!1;a.readonlyInput=angular.isDefined(o.readonlyInput)?a.$eval(o.readonlyInput):n.readonlyInput,a.readonlyInput?(a.updateHours=angular.noop,a.updateMinutes=angular.noop):(a.updateHours=function(){var e=r();angular.isDefined(e)?(b="h",null===a.model&&(a.model=new Date(c)),a.model.setHours(e)):(a.model=null,a.validHours=!1)},g.bind("blur",function(){a.validHours&&10>a.hours&&a.$apply(function(){a.hours=e(a.hours)})}),a.updateMinutes=function(){var e=parseInt(a.minutes,10);e>=0&&60>e?(b="m",null===a.model&&(a.model=new Date(c)),a.model.setMinutes(e)):(a.model=null,a.validMinutes=!1)},f.bind("blur",function(){a.validMinutes&&10>a.minutes&&a.$apply(function(){a.minutes=e(a.minutes)})})),a.$watch(function(){return+a.model},function(e){!isNaN(e)&&e>0&&(c=new Date(e),l())}),a.incrementHours=function(){s(60*p)},a.decrementHours=function(){s(60*-p)},a.incrementMinutes=function(){s(d)},a.decrementMinutes=function(){s(-d)},a.toggleMeridian=function(){s(720*(12>c.getHours()?1:-1))}}}}]),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position"]).factory("typeaheadParser",["$parse",function(e){var t=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(n){var a=n.match(t);if(!a)throw Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '"+n+"'.");return{itemName:a[3],source:e(a[4]),viewMapper:e(a[2]||a[1]),modelMapper:e(a[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$timeout","$document","$position","typeaheadParser",function(e,t,n,a,i,o,r){var l=[9,13,27,38,40];return{require:"ngModel",link:function(s,c,u,p){var d=s.$eval(u.typeaheadMinLength)||1,m=s.$eval(u.typeaheadWaitMs)||0,g=s.$eval(u.typeaheadEditable)!==!1,f=t(u.typeaheadLoading).assign||angular.noop,h=t(u.typeaheadOnSelect),v=u.typeaheadInputFormatter?t(u.typeaheadInputFormatter):void 0,b=t(u.ngModel).assign,$=r.parse(u.typeahead),y=angular.element("<typeahead-popup></typeahead-popup>");y.attr({matches:"matches",active:"activeIdx",select:"select(activeIdx)",query:"query",position:"position"}),angular.isDefined(u.typeaheadTemplateUrl)&&y.attr("template-url",u.typeaheadTemplateUrl);var w=s.$new();s.$on("$destroy",function(){w.$destroy()});var k=function(){w.matches=[],w.activeIdx=-1},D=function(e){var t={$viewValue:e};f(s,!0),n.when($.source(w,t)).then(function(n){if(e===p.$viewValue){if(n.length>0){w.activeIdx=0,w.matches.length=0;for(var a=0;n.length>a;a++)t[$.itemName]=n[a],w.matches.push({label:$.viewMapper(w,t),model:n[a]});w.query=e,w.position=o.position(c),w.position.top=w.position.top+c.prop("offsetHeight")}else k();f(s,!1)}},function(){k(),f(s,!1)})};k(),w.query=void 0;var x;p.$parsers.push(function(e){return k(),e&&e.length>=d&&(m>0?(x&&a.cancel(x),x=a(function(){D(e)},m)):D(e)),g?e:void 0}),p.$formatters.push(function(e){var t,n,a={};return v?(a.$model=e,v(s,a)):(a[$.itemName]=e,t=$.viewMapper(s,a),n=$.viewMapper(s,{}),t!==n?t:e)}),w.select=function(e){var t,n,a={};a[$.itemName]=n=w.matches[e].model,t=$.modelMapper(s,a),b(s,t),h(s,{$item:n,$model:t,$label:$.viewMapper(s,a)}),k(),c[0].focus()},c.bind("keydown",function(e){0!==w.matches.length&&-1!==l.indexOf(e.which)&&(e.preventDefault(),40===e.which?(w.activeIdx=(w.activeIdx+1)%w.matches.length,w.$digest()):38===e.which?(w.activeIdx=(w.activeIdx?w.activeIdx:w.matches.length)-1,w.$digest()):13===e.which||9===e.which?w.$apply(function(){w.select(w.activeIdx)}):27===e.which&&(e.stopPropagation(),k(),w.$digest()))}),i.bind("click",function(){k(),w.$digest()}),c.after(e(y)(w))}}}]).directive("typeaheadPopup",function(){return{restrict:"E",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead-popup.html",link:function(e,t,n){e.templateUrl=n.templateUrl,e.isOpen=function(){return e.matches.length>0},e.isActive=function(t){return e.active==t},e.selectActive=function(t){e.active=t},e.selectMatch=function(t){e.select({activeIdx:t})}}}}).directive("typeaheadMatch",["$http","$templateCache","$compile","$parse",function(e,t,n,a){return{restrict:"E",scope:{index:"=",match:"=",query:"="},link:function(i,o,r){var l=a(r.templateUrl)(i.$parent)||"template/typeahead/typeahead-match.html";e.get(l,{cache:t}).success(function(e){o.replaceWith(n(e.trim())(i))})}}}]).filter("typeaheadHighlight",function(){function e(e){return e.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(t,n){return n?t.replace(RegExp(e(n),"gi"),"<strong>$&</strong>"):n}}),angular.module("template/accordion/accordion-group.html",[]).run(["$templateCache",function(e){e.put("template/accordion/accordion-group.html",'<div class="accordion-group">\n  <div class="accordion-heading" ><a class="accordion-toggle" ng-click="isOpen = !isOpen" accordion-transclude="heading">{{heading}}</a></div>\n  <div class="accordion-body" collapse="!isOpen">\n    <div class="accordion-inner" ng-transclude></div>  </div>\n</div>')}]),angular.module("template/accordion/accordion.html",[]).run(["$templateCache",function(e){e.put("template/accordion/accordion.html",'<div class="accordion" ng-transclude></div>')}]),angular.module("template/alert/alert.html",[]).run(["$templateCache",function(e){e.put("template/alert/alert.html","<div class='alert' ng-class='type && \"alert-\" + type'>\n    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n    <div ng-transclude></div>\n</div>\n")}]),angular.module("template/carousel/carousel.html",[]).run(["$templateCache",function(e){e.put("template/carousel/carousel.html",'<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel">\n    <ol class="carousel-indicators" ng-show="slides().length > 1">\n        <li ng-repeat="slide in slides()" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a ng-click="prev()" class="carousel-control left" ng-show="slides().length > 1">&lsaquo;</a>\n    <a ng-click="next()" class="carousel-control right" ng-show="slides().length > 1">&rsaquo;</a>\n</div>\n')}]),angular.module("template/carousel/slide.html",[]).run(["$templateCache",function(e){e.put("template/carousel/slide.html","<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item\" ng-transclude></div>\n")}]),angular.module("template/datepicker/datepicker.html",[]).run(["$templateCache",function(e){e.put("template/datepicker/datepicker.html",'<table class="datepicker">\n  <thead>\n    <tr class="text-center">\n      <th><button type="button" class="btn pull-left" ng-click="move(-1)"><i class="icon-chevron-left"></i></button></th>\n      <th colspan="{{rows[0].length - 2 + showWeekNumbers}}"><button type="button" class="btn btn-block" ng-click="toggleMode()"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn pull-right" ng-click="move(1)"><i class="icon-chevron-right"></i></button></th>\n    </tr>\n    <tr class="text-center" ng-show="labels.length > 0">\n      <th ng-show="showWeekNumbers">#</th>\n      <th ng-repeat="label in labels">{{label}}</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows">\n      <td ng-show="showWeekNumbers" class="text-center"><em>{{ getWeekNumber(row) }}</em></td>\n      <td ng-repeat="dt in row" class="text-center">\n        <button type="button" style="width:100%;" class="btn" ng-class="{\'btn-info\': dt.selected}" ng-click="select(dt.date)" ng-disabled="dt.disabled"><span ng-class="{muted: dt.secondary}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')}]),angular.module("template/datepicker/popup.html",[]).run(["$templateCache",function(e){e.put("template/datepicker/popup.html",'<ul class="dropdown-menu" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top+\'px\', left: position.left+\'px\'}" class="dropdown-menu">\n	<li ng-transclude></li>\n </ul>')}]),angular.module("template/dialog/message.html",[]).run(["$templateCache",function(e){e.put("template/dialog/message.html",'<div class="modal-header">\n	<h3>{{ title }}</h3>\n</div>\n<div class="modal-body">\n	<p>{{ message }}</p>\n</div>\n<div class="modal-footer">\n	<button ng-repeat="btn in buttons" ng-click="close(btn.result)" class="btn" ng-class="btn.cssClass">{{ btn.label }}</button>\n</div>\n')}]),angular.module("template/modal/backdrop.html",[]).run(["$templateCache",function(e){e.put("template/modal/backdrop.html",'<div class="modal-backdrop fade in"></div>')}]),angular.module("template/modal/window.html",[]).run(["$templateCache",function(e){e.put("template/modal/window.html",'<div class="modal in" ng-transclude></div>')}]),angular.module("template/pagination/pager.html",[]).run(["$templateCache",function(e){e.put("template/pagination/pager.html",'<div class="pager">\n  <ul>\n    <li ng-repeat="page in pages" ng-class="{disabled: page.disabled, previous: page.previous, next: page.next}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  </ul>\n</div>\n')}]),angular.module("template/pagination/pagination.html",[]).run(["$templateCache",function(e){e.put("template/pagination/pagination.html",'<div class="pagination"><ul>\n  <li ng-repeat="page in pages" ng-class="{active: page.active, disabled: page.disabled}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  </ul>\n</div>\n')}]),angular.module("template/tooltip/tooltip-html-unsafe-popup.html",[]).run(["$templateCache",function(e){e.put("template/tooltip/tooltip-html-unsafe-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind-html-unsafe="content"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-popup.html",[]).run(["$templateCache",function(e){e.put("template/tooltip/tooltip-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')}]),angular.module("template/popover/popover.html",[]).run(["$templateCache",function(e){e.put("template/popover/popover.html",'<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')}]),angular.module("template/progressbar/bar.html",[]).run(["$templateCache",function(e){e.put("template/progressbar/bar.html",'<div class="bar" ng-class=\'type && "bar-" + type\'></div>')}]),angular.module("template/progressbar/progress.html",[]).run(["$templateCache",function(e){e.put("template/progressbar/progress.html",'<div class="progress"><progressbar ng-repeat="bar in bars" width="bar.to" old="bar.from" animate="bar.animate" type="bar.type"></progressbar></div>')}]),angular.module("template/rating/rating.html",[]).run(["$templateCache",function(e){e.put("template/rating/rating.html",'<span ng-mouseleave="reset()">\n	<i ng-repeat="number in range" ng-mouseenter="enter(number)" ng-click="rate(number)" ng-class="{\'icon-star\': number <= val, \'icon-star-empty\': number > val}"></i>\n</span>')}]),angular.module("template/tabs/pane.html",[]).run(["$templateCache",function(e){e.put("template/tabs/pane.html",'<div class="tab-pane" ng-class="{active: selected}" ng-show="selected" ng-transclude></div>\n')}]),angular.module("template/tabs/tab.html",[]).run(["$templateCache",function(e){e.put("template/tabs/tab.html",'<li ng-class="{active: active, disabled: disabled}">\n  <a ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n')}]),angular.module("template/tabs/tabs.html",[]).run(["$templateCache",function(e){e.put("template/tabs/tabs.html",'<div class="tabbable">\n  <ul class="nav nav-tabs">\n    <li ng-repeat="pane in panes" ng-class="{active:pane.selected}">\n      <a ng-click="select(pane)">{{pane.heading}}</a>\n    </li>\n  </ul>\n  <div class="tab-content" ng-transclude></div>\n</div>\n')}]),angular.module("template/tabs/tabset-titles.html",[]).run(["$templateCache",function(e){e.put("template/tabs/tabset-titles.html","<ul class=\"nav {{type && 'nav-' + type}}\" ng-class=\"{'nav-stacked': vertical}\">\n</ul>\n")}]),angular.module("template/tabs/tabset.html",[]).run(["$templateCache",function(e){e.put("template/tabs/tabset.html",'\n<div class="tabbable" ng-class="{\'tabs-right\': direction == \'right\', \'tabs-left\': direction == \'left\', \'tabs-below\': direction == \'below\'}">\n  <div tabset-titles="tabsAbove"></div>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n  <div tabset-titles="!tabsAbove"></div>\n</div>\n')}]),angular.module("template/timepicker/timepicker.html",[]).run(["$templateCache",function(e){e.put("template/timepicker/timepicker.html",'<table class="form-inline">\n	<tr class="text-center">\n		<td><a ng-click="incrementHours()" class="btn btn-link"><i class="icon-chevron-up"></i></a></td>\n		<td>&nbsp;</td>\n		<td><a ng-click="incrementMinutes()" class="btn btn-link"><i class="icon-chevron-up"></i></a></td>\n		<td ng-show="showMeridian"></td>\n	</tr>\n	<tr>\n		<td class="control-group" ng-class="{\'error\': !validHours}"><input type="text" ng-model="hours" ng-change="updateHours()" class="span1 text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2" /></td>\n		<td>:</td>\n		<td class="control-group" ng-class="{\'error\': !validMinutes}"><input type="text" ng-model="minutes" ng-change="updateMinutes()" class="span1 text-center" ng-readonly="readonlyInput" maxlength="2"></td>\n		<td ng-show="showMeridian"><button ng-click="toggleMeridian()" class="btn text-center">{{meridian}}</button></td>\n	</tr>\n	<tr class="text-center">\n		<td><a ng-click="decrementHours()" class="btn btn-link"><i class="icon-chevron-down"></i></a></td>\n		<td>&nbsp;</td>\n		<td><a ng-click="decrementMinutes()" class="btn btn-link"><i class="icon-chevron-down"></i></a></td>\n		<td ng-show="showMeridian"></td>\n	</tr>\n</table>')}]),angular.module("template/typeahead/typeahead-match.html",[]).run(["$templateCache",function(e){e.put("template/typeahead/typeahead-match.html",'<a tabindex="-1" ng-bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')}]),angular.module("template/typeahead/typeahead-popup.html",[]).run(["$templateCache",function(e){e.put("template/typeahead/typeahead-popup.html",'<ul class="typeahead dropdown-menu" ng-style="{display: isOpen()&&\'block\' || \'none\', top: position.top+\'px\', left: position.left+\'px\'}">\n    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)">\n        <typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></typeahead-match>\n    </li>\n</ul>')}]),angular.module("template/typeahead/typeahead.html",[]).run(["$templateCache",function(e){e.put("template/typeahead/typeahead.html",'<ul class="typeahead dropdown-menu" ng-style="{display: isOpen()&&\'block\' || \'none\', top: position.top+\'px\', left: position.left+\'px\'}">\n    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)">\n        <a tabindex="-1" ng-click="selectMatch($index)" ng-bind-html-unsafe="match.label | typeaheadHighlight:query"></a>\n    </li>\n</ul>')}]);// Generated by CoffeeScript 1.6.3
var sivaui;

sivaui = angular.module('siva.directives', ['ui.bootstrap']);

sivaui.directive('pageClass', function($route, $timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attributes) {
      return $timeout(function() {
        return scope.$watch(function() {
          return $route.current;
        }, function(current) {
          element.removeClass();
          if (current.$$route) {
            if (current.$$route.pageClass) {
              return element.addClass(current.$$route.pageClass);
            }
          }
        });
      }, 0, false);
    }
  };
});

sivaui.directive('pageTitle', function($route, $timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attributes) {
      return $timeout(function() {
        return scope.$watch(function() {
          return $route.current;
        }, function(current) {
          element.html('');
          if (current.$$route) {
            if (current.$$route.title) {
              return element.html('Siva: ' + current.$$route.title);
            }
          }
        });
      }, 0, false);
    }
  };
});

sivaui.filter('genre', function() {
  return function(text) {
    if (text) {
      if (text.toLowerCase() === 'm') {
        return 'Masculino';
      } else {
        return 'Femenino';
      }
    }
  };
});

sivaui.filter('laterality', function() {
  return function(text) {
    if (text) {
      if (text.toLowerCase() === 'i') {
        return 'Izquierda';
      } else {
        return 'Derecha';
      }
    }
  };
});

sivaui.filter('pre', function() {
  return function(text) {
    var element;
    if (text) {
      element = text.replace(/[\r\n]/g, '<br/>');
      return element;
    }
  };
});

sivaui.filter('truncate', function() {
  return function(text, length, end) {
    if (isNaN(length)) {
      length = 10;
    }
    if (end === void 0) {
      end = "...";
    }
    if (text.length <= length || text.length - end.length <= length) {
      return text;
    } else {
      return String(text).substring(0, length - end.length) + end;
    }
  };
});

sivaui.filter('properties', function() {
  return function(listado, propiedades) {
    var item, item_op, nuevo, opciones, opt, _i, _j, _len, _len1;
    if (propiedades) {
      opciones = [];
      nuevo = [];
      angular.forEach(propiedades, function(value, key) {
        if (value === true) {
          return opciones.push(key);
        }
      });
      for (_i = 0, _len = listado.length; _i < _len; _i++) {
        item = listado[_i];
        for (_j = 0, _len1 = opciones.length; _j < _len1; _j++) {
          opt = opciones[_j];
          item_op = item[opt];
          if (item_op === "0" || item_op === "N/A") {
            item = false;
          }
        }
        if (item) {
          nuevo.push(item);
        }
      }
      return nuevo;
    }
    return listado;
  };
});

sivaui.filter('filterUser', function() {
  return function(listado, argumento) {
    var nuevo, user, _i, _j, _k, _len, _len1, _len2;
    nuevo = [];
    if (argumento) {
      switch (argumento) {
        case 'new':
          for (_i = 0, _len = listado.length; _i < _len; _i++) {
            user = listado[_i];
            if (user.is_new === '1' && user.deleted_at === null) {
              nuevo.push(user);
            }
          }
          break;
        case 'active':
          for (_j = 0, _len1 = listado.length; _j < _len1; _j++) {
            user = listado[_j];
            if (user.is_new !== '1' && user.deleted_at === null) {
              nuevo.push(user);
            }
          }
          break;
        case 'deleted':
          for (_k = 0, _len2 = listado.length; _k < _len2; _k++) {
            user = listado[_k];
            if (user.deleted_at !== null) {
              nuevo.push(user);
            }
          }
      }
      console.dir(nuevo);
      return nuevo;
    } else {
      return listado;
    }
  };
});

sivaui.filter('findcorpse', function() {
  return function(listado, argumento) {
    var certificate, corpse, fullname, nuevo, _i, _len;
    if (argumento) {
      argumento = argumento.toLowerCase();
      nuevo = [];
      for (_i = 0, _len = listado.length; _i < _len; _i++) {
        corpse = listado[_i];
        certificate = corpse.num_death_certificate.toLowerCase();
        fullname = corpse.fullname.toLowerCase();
        if (certificate.search(argumento) !== -1 || fullname.search(argumento) !== -1) {
          nuevo.push(corpse);
        }
      }
      return nuevo;
    }
    return listado;
  };
});

sivaui.filter('findvariation', function() {
  return function(listado, argumento) {
    var certificate, corpsename, name, nuevo, variation, _i, _len;
    if (argumento) {
      argumento = argumento.toLowerCase();
      nuevo = [];
      for (_i = 0, _len = listado.length; _i < _len; _i++) {
        variation = listado[_i];
        certificate = variation.corpse.num_death_certificate.toLowerCase();
        corpsename = (variation.corpse.name + ' ' + variation.corpse.lastname).toLowerCase();
        name = variation.variation.name.toLowerCase();
        if (certificate.search(argumento) !== -1 || name.search(argumento) !== -1 || corpsename.search(argumento) !== -1) {
          nuevo.push(variation);
        }
      }
      return nuevo;
    }
    return listado;
  };
});

sivaui.filter('startFrom', function() {
  return function(input, start) {
    start = +start;
    return input.slice(start);
  };
});

sivaui.filter('svDate', function($filter) {
  var format, standardDateFilterFn;
  standardDateFilterFn = $filter('date');
  format = function(dateToFormat) {
    var d, t;
    t = dateToFormat.split(/[- :]/);
    d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
    return standardDateFilterFn(d, 'MMM d, y');
  };
  return format;
});

sivaui.directive('fullscreenBox', function($compile, $timeout) {
  return {
    restrict: 'C',
    link: function(scope, element, attributes) {
      var container, format, functions, message;
      message = {};
      container = angular.element('<div class="container"></div>');
      format = function(container) {
        var closeButton;
        element.append(container);
        if (message.close === true) {
          closeButton = '<a href="" title="Cerrar" ng-click="functions.hide()" class="close"></a>';
          container.append($compile(closeButton)(scope));
        }
        element.append(container);
        return element.addClass('show');
      };
      functions = {
        show: function() {
          if (message.scope) {
            container.append($compile(message.content)(message.scope));
          } else {
            container.append(message.content);
          }
          return format(container);
        },
        spinner: function() {
          var content;
          content = "<div class=\"spinner\">\n	" + message.content + "\n	<span class=\"ball first\"></span>\n	<span class=\"ball\"></span>\n	<span class=\"ball last\"></span>\n</div>";
          container.append(content);
          return format(container);
        },
        hide: function() {
          element.removeClass('show');
          return element.html('');
        }
      };
      return scope.$on('fullscreen-box', function(event, message) {
        var fn;
        message = message;
        functions.hide();
        fn = functions[message.action];
        return fn();
      });
    }
  };
});

sivaui.directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      elm.bind('blur', function() {
        return scope.$apply(function() {
          return ctrl.$setViewValue(elm.html());
        });
      });
      ctrl.$render = function(value) {
        return elm.html(value);
      };
      return ctrl.$setViewValue(elm.html());
    }
  };
});

sivaui.directive('photoTag', function($compile, $timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      scope.editarLabel = true;
      return $timeout(function() {
        var addTag, checksize, imageContainer, img, labelsBox, labelsContainer, overlay, tag, tagElm;
        tagElm = {};
        tag = {};
        imageContainer = angular.element('<div class="photo-container"></div>');
        labelsBox = "<div class=\"labels-container\" ng-show=\"!editarLabel\">\n	<div class=\"label\" ng-repeat=\"tag in tags\" style=\"top:{{tag.top}}px;left:{{tag.left}}px;width:{{tag.width}}px;height:{{tag.height}}px;\">\n		<div class=\"text\">{{tag.description}}</div>\n	</div>\n</div>";
        labelsContainer = angular.element($compile(labelsBox)(scope));
        overlay = angular.element($compile('<div class="overlay" ng-show="editarLabel && found.owner">\
							<div class="tag" title="{{tag.description}}" ng-repeat="tag in tags" tag="tag">\
							<span class="delete" ng-click="delete()"> <i class="icon-trash"></i> Eliminar</span>\
\
							</div>\
							</div>')(scope));
        img = element[0];
        checksize = function() {
          overlay.css('height', "" + img.height + "px");
          overlay.css('width', "" + img.width + "px");
          labelsContainer.css('height', "" + img.height + "px");
          return labelsContainer.css('width', "" + img.width + "px");
        };
        img.onload = function() {
          element.wrap(imageContainer);
          element.after(labelsContainer);
          element.after(overlay);
          return checksize();
        };
        addTag = function(event) {
          tag = {
            top: event.offsetY,
            left: event.offsetX,
            height: 120,
            width: 120
          };
          scope.$emit('create-tag', tag);
          overlay.unbind('click');
          overlay.toggleClass('mark-tag');
          return scope.$apply();
        };
        scope.newTag = function(e) {
          e.preventDefault();
          e.stopPropagation();
          overlay.bind('click', addTag);
          checksize();
          return overlay.toggleClass('mark-tag');
        };
        return scope.finishTag = function() {
          return overlay.remove();
        };
      }, 0, false);
    }
  };
});

sivaui.directive('tag', function($compile) {
  return {
    restrict: 'A',
    scope: {
      tag: "="
    },
    link: function(scope, element, attributes) {
      var disable, enable, label, overlay;
      label = "<div class=\"pure-form\">\n	<textarea ng-model=\"tag.description\" ui-event=\"{ blur : 'updateTag(tag)'}\"></textarea>\n</div>";
      element.append($compile(label)(scope));
      element.css('height', scope.tag.height + 'px');
      element.css('width', scope.tag.width + 'px');
      element.css('top', scope.tag.top + 'px');
      element.css('left', scope.tag.left + 'px');
      element.css('position', 'absolute');
      scope.editable = false;
      overlay = angular.element('<div class="overlay-tag"></div>');
      overlay.css('z-index', '1000');
      scope["delete"] = function() {
        var confirmado;
        confirmado = window.confirm('¿Esta seguro de eliminar la etiqueta?');
        if (confirmado) {
          return scope.tag.$delete(function(success) {
            console.dir(success);
            scope.$emit('notification', {
              t: 'neutral',
              m: success.message
            });
            element.remove();
            return disable();
          }, function(error) {
            return console.dir(error.data);
          });
        }
      };
      enable = function() {
        if (!scope.editable) {
          element.css('z-index', '1001');
          element.addClass('editable');
          element.after(overlay);
          scope.editable = true;
          element.draggable({
            containment: "parent",
            stop: function(e, el) {
              element.css('position', 'absolute');
              scope.tag.top = el.position.top;
              scope.tag.left = el.position.left;
              scope.$emit('update-tag', scope.tag);
              return scope.$apply();
            }
          });
          element.resizable({
            handles: "n, ne, nw, e, s, se, sw, w",
            containment: "parent",
            stop: function(e, el) {
              element.css('position', 'absolute');
              scope.tag.top = el.position.top;
              scope.tag.left = el.position.left;
              scope.tag.height = el.size.height;
              scope.tag.width = el.size.width;
              scope.$emit('update-tag', scope.tag);
              return scope.$apply();
            }
          });
        }
        return overlay.bind('click', function(e) {
          disable();
          return scope.editable = false;
        });
      };
      disable = function() {
        overlay.remove();
        element.css('z-index', 'auto');
        element.draggable("destroy");
        element.resizable("destroy");
        return element.removeClass('editable');
      };
      element.bind('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        enable();
        return scope.editable = true;
      });
      return scope.updateTag = function(tag) {
        return scope.$emit('update-tag', tag);
      };
    }
  };
});

sivaui.directive('uploader', function($compile, $timeout) {
  return {
    restrict: 'A',
    scope: {
      uploader: '='
    },
    link: function(scope, element, attributes) {
      var input, spinner, triggerDialog, wrapper;
      scope.disable = false;
      wrapper = document.createElement('div');
      wrapper.className = 'uploader-container';
      element.after(wrapper);
      spinner = "<div class=\"spinner\" ng-show=\"disable\">\n	<img src=\"/img/spinner.gif\" alt=\"spinner\">\n	<span class=\"message\">Cargando imagen</span>\n</div>";
      element.after($compile(spinner)(scope));
      angular.element(wrapper).append(element);
      input = document.createElement('input');
      input.type = 'file';
      element.after(input);
      triggerDialog = function(e) {
        e.preventDefault();
        e.stopPropagation();
        return input.click();
      };
      element.bind('click', triggerDialog);
      input.onchange = function(value) {
        return scope.uploader(input.files);
      };
      return scope.$on('uploader', function(e, action) {
        if (action === 'enable') {
          return $timeout(function() {
            return scope.disable = false;
          }, 150);
        } else {
          return scope.disable = true;
        }
      });
    }
  };
});

sivaui.directive('onBottom', function($timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      var checktop, lista;
      lista = element[0];
      checktop = function(event) {
        var position;
        position = (this.scrollHeight - this.scrollTop) - this.clientHeight;
        return $timeout(function() {
          if (position <= 50) {
            return scope.loadMore();
          }
        }, 0, false);
      };
      return lista.onscroll = checktop;
    }
  };
});

sivaui.directive('notification', function($compile, $timeout) {
  return {
    restrict: 'C',
    link: function(scope, elm, attr) {
      var lista, remove;
      scope.alerts = [];
      lista = angular.element("<ul ng-show='alerts'></ul>");
      elm.append(lista);
      remove = function(item) {
        return $timeout(function() {
          return item.remove();
        }, 3500);
      };
      return scope.$on('notification', function(ev, item) {
        item = angular.element("<li class='" + item.t + "'>" + item.m + "</li>");
        lista.append(item);
        return item.bind('ready', remove(item));
      });
    }
  };
});

sivaui.directive('pie', function($timeout) {
  return {
    restrict: 'A',
    scope: {
      pie: "="
    },
    link: function($scope, $elm, $attr) {
      var addLabel, ctx, legendContainer, options, title;
      ctx = $elm[0].getContext("2d");
      options = {
        scaleOverlay: true,
        scaleOverride: true,
        scaleSteps: 10,
        scaleStepWidth: 10,
        scaleStartValue: 0
      };
      legendContainer = angular.element('<div class="report-legend-container"></div>');
      $elm.after(legendContainer);
      title = angular.element('<h2 class="report-title"></h2>');
      $elm.before(title);
      $scope.$watch('pie', function(report) {
        var data, result;
        if (report) {
          legendContainer.html('');
          title.text(report.title);
        }
        if (report.datasets.hombres && report.datasets.mujeres) {
          data = {
            labels: report.labels,
            datasets: [report.datasets.hombres, report.datasets.mujeres]
          };
          result = new Chart(ctx).Bar(data, options);
          addLabel(report.datasets.hombres);
          addLabel(report.datasets.mujeres);
        }
        if (report.datasets.hombres && !report.datasets.mujeres) {
          data = {
            labels: report.labels,
            datasets: [report.datasets.hombres]
          };
          result = new Chart(ctx).Bar(data, options);
          addLabel(report.datasets.hombres);
        }
        if (report.datasets.mujeres && !report.datasets.hombres) {
          data = {
            labels: report.labels,
            datasets: [report.datasets.mujeres]
          };
          result = new Chart(ctx).Bar(data, options);
          return addLabel(report.datasets.mujeres);
        }
      });
      return addLabel = function(d) {
        var legend;
        legend = "<div class=\"report-legend\">\n	<span class=\"color\" style=\"background:" + d.fillColor + "\"></span>\n	<span class=\"title\">" + d.title + "</span>\n</div>";
        return legendContainer.append(legend);
      };
    }
  };
});

sivaui.directive('loader', function($compile) {
  return {
    restrict: 'A',
    scope: {
      loader: "="
    },
    link: function(scope, elm, attr) {
      var template;
      template = "<div class=\"loading-message\" ng-show=\"loader\">\n	<div class=\"center\">\n		<img src=\"/img/spinner.gif\" alt=\"spinner\">\n		Cargando\n	</div>\n</div>";
      return elm.append($compile(template)(scope));
    }
  };
});

sivaui.directive('paginate', function($compile) {
  return {
    restrict: 'A',
    scope: {
      options: '='
    },
    template: "<div class=\"paginate\" ng-show=\"total > 1\">\n	<a href=\"\" ng-click=\"previous()\">&#171;</a>\n	<a href=\"\" ng-click=\"setPage(1)\" ng-class=\"{'active':1 == page}\">1</a>\n	<a ng-repeat=\"link in links\" href=\"\" ng-click=\"setPage(link)\" ng-class=\"{'active':link == page}\">{{link}}</a>\n	<a href=\"\" ng-click=\"setPage(total)\" ng-class=\"{'active':total == page}\">{{total}}</a>\n	<a href=\"\" ng-click=\"next()\">&#187;</a>\n</div>",
    link: function(scope, element, attributes) {
      var addLink, makeLinks;
      scope.$watch('options', function(value) {
        if (value) {
          scope.paginator = value;
          scope.total = scope.paginator.last;
          scope.page = parseInt(scope.paginator.page);
          return makeLinks();
        }
      });
      scope.next = function() {
        if (scope.page < scope.total) {
          scope.page++;
          scope.$emit('paginate', scope.page);
          return makeLinks();
        }
      };
      scope.previous = function() {
        if (scope.page > 1) {
          scope.page = scope.page - 1;
          scope.$emit('paginate', scope.page);
          return makeLinks();
        }
      };
      scope.setPage = function(page) {
        if ((page >= 1) && (page <= scope.total)) {
          scope.page = page;
          scope.$emit('paginate', scope.page);
          return makeLinks();
        }
      };
      scope.newPAge = function() {
        var algo;
        return algo = 'funciona mierda';
      };
      makeLinks = function() {
        var num, page, total, _i, _j, _k, _l, _ref, _ref1, _ref2, _ref3, _ref4, _results;
        scope.links = [];
        page = scope.page;
        total = scope.total;
        if (total > 5) {
          if (page > (total - 3)) {
            for (num = _i = _ref = total - 5, _ref1 = total - 1; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; num = _ref <= _ref1 ? ++_i : --_i) {
              addLink(num);
            }
            return;
          }
          if (page > 3) {
            for (num = _j = _ref2 = page - 2, _ref3 = page + 2; _ref2 <= _ref3 ? _j <= _ref3 : _j >= _ref3; num = _ref2 <= _ref3 ? ++_j : --_j) {
              addLink(num);
            }
            return;
          }
          for (num = _k = 2; _k <= 6; num = ++_k) {
            addLink(num);
          }
        } else {
          if (total > 2) {
            _results = [];
            for (num = _l = 2, _ref4 = total - 1; 2 <= _ref4 ? _l <= _ref4 : _l >= _ref4; num = 2 <= _ref4 ? ++_l : --_l) {
              _results.push(addLink(num));
            }
            return _results;
          }
        }
      };
      scope.links = [];
      return addLink = function(number) {
        return scope.links.push(number);
      };
    }
  };
});
// Generated by CoffeeScript 1.6.3
var servicios;

servicios = angular.module('siva.services', ['ngResource']);

servicios.factory('formDataObject', function() {
  return function(data) {
    var fd;
    fd = new FormData();
    angular.forEach(data, function(value, key) {
      return fd.append(key, value);
    });
    return fd;
  };
});

servicios.factory('Memory', function() {
  return this;
});

servicios.factory('User', function($resource) {
  return $resource('/user/:id', {
    id: "@id"
  }, {
    update: {
      method: 'PUT'
    },
    get: {
      method: 'GET',
      cache: true
    },
    query: {
      method: 'GET',
      isArray: false
    }
  });
});

servicios.factory('Corpse', function($resource) {
  return $resource('/corpse/:id', {
    id: "@id"
  }, {
    update: {
      method: 'PUT'
    },
    get: {
      method: 'GET',
      cache: true
    },
    query: {
      method: 'GET',
      isArray: false
    }
  });
});

servicios.factory('Sesion', function($resource) {
  return $resource('/sesion/:id', {
    id: "@id"
  });
});

servicios.factory('Variation', function($resource) {
  return $resource('/variation/:id', {
    id: "@id"
  }, {
    update: {
      method: 'PUT'
    },
    get: {
      method: 'GET',
      cache: true
    }
  });
});

servicios.factory('Found', function($resource) {
  return $resource('/found/:id', {
    id: "@id"
  }, {
    update: {
      method: 'PUT'
    },
    get: {
      method: 'GET',
      cache: true
    },
    query: {
      method: 'GET',
      isArray: false
    }
  });
});

servicios.factory('Structure', function($resource) {
  return $resource('/structure/:id', {
    id: "@id"
  }, {
    update: {
      method: 'PUT'
    },
    get: {
      method: 'GET',
      cache: true
    }
  });
});

servicios.factory('Label', function($resource) {
  return $resource('/label/:id', {
    id: "@id"
  }, {
    update: {
      method: 'PUT'
    },
    get: {
      method: 'GET',
      cache: true
    }
  });
});

servicios.factory('Location', function($resource) {
  return $resource('/location/:id', {
    id: "@id"
  });
});

servicios.factory('Rdf', function($resource) {
  return $resource('/rdf/:area');
});
