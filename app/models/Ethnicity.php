<?php

use LaravelBook\Ardent\Ardent;

class Ethnicity extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'ethnicity';

	/*
	* Se desactivan los timestamps del framework
	*/
	public $timestamps = false; 

	/**
	 * Deshabilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = false;

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('description');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array('description' => 'required|min:3|max:45');

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array( 
		'description' => 'Ingrese la etnia que desea almacenar');

	/**
	* Relacion uno a muchos entre etnias y cadaveres
	* @return usuarios
	**/
	public function corpses(){
		return $this->hasMany('Corpse');
	}
}