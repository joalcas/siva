<?php

use LaravelBook\Ardent\Ardent;

class Label extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'labels';


	/**
	 * Deshabilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = false;

	public $timestamps = false;


	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('height', 'width', 'top', 'left', 'description');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array(
		'image_id'	=> 'required|exists:images,id', 
		'height'	=>'required|numeric', 
		'width'		=>'required|numeric', 
		'top'		=>'required|numeric', 
		'left'		=>'required|numeric', 
		'description'=>'required'
	);


	/**
	 * Establece los atributos que se excluyen al mostrar los datos.
	 * @var array
	 */
	protected $hidden = array('deleted_at');


	/**
	* Relacion uno a muchos entre labels e images
	* @return etiquetas de la imagen
	**/
	public function image(){
		return  $this->belongsTo('Image');
	}
}
