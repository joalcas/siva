<?php

use LaravelBook\Ardent\Ardent;

class Typeid extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'typeids';

	/**
	* Relacion uno a muchos entre perfiles y usuarios
	* @return usuarios
	**/
	public function users(){
		return $this->hasMany('User');
	}
}