<?php

use LaravelBook\Ardent\Ardent;

class Variationfound extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'variationfounds';

	/**
	 * Habilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = true;

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('body_region', 'description', 'thickness', 'diameter',
								'length', 'perimeter', 'width', 'laterality', 'form');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array(
		'body_region' 	=>'required|max:200',
		'description'	=>'required',
		'thickness'		=>'numeric', 
		'diameter' 		=>'numeric',
		'length'		=>'numeric', 
		'perimeter'		=>'numeric', 
		'width'			=>'numeric', 
		'laterality'	=>'required',
		'structure_id'	=>'required|exists:structures,id',
		'variation_id'	=>'required|exists:variations,id',
		'user_id'		=>'required|exists:users,id',
		'corpse_id'		=>'required|exists:corpses,id'
	);

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array(
		'body_region.required' 	=>'Identifique la región corporal en la que se encontró la variación',
		'body_region.max' 		=>'Este campo debe tener máximo 200 caracteres',
		'description.required'	=>'Ingrese una descripción de la variación hallada',
		'thickness.numeric'		=>'El grosor debe estar indicado en números (cms)', 
		'diameter.numeric' 		=>'El diametro debe estar indicado en números (cms)',
		'length.numeric'		=>'La longitud debe estar indicado en números (cms)', 
		'perimeter.numeric'		=>'El perimetro debe estar indicado en números (cms)', 
		'width.numeric'			=>'El ancho debe estar indicado en números (cms)',
		'required'				=>'Este campo es requerido',
		'exists'				=>'Este campo no existe en la base de datos, por favor verifique.'
	);

	/**
	 * Establece los atributos que se excluyen al mostrar los datos.
	 * @var array
	 */
	protected $hidden = array('updated_at', 'deleted_at');

	/**
	 * Retorna los reportes de variaciones que coinciden con el filtro ingresado.
	 * @var value
	 */
	public function getByFilter($value){
    	$query = "SELECT V.id, W.name, S.name As structure, body_region ".
    			 "FROM  variationfounds As V, variations As W, structure As S ".
				 "WHERE (V.structure_id = S.id And V.variation_id = W.id ) And ".
				 "W.name LIKE '%" . $value . "%' Or S.name LIKE '%" . $value . "%' ".
				 "Or body_region LIKE '%" . $value . "%' LIMIT 100";

		$result = DB::select($query);
		return Response::json($result);
    }

    /**
	* Relacion uno a muchos entre users y founds
	* @return usuario que lo registro
	**/
	public function user(){
		return $this->belongsTo('User');
	}

	/**
	* Relacion uno a muchos entre corpses y founds
	* @return cadaver al que pertenece
	**/
	public function corpse(){
		return $this->belongsTo('Corpse');
	}

	/**
	* Relacion uno a muchos entre founds y variations
	* @return nombre de la variacion
	**/
	public function variation(){
		return $this->belongsTo('Variation');
	}

	/**
	* Relacion uno a muchos entre founds y structures
	* @return estructura a la que pertenece
	**/
	public function structure(){
		return  $this->belongsTo('Structure');
	}

	/**
	* Relacion uno a muchos entre founds e images
	* @return imagenes del reporte
	**/
	public function images(){
		return $this->hasMany('Image');
	}

	public function delete(){
		$images = $this->images;
		foreach ($images as $img ) {
			$img->delete();
		}
		return parent::delete();
	}
	public function setIsPendingAttribute($value)  {
        $this->attributes['is_pending'] = (bool) $value;
    }
	public function getIsPendingAttribute($value)  {
        return (bool) $this->attributes['is_pending'];
    }
	public function setIsPublicgAttribute($value)  {
        $this->attributes['is_Public'] = (bool) $value;
    }
	public function getIsPublicAttribute($value)  {
        return (bool) $this->attributes['is_public'];
    }
}