<?php

use LaravelBook\Ardent\Ardent;

class Structure extends Ardent {

	/*
	* Se desactivan los timestamps del framework
	*/
	public $timestamps = false; 

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'structures';

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('name');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array('name' => 'required|min:3|max:200');

	/**
	* Relacion uno a muchos entre structures y varitationfounds
	* @return variaciones encontradas que pertenecen a esta estructura anatómica
	**/
	public function varitationfounds(){
		return  $this->hasMany('Variationfound');
	}
}