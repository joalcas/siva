<?php

use LaravelBook\Ardent\Ardent;

class Area extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'areas';

	/*
	* Se desactivan los timestamps del framework
	*/
	public $timestamps = false; 

	/**
	 * Deshabilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = false;

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('name');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array('name' => 'required|unique:areas,name');

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array( 
		'name.required' => 'Ingrese el nombre del area de investigación a la que pertenece',
		'name.unique' 	=> 'El area de investigación ya esta en la lista');

	/**
	* Relacion uno a muchos entre areas y usuarios
	* @return usuarios
	**/
	public function users(){
		return $this->hasMany('User');
	}
}