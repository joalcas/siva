<?php
require_once 'HTTP/Request2.php';
use LaravelBook\Ardent\Ardent;

class Rdf extends Ardent {

	public function getByLevel($struct){
		$struct = str_replace(" ", "_", $struct);
		$url = 'http://localhost:8080/openrdf-sesame/repositories/Taxonomia';
		$request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
		$request->setHeader('Accept', 'application/json');
		$url = $request->getUrl();

		$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
				 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";


		//Se obtiene la "superclase" o estructura padre de la estructura actual.
		$query .= " SELECT DISTINCT ?Estructura WHERE  { dbstruct:".$struct." rdfs:subClassOf ?Estructura . }";	
		$url->setQueryVariable("query",$query);
		$response = $request->send();
		$superclass_array = $this->replacing(json_decode($response->getBody())->results->bindings);
		$superclass = $superclass_array[0];
		$superclass = str_replace(" ", "_", $superclass);
		$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
				 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";
		//Con la superclase obtenida se obtienen las subclases con sus respectivos subarboles
		$query .= " SELECT DISTINCT ?Estructura WHERE  { ?Estructura rdfs:subClassOf* dbstruct:".$superclass." . }";	
		$url->setQueryVariable("query",$query);

		$response = $request->send();
		return $this->replacing(json_decode($response->getBody())->results->bindings);
	}

	public function getByRegion($struct){
		$struct = str_replace(" ", "_", $struct);
		$url = 'http://localhost:8080/openrdf-sesame/repositories/Taxonomia';
		$request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
		$request->setHeader('Accept', 'application/json');
		$url = $request->getUrl();

		$query = "PREFIX dbstruct: <" . "http://dbstruct.owl"  ."#> ".
				 "PREFIX rdfs: <" . "http://www.w3.org/2000/01/rdf-schema" . "#> ";
		//Consulta que devuelve todas las superclases de $struct
		$query .= " SELECT DISTINCT ?Estructura WHERE  { dbstruct:" . $struct . " rdfs:subClassOf* ?Estructura . }";		

		$url->setQueryVariable("query",$query);
		$response = $request->send();
		$superclass_array = $this->replacing(json_decode($response->getBody())->results->bindings);

		foreach ($superclass_array as $str) {
			if($str == 'Abdomen' || $str == 'Cabeza' || $str == 'Cuello' || $str == 'Dorso' 
				|| $str == 'Miembro inferior' || $str == 'Miembro superior' || $str == 'Pelvis' || $str == 'Torax'){
				$struct = $str;
				break;
			}
		}
		//Con la region obtenida se obtienen las subclases
		$struct = str_replace(" ", "_", $struct);
		$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
				 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";
		$query .= " SELECT DISTINCT ?Estructura WHERE  { ?Estructura rdfs:subClassOf* dbstruct:".$struct."  }";	
			$url->setQueryVariable("query",$query);
		$response = $request->send();
		return $this->replacing(json_decode($response->getBody())->results->bindings);
	}

	public function getByName($struct){
		$struct = str_replace(" ", "_", $struct);
		$url = 'http://localhost:8080/openrdf-sesame/repositories/Taxonomia';
		$request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
		$request->setHeader('Accept', 'application/json');
		$url = $request->getUrl();

		$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
				 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";


		$array_nombre = explode("_", $struct);
		$array_temp = array();
		$contador = 0;
		foreach ($array_nombre as $word) {
			$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
					 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";
			$query .= ' SELECT DISTINCT ?Estructura WHERE  { ?Estructura rdfs:subClassOf* dbstruct:Cuerpo_humano .  FILTER regex(str(?Estructura), '.'"'.$word.'")  }';
			$url->setQueryVariable("query",$query);
			$response = $request->send();
			$array_temp[$contador] = $this->replacing(json_decode($response->getBody())->results->bindings);
			$contador = $contador+1;
		}					
		foreach ($array_temp as $res ) {
			$result = array_merge($result, $res);
		}
		return $result;
	}

	public function getSubstructures($struct){
		$struct = str_replace(" ", "_", $struct);
		$url = 'http://localhost:8080/openrdf-sesame/repositories/Taxonomia';
		$request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
		$request->setHeader('Accept', 'application/json');
		$url = $request->getUrl();

		$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
				 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";


		$query .= " SELECT DISTINCT ?Estructura WHERE  { ?Estructura rdfs:subClassOf* dbstruct:".$struct." . }";	
		$url->setQueryVariable("query",$query);
		$response = $request->send();
		$result = $this->replacing(json_decode($response->getBody())->results->bindings);
		array_shift($result);
		return $result;
	}

	public function getDefault($struct){
		$struct = str_replace(" ", "_", $struct);
		$url = 'http://localhost:8080/openrdf-sesame/repositories/Taxonomia';
		$request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
		$request->setHeader('Accept', 'application/json');
		$url = $request->getUrl();

		$query = "PREFIX dbstruct: <"."http://dbstruct.owl"."#> ".
				 "PREFIX rdfs: <"."http://www.w3.org/2000/01/rdf-schema"."#> ";



		$query .= " SELECT DISTINCT ?Estructura WHERE  { ?Estructura rdfs:subClassOf dbstruct:".$struct." . }";
		$url->setQueryVariable("query",$query);
		$response = $request->send();
		return $this->replacing(json_decode($response->getBody())->results->bindings);
	}

	/**
	 * Reemplaza los nombres de estructuras contenidos en un array de objetos tipo Estructura
	 * @return $array_temp que contiene los nombres de las estructuras contenidas en $array
	 */
	private function replacing($array){
		$array_temp = array();

		foreach ($array as $str) {
			$var = str_replace("http://dbstruct.owl#", "", $str->Estructura->value);
			$array_temp[] = str_replace("_", " ", $var);
		}

		return $array_temp;
	}

	/**
	 * Simplifica un $array de labels devuelto por una consulta SPARQL
	 * @return $array_temp que contiene los nombres de las estructuras contenidas en $array
	 */
	private function simplify($array){
		$array_temp = array();
		foreach ($array as $str) {
			$array_temp[] = $str->label->value;
		}
		return $array_temp;
	}	
}