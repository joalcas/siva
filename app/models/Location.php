<?php

use LaravelBook\Ardent\Ardent;

class Location extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'locations';

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('region', 'department', 'city', 'institution');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array(
		'region' 		=> 'required',
		'department'	=> 'required',
		'city' 			=> 'required',
		'institution' 	=> 'required'
	);

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array( 'required' => 'Por favor rellene todos los campos');

	/**
	* Relacion uno a muchos entre locations y usuarios
	* @return usuarios
	**/
	public function users(){
		return  $this->hasMany('User');
	}

	/**
	* Relacion uno a muchos entre locations y corpses
	* @return cadaveres registrados
	**/
	public function corpses(){
		return  $this->hasMany('Corpse');
	}
}