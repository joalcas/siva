<?php

use LaravelBook\Ardent\Ardent;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Ardent implements UserInterface, RemindableInterface {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Habilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = true;

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('name', 'lastname', 'phone');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array(
			'id_num' 	=> 'required|unique:users,id_num',
			'location_id'=> 'required|exists:locations,id',
			'typeid_id' => 'required|exists:typeids,id',
			'profile_id'=> 'required|exists:profiles,id',
			'area_id' 	=> 'required|exists:areas,id',
			'name' 		=> 'required|max:45',
			'lastname' 	=> 'required|max:45',
			'password' 	=> 'required|min:4',
			'email' 	=> 'required|email|unique:users,email'
		);

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array(
			'id_num.required' 		=> 'Ingrese su número de identificación',
			'id_num.unique' 		=> 'Este número de identificación ya se encuentra registrado',
			'location_id.required' 	=> 'Debe ingresar su ubicación',
			'typeid_id.required' 	=> 'Debe indicar el tipo de identificación',
			'area_id.required'		=> 'Indique el areá de investigación a la que pertenece',
			'name.required'			=> 'Ingrese su nombre',
			'name.max'				=> 'El nombre debe tener máximo 45 caracteres',
			'lastname.required'		=> 'Ingrese su apellido',
			'lastname.max'			=> 'El apellido debe tener máximo 45 caracteres',
			'password.required'		=> 'Ingrese su contraseña',
			'password.min'			=> 'La contraseña debe tener minimo 4 caracteres',
			'email.required'		=> 'Ingrese su correo electronico',
			'email.email'			=> 'Ingrese un correo electronico válido',
			'email.unique'			=> 'Este correo electronico ya se encuentra registrado, por favor ingresa uno diferente',
			'location_id.required'	=> 'Por favor ingrese la institución educativa a la que pertenece',
			'location_id.exists'	=> 'Seleccione una institución de la lista'
		);

	/**
	 * Establece los atributos que se excluyen al mostrar los datos.
	 * @var array
	 */
	protected $hidden = array('password', 'updated_at');

	/**
	 * Get the unique identifier for the user.
	 * @return mixed
	 */
	public function getAuthIdentifier(){
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 * @return string
	 */
	public function getAuthPassword(){
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 * @return string
	 */
	public function getReminderEmail(){
		return $this->email;
	}

	/**
	 * Encripta la contraseña antes de ser almacenada en la bd.
	 * @param string $value
	**/
	public function setPasswordAttribute($value)  {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getRole(){
    	switch ($this->attributes['profile_id']) {
    	 	case '1':
    	 		return 'admin';
    	 		break;
    	 	case '2':
    	 		return 'researcher';
    	 		break;    	 		
    	 	default:
    	 		return 'public';
    	 		break;
    	 } ;
    }

    /**
	* Relacion uno a muchos entre areas y users
	* @return area de investigacion a la que pertenece
	**/
	public function area(){
		return  $this->belongsTo('Area');
	}

	/**
	* Relacion uno a muchos entre perfiles y users
	* @return perfil de usuario
	**/
	public function profile(){
		return  $this->belongsTo('Profile');
	}

	/**
	* Relacion uno a muchos entre typeids y users
	* @return tipo de identificación del usuario
	**/
	public function typeid(){
		return $this->belongsTo('Typeid');
	}

	/**
	* Relacion uno a muchos entre locations y users
	* @return la ubicacion del usuario
	**/
	public function location(){
		return  $this->belongsTo('Location');
	}

	/**
	* Relacion uno a muchos entre users y corpses
	* @return cadaveres registrados
	**/
	public function corpses(){
		return  $this->hasMany('Corpse');
	}

	/**
	* Relacion uno a muchos entre users y variationfounds
	* @return variaciones registrados
	**/
	public function variationfounds(){
		return  $this->hasMany('Variationfound')->take(15);
	}
}
