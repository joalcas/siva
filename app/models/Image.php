<?php

use LaravelBook\Ardent\Ardent;

class Image extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'images';


	/**
	 * Habilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = false;


	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('variationfound_id', 'path');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array(
		'variationfound_id'	=>'required|exists:variationfounds,id', 
		'path'				=>'required'
	);


	/**
	 * Establece los atributos que se excluyen al mostrar los datos.
	 * @var array
	 */
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');


	public function delete(){
		$folder = public_path() . "/uploads/" . $this->path;
		File::deleteDirectory($folder);
		$labels = $this->labels;
		foreach ($labels as $lbl) {
			$lbl->delete();
		}
		return parent::delete();
	}

	/**
	* Relacion uno a muchos entre variationfounds e images
	* @return reporte de variacion a la que pertenece
	**/
	public function variationfound(){
		return $this->belongsTo('Variationfound');
	}

	/**
	* Relacion uno a muchos entre images y labels
	* @return etiquetas de la imagen
	**/
	public function labels(){
		return $this->hasMany('Label');
	}
}