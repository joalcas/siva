<?php

use LaravelBook\Ardent\Ardent;

class Corpse extends Ardent {

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'corpses';


	/**
	 * Habilita una variable que en lugar de borrar el registro lo deja como inactivo.
	 * @var string
	 */
	protected $softDelete = true;


	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('name', 'lastname', 'internal_id', 'weight', 'height', 'age_years',
								'age_months', 'genre', 'body_mass_index', 'death_at', 'ethnicity_id',
								'num_death_certificate');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array(
		'num_death_certificate' =>'max:45|unique:corpses,num_death_certificate', 
		'death_at'				=>'date', 
		'location_id' 			=>'required|exists:locations,id', 
		'user_id' 				=>'required|exists:users,id',
		'internal_id' 			=>'required',
		'genre'					=>'required', 
		'age_years'				=>'required|numeric', 
		'age_months'			=>'numeric',
		'body_mass_index' 		=>'numeric', 
		'ethnicity_id'			=>'exists:ethnicity,id'
	);

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array( 
		'num_death_certificate.required'=>'Se debe ingresar el número del certificado de defunción', 
		'num_death_certificate.unique'	=>'Este número de certificado de defunción ya existe en el sistema', 
		'internal_id.required' 			=>'Por favor ingrese el número de identificación del cadaver en el anfiteatro', 
		'age.numeric'					=>'Recuerda que la edad debe ser numerica',
		'body_mass_index.numeric' 		=>'El indice de masa corporal debe ser numerico', 
		'genre.required'				=>'Indica el género',
		'age_years'						=>'La edad (años y meses) debe ser expresada en números',
		'age_months'					=>'La edad (años y meses) debe ser expresada en números'
	);

	/**
	 * Establece los atributos que se excluyen al mostrar los datos.
	 * @var array
	 */
	protected $hidden = array('updated_at', 'deleted_at');

	public function setDeath_atAttribute($value) {
		$this->date = strtotime($value);
	}

    /**
	 * Retorna los cadáveres que coinciden con el filtro ingresado.
	 * @var value
	 */
    public function getByFilter($value){
    	$query = "SELECT * FROM  corpses ".
				 "WHERE name LIKE '%" . $value . "%' Or lastname LIKE '%" . $value . "%' ".
				 "Or num_death_certificate LIKE '%" . $value . "%' Or internal_id LIKE '%" . 
				 $value . "%'";
		$result = DB::select($query)->paginate(20);
		return Response::json($result);
    }

    /**
	* Relacion uno a muchos entre corpses y locations
	* @return ubicacion
	**/
	public function location(){
		return $this->belongsTo('Location');
	}

	/**
	* Relacion uno a muchos entre corpses y users
	* @return usuario que lo registro
	**/
	public function user(){
		return $this->belongsTo('User');
	}

	/**
	* Relacion uno a muchos entre corpses y ethnicity
	* @return grupo etnico al que pertenece
	**/
	public function ethnicity(){
		return $this->belongsTo('Ethnicity');
	}


	/**
	* Relacion uno a muchos entre corpses y variationfounds
	* @return variaciones registradas
	**/
	public function variationfounds(){
		return $this->hasMany('Variationfound');
	}
}