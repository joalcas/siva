<?php

use LaravelBook\Ardent\Ardent;

class Variation extends Ardent {

	/*
	* Se desactivan los timestamps del framework
	*/
	public $timestamps = false;

	/**
	 * Nombre de la base de datos.
	 * @var string
	 */
	protected $table = 'variations';

	/**
	 * Indica que campos se pueden actualizar de manera "masiva".
	 * @var array
	 */
	protected $fillable = array('name', 'reference');

	/**
	 * Establece las reglas de validación para los campos de la tabla.
	 * @var array
	 */
	public static $rules = array('name' => 'required|min:3|max:200|unique:variations,name');

	/**
	 * Indica los mensajes de error que se muestran al aplicar las reglas de validación.
	 * @var array
	 */
	public static $customMessages = array( 'name' => 'Ingrese el nombre de la variación que desea registrar');

	/**
	* Elimina los registros de la tabla variations que no tiene ningun reporte relacionado
	**/
	public static function deleteUnused(){
		$query = "SELECT W.id  ".
    			 "FROM  variations As W  ".
				 "WHERE W.id not in (SELECT variation_id FROM variationfounds) ";
		$results = DB::select($query);
		
		foreach ($results as $res) {
			Variation::destroy($res->id);			
		}
	}

	/**
	* Relacion uno a muchos entre variations y founds
	* @return variaciones registradas
	**/
	public function founds(){
		return  $this->hasMany('Found');
	}
}