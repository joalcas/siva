<!DOCTYPE HTML>
<html lang="es" ng-app="siva">
<head>
	<meta charset="UTF-8">
	<title page-title>Siva</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	@section('styles')
		<link rel="stylesheet" href="/css/app.css">
	@show
</head>
<body page-class class="base">
<div class="notification"></div>
<header class="nav">
	<div class="nav-inner">
		<div class="pure-g-r">
			<div class="pure-u-1-2">
				<a ng-href="/" href="/" class="logo"> <img src="/img/siva.png" alt="Siva"></a>
				<div class="pure-menu pure-menu-open pure-menu-horizontal">
					@section('navigation')
						<ul>
						@if (Auth::check())
							@if (Auth::user()->getRole() === 'researcher')
								<li ng-class="{'pure-menu-selected': (location.path() == '/corpses') }">
									<a ng-href="/#/corpses" title="Cadáveres de la institución">Cadaveres</a>
								</li>
							@endif
							@if (Auth::user()->getRole() === 'admin')
								<li ng-class="{'pure-menu-selected':location.path() == '/users'}">
									<a ng-href="/#/users">Usuarios</a>
								</li>
								<li ng-class="{'pure-menu-selected':location.path() == '/corpses'}">
									<a ng-href="/#/corpses">Cadáveres</a>
								</li>
							@endif
						@endif
						<li ng-class="{'pure-menu-selected': (location.path() == '/founds') }">
							<a ng-href="/#/founds">Variaciones</a>
						</li>
						<li ng-class="{'pure-menu-selected': (location.path() == '/reports') }">
							<a ng-href="/#/reports">Reportes</a>
						</li>
						</ul>
					@show
				</div>
			</div>
			<div class="pure-u-1-2">
				<div class="pure-menu pure-menu-open pure-menu-horizontal" style="text-align:right;">
					<ul>
						@if (Auth::check())
				  			<li class="logout">
							  <a href="" class="dropdown-toggle">
							    <i class="icon-cog"></i> {{user.name}} {{user.lastname}}
							  </a>
							  <ul class="pure-menu pure-menu-open pure-menu-dropdown">
							  	<li><a ng-href="/#/profile">Mis datos</a></li>
							  	<li><a ng-href="/salir" href="/salir">Salir</a></li>
							  </ul>
							</li>
						@else
							<li><a ng-href="/registrarme" href="/registrarme">Crear cuenta</a></li>
							<li><a ng-href="/entrar" href="/entrar">Entrar</a></li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="pure-g-r content layout">
	@yield('content')
</div>
	@section('scripts')
	<script src="/js/libraries.js"></script>
	<script src="/js/modules.js"></script>

	<script type="text/ng-template" id="/template/dashboard/reports.html">
		@include('dashboard.reports')
	</script>

	<script type="text/ng-template" id="/template/dashboard/users.html">
		@include('dashboard.users')
	</script>
	<script type="text/ng-template" id="/template/dashboard/user-form.html">
		@include('dashboard.user-form')
	</script>
	<script type="text/ng-template" id="/template/dashboard/user-edit.html">
		@include('dashboard.user-edit')
	</script>
	<script type="text/ng-template" id="/template/dashboard/founds.html">
		@include('dashboard.founds')
	</script>
	<script type="text/ng-template" id="/template/dashboard/found-form.html">
		@include('dashboard.found-form')
	</script>
	<script type="text/ng-template" id="/template/dashboard/corpses.html">
		@include('dashboard.corpses')
	</script>
	<script type="text/ng-template" id="/template/dashboard/corpse-form.html">
		@include('dashboard.corpse-form')
	</script>
	<script type="text/ng-template" id="/template/dashboard/variation-new.html">
		<div class="pure-u-2-5"></div>
		<div class="pure-u-1-5">
			<h1 style="text-align:center;">Crear nueva variación</h1>
			<form name="variationEditor" ng-submit="save()" class="pure-form pure-form-stacked" novalidate>
				<label for="nomdvn" class="control-label">Nombre</label>
				<input type="text" id="nomdvn" ng-model="variation.name" placeholder="Nombre de la variación." class="pure-input-1" required>
				<p class="error" ng-show="errors.name">{{errors.name[0]}}</p>
				<br>
				<label for="refdvn" class="control-label">Referencias</label>
				<textarea id="refdvn" class="pure-input-1" ng-model="variation.reference" placeholder="URL o texto de referencia"></textarea>
				<p class="error" ng-show="errors.reference">{{errors.reference[0]}}</p>

				<button type="submit" class="pure-button pure-button-primary" ng-disabled="variationEditor.$invalid">Guardar</button>
			</form>
		</div>
	</script>
	<script type="text/ng-template" id="/template/home.html">
		<div id="home">
		<div class="image-background"></div>
			<div class="pure-u introduccion">
				<h1>Bienvenido a Siva</h1>
				<h2>Introducción</h2>
				<p>El estudio con cadáveres humanos le ofrece a estudiantes, docentes y profesionales del área de la salud la posibilidad de identificar y conocer a profundidad la anatomía normal de las estructuras y órganos, como también de las variaciones anatómicas que se puedan presentar. Este proceso enriquece su experiencia de aprendizaje al poder aplicar este conocimiento a los futuros pacientes y contribuye a disminuir las posibles iatrogenias por el aporte al conocimiento minucioso de las estructuras anatómicas y sus variaciones en la población colombiana, con el consecuente beneficio para la salud biológica, física, social, cultural y económica del paciente.</p>
				<p>Como en la actualidad son pocos los reportes encontrados sobre las variaciones anatómicas, surgió la necesidad de diseñar SIVA (Sistema de Informacion de Variaciones Anatómicas), que recolecta la  información sobre las variaciones anatómicas encontradas en muestras de la población colombiana, permitiendo a los docentes, estudiantes y profesionales de la salud a nivel nacional e internacional ejecutar una búsqueda ágil y especializada en lo referente a las variantes anatómicas, a sus descripciones tanto cualitativas como cuantitativas y a sus posibles prevalencias, ya sea con fines académicos, investigativos y/o clínicos.</p>
			</div>
		</div>
	</script>
	@show
</body>
</html>