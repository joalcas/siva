@extends('master')

@section('content')
	<div class="ng-view"></div>
@stop
@section('navigation')
	<ul>
		<li ng-class="{'pure-menu-selected':location.path() == '/users'}">
			<a ng-href="/dashboard#/users">Usuarios</a>
		</li>
		<li ng-class="{'pure-menu-selected': (location.path() == '/founds') }">
			<a ng-href="/dashboard#/founds">Variaciones</a>
		</li>
		<li ng-class="{'pure-menu-selected':location.path() == '/news'}">
			<a ng-href="/dashboard#/corpses">Cadáveres</a>
		</li>
		<li ng-class="{'pure-menu-selected': (location.path() == '/reports') }">
			<a ng-href="/dashboard#/reports">Reportes</a>
		</li>
	</ul>
@stop
@section('scripts')
	<script src="bower_components/jquery/jquery.min.js"></script>
	<script src="bower_components/angular/angular.min.js"></script>	
	<script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	<script src="bower_components/angular-resource/angular-resource.min.js"></script>

	<script src="js/services.js"></script>
	<script src="js/directives.js"></script>
	<script src="js/app.js"></script>
@stop