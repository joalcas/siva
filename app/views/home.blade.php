@extends('master')
@section('content')
	<div ng-view style="width:100%;"></div>
@stop

@section('scripts')
	@parent
	<script src="/js/app.js"></script>
@stop
</body>
</html>																				