@extends('master')
@section('content')
	<div class="ng-view pure-u-1"></div>
@stop

@section('scripts')
	@parent
	<script src="/js/admin.js"></script>
	<script>
		dashboard.run(['$rootScope', '$location', '$http', fn = function($rootScope, $location, $http) {
			$rootScope.user = {= json_encode(Auth::user()->toArray()) =};
			return $rootScope.location = $location;
		}]);
	</script>
@stop