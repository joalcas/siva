<div class="pure-u-1" id="list">
	<form class="pure-form filter-form box-inner" ng-submit="search()">
		<select id="afhom" ng-model="show" ng-init="show='all'" ng-options="o.value as o.label for o in showOptions" ng-change="search()">
			<option value="all">Todas</option>
			<option value="published">Publicadas</option>
			<option value="mine">Propias</option>
			<option value="pending">Pendientes</option>
		</select>
		<select ng-model="order" id="ofhom" ng-init="order='DESC'" ng-change="search()">
			<option value="">--Orden--</option>
			<option value="DESC">Recientes primero</option>
			<option value="ASC">Antiguos primero</option>
		</select>
		<input type="text" ng-model="term" placeholder="Buscar" class="pure-input-1">
		<div class="checkbox-list">
			<label ng-repeat="filterName in filters">
			  <input
			    type="checkbox"
			    name="selectedFilters[]"
			    value="{{filterName.value}}"
			    ng-checked="filterActive(filterName.value)"
			    ng-click="toggleSelection(filterName.value)">
			    {{filterName.label}}
			</label>
		</div>
		<a href="" class="pure-button pure-button-small" ng-click="removeFilters()">Quitar filtros</a>
	</form>
	<div  paginate options="pagination"></div>
	<div loader="loadingList"></div>
	<div class="pure-menu pure-menu-open" ng-hide="loadingList">
		<ul>
			<li bindonce ng-repeat="found in founds" ng-class="{'pure-menu-selected':found.id==found_id}">
				<a bo-href="'#/founds?area=detail&found=' + found.id">
					<strong bo-text="found.name"></strong> <br>
					<small> En <span bo-text="found.body_region"></span>, registrada en <span bo-text="found.created_at | svDate"></span> </small>
				</a>
			</li>
		</ul>
	</div>
</div>
<div class="pure-u-1" id="main">
	<div loader="loadingFound"></div>
	<div ng-switch on="area" ng-hide="loadingFound">
		<div ng-switch-when="detail">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1 title="Variación">{{found.variation.name}}</h1>
					<p class="subtitle">En {{found.body_region}} / {{found.structure.name}}</p>
				</div>
				<div class="pure-u-1-2 controls">
					<span ng-show="found.is_pending && found.owner" title"El reporte sera revisado por un administrador">Revisión pendiente</span>
					<a class="pure-button pure-button-success" ng-show="found.is_pending == false && found.owner" ng-click="publish()"href=""><i class="icon-share"></i> Publicar</a>
					<div class="uploader" ng-show="found.owner" ng-controller="ImagesCtrl" ng-init="found_id = found.id">
						<a title="Añadir imagen" href="" uploader="handleFiles" class="pure-button pure-button-primary" ng-disabled="disable" ng-hide="disable"><i class="icon-upload"></i> Añadir imagen</a>
					</div>
					<a title="publicar" ng-click="publish()" ng-show="found.is_pending == true && user.profile_id == '1'" href="" class="pure-button pure-button-success"> <i class="icon-ok-circle"></i> Publicar</a>
					<a title="rechazar" ng-click="reject()" ng-show="found.is_pending == true && user.profile_id == '1'" href="" class="pure-button pure-button-error"> <i class="icon-ban-circle"></i> Rechazar</a>
					<a title="modificar" ng-show="found.owner || user.profile_id == '1'" ng-href="#/founds?area=edit&found={{found.id}}" class="pure-button"> <i class="icon-pencil"></i></a>
					<a title="eliminar" ng-show="found.owner" ng-click="delete(found, $index)" class="pure-button"> <i class="icon-trash"></i></a>
				</div>
			</header>
			<article>
				<div class="pure-g-r">
					<div class="pure-u-1-2">
						<p ng-bind-html-unsafe="found.description | pre"></p>
						<h4 ng-show="found.images">Imágenes</h4>
						<div class="pure-u-g-r">
							<div class="pure-u thumbnail" ng-repeat="imagen in found.images">
								<img ng-click="openEditor(imagen)" ng-src="/uploads/{{imagen.path}}/thumb.jpg" alt="">
								<div class="delete" ng-show="found.owner" ng-controller="ImagesCtrl">
									<button title="Eliminar" class="pure-button" ng-click="remove(imagen, $index)"> <i class="icon-trash"></i> </button>
								</div>
							</div>
						</div>
					</div>
					<div class="pure-u-1-4">
						<div class="box-inner">
							<table class="">
								<tr ng-show="found.thickness"><th>Grosor:</th><td>{{found.thickness}}</td></tr>
								<tr ng-show="found.perimeter"><th>Perímetro:</th> <td>{{found.perimeter}}</td></tr>
								<tr ng-show="found.length"><th>Longitud:</th> <td>{{found.length}} </td></tr>
								<tr ng-show="found.form"><th>Forma:</th> <td>{{found.form}} </td></tr>
								<tr ng-show="found.diameter"><th>Diametro:</th> <td>{{found.diameter}}</td></tr>
								<tr ng-show="found.width"><th>Ancho:</th> <td>{{found.width}} </td></tr>
								<tr ng-show="found.laterality"><th>Lateralidad:</th> <td>{{found.laterality | laterality}} </td></tr>
							</table>
						</div>
					</div>
					<aside class="pure-u-1-4">
						<ul>
							<li class="heading">Variaciones relacionadas</li>
							<li bindonce ng-repeat="found in related">
								<a bo-href="'#/founds?area=detail&found='+found.id" bo-html="found.structure+': ' +found.name.toLowerCase()"></a>
							</li>
						</ul>
					</aside>
				</div>
				<div ng-controller="ImagesCtrl" ng-init="owner = found.owner">
					<div modal="editImage" close="closeEditor()" options="opts">
						<div class="modal-header">
							<div style="text-align:right;margin:5px;">
								<button class="pure-button pure-button-primary" ng-click="newTag($event)" ng-show="editarLabel && found.owner"> <i class="icon-plus"></i> Añadir etiqueta</button>
								<button class="pure-button" ng-click="closeEditor()"> <i class="icon-remove"></i> </button>
							</div>
						</div>

					    <div class="modal-body pure-g">
					    	<div class="pure-u-1">
								<div class="labels-container" ng-show="!found.owner">
									<div class="label" ng-repeat="tag in tags" style="top:{{tag.top}}px;left:{{tag.left}}px;width:{{tag.width}}px;height:{{tag.height}}px;">
										<div class="text">{{tag.description}}</div>
									</div>
								</div>
					    		<img ng-src="/uploads/{{image.path}}/full.jpg" alt="{{image.path}}" photo-tag tags="tags">
					    	</div>
					    </div>
					</div>
				</div>
			</article>
		</div>
		<div ng-switch-when="edit">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1>Modificar variación</h1>
					<p class="subtitle"><a ng-href="#/founds?area=detail&found={{found.id}}">{{found.variation.name}}</a> del cadáver <a ng-href="#/corpses?area=detail&corpse={{found.corpse.id}}">{{found.corpse.num_death_certificate}}</a> </p>
				</div>
				<div class="pure-u-1-2 controls">
					<button class="pure-button pure-button-primary" ng-disabled="foundEditor.$invalid" ng-click="update()" title="Guardar"> <i class="icon-ok"></i> Guardar</button>
					<button class="pure-button" ng-click="cancel()"> <i class="icon-remove"></i> Cancelar </button>
				</div>
			</header>
			<article ng-include="'/template/dashboard/found-form.html'"></article>
		</div>
		<div ng-switch-when="new">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1>Reportar variación</h1>
					<p class="subtitle">Reportar nueva variación del cadáver <a ng-href="#/corpses?area=detail&corpse={{corpse.id}}">{{corpse.num_death_certificate}}</a></p>
				</div>
				<div class="pure-u-1-2 controls">
					<button class="pure-button pure-button-primary" ng-disabled="foundEditor.$invalid"
					ng-click="create(variation)">
						<i class="icon-ok"></i> Guardar
					</button>
					<button class="pure-button" ng-click="cancel()"> <i class="icon-remove"></i> Cancelar </button>
				</div>
			</header>
			<article ng-include="'/template/dashboard/found-form.html'"></article>
			<footer>
			{{variation | json}}
			</footer>
		</div>
	</div>
</div>