<div class="pure-u-3-4" id="main">
	<div loader="loadingFound"></div>
	<div ng-switch on="area" ng-hide="loadingFound">
		<header class="pure-g">
			<div class="pure-u-1-2">
				<h1>Actualizar mis datos</h1>
			</div>
			<div class="pure-u-1-2 controls">
				<a title="Guardar" href="" ng-click="update()" class="pure-button pure-button-primary" ><i class="icon-ok"></i> Guardar</a>
				<a title="Cancelar" ng-click="cancel()" class="pure-button"> <i class="icon-remove"></i></a>
			</div>
		</header>
		<article>
			<div class="pure-u-1-3">
				<form name="nuevaCuenta" ng-submit="update()" class="pure-form pure-form-stacked pure-g" novalidate>
					<div ng-include="'/template/dashboard/user-form.html'"></div>
				</form>
				<br>
				<a href="/password/remind">Cambiar contraseña</a>
			</div>
		</article>
	</div>
</div>
