<div id="list" class="pure-u-1">
	<form name="queryForm" class="pure-form box-inner">
		<fieldset>
			<legend>Anatomía</legend>
			<div class="pure-input-grouped">
				<select ng-model="anatomic" ng-change="clearAnatomicFilter()" ng-options="field.value as field.label for field in options.anatomic">
				</select>
				<input type="text" ng-required="anatomic == 'variation'" ng-show="anatomic == 'variation'" ng-model="query.variation" placeholder="Variación" typeahead="var for var in variations | filter:$viewValue|limitTo:10">
				<select ng-required="anatomic == 'region'" ng-show="anatomic == 'region'" ng-model="query.body_region" ng-options="region for region in body_regions">
					<option value="">Region corporal</option>
				</select>
				<input type="text" ng-required="anatomic == 'structure'" ng-show="anatomic == 'structure'" ng-model="query.structure" placeholder="Estructura" typeahead="str for str in structures | filter:$viewValue|limitTo:10">
			</div>
		</fieldset>
		<fieldset>
			<legend>Geografía</legend>
			<div class="pure-input-grouped">
				<select ng-model="geographic" ng-change="clearGeographicFilter()" ng-options="field.value as field.label for field in options.geographic">
				</select>
				<input type="text" ng-show="geographic == 'city'"  ng-required="geographic == 'city'" ng-model="query.city" placeholder="Ciudad" typeahead="city.city for city in cities | filter:$viewValue|limitTo:10">
				<select ng-show="geographic == 'region'" ng-model="query.natural_region" ng-options="region for region in regions">
					<option value="">Region geográfica</option>
				</select>
			</div>
		</fieldset>
		<fieldset>
			<legend>Demografía</legend>
			<select class="pure-u-1" ng-model="query.sex">
				<option value="">-- Sexo --</option>
				<option value="F">Femenino</option>
				<option value="M">Masculino</option>
			</select>
			<select class="pure-u-1" ng-model="query.age_range" ng-options="age for age in age_ranges">
				<option value="">-- Rango de edad --</option>
			</select>
		</fieldset>
		<button class="pure-button pure-button-primary" ng-click="generate()" ng-disabled="queryForm.$invalid"> <i class="icon-bar-chart"></i> Generar</button>
	</form>
</div>
<div id="main" class="pure-u-1">
	<p class="error" ng-show="error">
		{{error}}
	</p>
	<div ng-repeat="report in report" class="report-container">
		<canvas pie="report" width="600" height="400"></canvas>
	</div>
</div>