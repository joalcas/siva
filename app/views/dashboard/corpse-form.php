<form name="formCorpse" ng-submit="save()" class="pure-form pure-form-stacked" novalidate>
	<fieldset>
		<legend>Identificación</legend>
		<div class="pure-g-r">
			<div class="pure-u-1-3">
				<label for="cdf" class="control-label">Certificado de Defunción</label>
				<input type="text" id="cdf" ng-model="corpse.num_death_certificate" placeholder="#" class="input-large">
				<p class="error" ng-show="errors.num_death_certificate">
					{{errors.num_death_certificate[0]}}
				</p>
			</div>
			<div class="pure-u-1-3">
				<label for="iidc" class="control-label">Número de identificación interno (*)</label>
				<input id="iidc" type="text" ng-readonly="area=='edit'" ng-model="corpse.internal_id" class="input-large" ng-required="true" title="Es el número de identificación del cadáver usado en el anfiteatro">
				<p class="error" ng-show="errors.internal_id">
					{{errors.internal_id[0]}}
				</p>
			</div>
			<div class="pure-u-1-3">
				<label for="fdd" class="control-label">Fecha de Defunción</label>
		        <input type="text" id="fdd" datepicker-popup="dd-MMMM-yyyy" show-weeks="false" ng-model="corpse.death_at"/>
				<p class="error" ng-show="errors.death_at">
					{{errors.death_at[0]}}
				</p>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Datos personales</legend>
		<div class="pure-g-r">
			<div class="pure-u-1-3">
				<label class="control-label" for="ndc">Nombres</label>
				<input id="ndc" type="text" ng-model="corpse.name" class="input-large" >
				<p class="error" ng-show="errors.name">
					{{errors.name[0]}}
				</p>
			</div>
			<div class="pure-u-1-3">
				<label class="control-label" for="adc">Apellidos</label>
				<input id="adc" type="text" ng-model="corpse.lastname" class="input-large">
				<p class="error" ng-show="errors.lastname">
					{{errors.lastname[0]}}
				</p>
			</div>
			<div class="pure-u-1-3">
				<label class="control-label">Género (*)</label>
				<label for="gdcm" class="radio">
					<input id="gdcm" type="radio" ng-model="corpse.genre" name="genero" value="M" ng-required="true">
					Masculino
				</label>
				<label for="gdcf" class="radio">
					<input id="gdcf" type="radio" ng-model="corpse.genre" name="genero" value="F">
					Femenino
				</label>
				<p class="error" ng-show="errors.genre">
					{{errors.genre[0]}}
				</p>
			</div>
		</div>
		<div class="pure-g-r">
			<div class="pure-u-1-3">
				<label for="egdc" class="control-label">Grupo etnico</label>
				<select id="egdc" ng-model="corpse.ethnicity_id" ng-options="e.id as e.description for e in ethnicities">
					<option value="">-- Seleccionar etnia --</option>
				</select>
				<p class="error" ng-show="errors.ethnicity_id">
					{{errors.ethnicity_id[0]}}
				</p>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Descripción física</legend>
		<div class="pure-g-r">
			<div class="pure-u-1-3 age">
				<label class="control-label" for="eac">Edad aproximada (*)</label>
				<input id="eac" type="text" placeholder="Años" class="pure-input-1-3" ng-model="corpse.age_years" ng-required="true">
				<input type="text" placeholder="meses" class="pure-input-1-3" ng-model="corpse.age_months">
				<p class="error" ng-show="errors.age_years">
					{{errors.age_years[0]}}
				</p>
				<p class="error" ng-show="errors.age_months">
					{{errors.age_months[0]}}
				</p>
			</div>
			<div class="pure-u-1-3">
				<label for="eadc" class="control-label">Altura (mts)</label>
				<input id="eadc" ng-model="corpse.height" type="text" class="input-large">
				<p class="error" ng-show="errors.height">
					{{errors.height[0]}}
				</p>
			</div>
			<div class="pure-u-1-3">
				<label for="padc" class="control-label">Peso (kg)</label>
				<input id="padc" ng-model="corpse.weight" type="text" class="input-large">
				<p class="error" ng-show="errors.weight">
					{{errors.weight[0]}}
				</p>
			</div>
		</div>
		<div class="pure-g-r">
			<div class="pure-u-1-3">
				<label for="padc" class="control-label">Indice de masa corporal</label>
				<input id="padc" ng-model="corpse.body_mass_index" type="text" class="input-large">
				<p class="error" ng-show="errors.body_maxx_index">
					{{errors.body_maxx_index[0]}}
				</p>
			</div>
		</div>
	</fieldset>
</form>