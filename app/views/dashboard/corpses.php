<div class="pure-u-1" id="list">
	<form class="pure-form pure-form-stacked filter-form box-inner" ng-submit="search()">
		<select ng-model="order" id="ofhom" ng-init="order='DESC'" ng-change="search()">
			<option value="">--Orden--</option>
			<option value="DESC">Recientes primero</option>
			<option value="ASC">Antiguos primero</option>
		</select>
		<input type="text" class="pure-input pure-input-1" placeholder="Buscar" ng-model="term">
	</form>
	<div class="pure-menu pure-menu-open">
		<ul>
			<li class="action" ng-class="{'pure-menu-selected':location.search().area == 'new'}">
				<a ng-href="#/corpses?area=new"><i class="icon-plus"></i> Añadir nuevo cadáver</a>
			</li>
		</ul>
	</div>
	<div  paginate options="pagination"></div>
	<div class="pure-menu pure-menu-open">
		<ul>
			<li bindonce ng-repeat="corpse in corpses" ng-class="{'pure-menu-selected':corpse.id==corpse_id}">
				<a bo-href="'#/corpses?area=detail&corpse='+corpse.id">
					<strong bo-text="corpse.fullname"></strong> <br>
					<small>Registrado: <span bo-text="corpse.created_at | svDate"></span></small>
				</a>
			</li>
		</ul>
	</div>
</div>
<div class="pure-u-1" id="main">
	<div ng-switch on="area">
		<div ng-switch-when="detail">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1 title="Certificado de defunción">{{corpse.name}} {{corpse.lastname}}</h1>
					<p class="subtitle">{{corpse.location.institution}} ({{corpse.location.city}})</p>
				</div>
				<div class="pure-u-1-2 controls">
					<a ng-href="#/founds?area=new&corpse={{corpse.id}}" class="pure-button pure-button-primary"> <i class="icon-plus"></i> Reportar variación </a>
					<a title="Modificar" ng-href="#/corpses?area=edit&corpse={{corpse.id}}" class="pure-button"> <i class="icon-pencil"></i> </a>
					<a title="Eliminar" href="" ng-click="delete(corpse,$index)" class="pure-button"> <i class="icon-trash"></i></a>
				</div>
			</header>
			<article>
				<div class="profile">
					<div class="pure-g-r">
						<div class="pure-u-1-2">
							<table>
								<tbody>
									<tr>
										<th>Edad aproximada</th> <td> <span ng-show="corpse.age_years">{{corpse.age_years}} años </span> <span ng-show="corpse.age_months">{{corpse.age_months}} Meses</span></td>
									</tr>
									<tr>
										<th>Etnia</th> <td>{{corpse.ethnicity.description}}</td>
									</tr>
									<tr>
										<th>Género</th> <td>{{corpse.genre | genre}}</td>
									</tr>
									<tr>
										<th>Estatura</th> <td>{{corpse.height}} Metros</td>
									</tr>
									<tr>
										<th>Peso</th> <td>{{corpse.weight}} Kilogramos</td>
									</tr>
									<tr>
										<th>Indice de masa corporal</th> <td>{{corpse.body_mass_index}}</td>
									</tr>
									<tr>
										<th>Certificado de defunción</th> <td>{{corpse.num_death_certificate}}</td>
									</tr>
									<tr>
										<th>Identificación interna</th> <td> {{corpse.internal_id}} </td>
									</tr>
									<tr>
										<th>Fecha de defunción</th> <td>{{corpse.death_at | date:'MMMM d, y'}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<hr>
					<div class="pure-g-r">
						<div class="pure-u-1-2">
							<div class="variations-list">
								<h2>Variaciones</h2>
								<div class="pure-menu pure-menu-open pure-menu-media">
									<ul>
										<li bindonce ng-repeat="found in corpse.variationfounds">
											<a bo-href="'#/founds?area=detail&found='+found.id">
												<strong bo-text="found.variation.name"></strong> - <span bo-text="found.structure.name"></span><br><em bo-text="found.description |truncate:80"></em>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
		<div ng-switch-when="edit">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1>Modificar cadáver</h1>
					<p class="subtitle">Modificar el cadáver <a ng-href="#/corpses?area=detail&corpse={{corpse.id}}">{{corpse.num_death_certificate}}</a></p>
				</div>
				<div class="pure-u-1-2 controls">
					<button class="pure-button pure-button-primary" ng-click="update()" ng-disabled="formCorpse.$invalid"><i class="icon-ok"></i> Guardar </button>
					<button class="pure-button" ng-click="cancel()"> <i class="icon-remove"></i> Cancelar </button>

				</div>
			</header>
			<article ng-include="'/template/dashboard/corpse-form.html'"></article>
		</div>
		<div ng-switch-when="new">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1>Registrar nuevo cadáver</h1>
					<p class="subtitle">Ingresa el cadáver perteneciente a la institución</p>
				</div>
				<div class="pure-u-1-2 controls">
					<button class="pure-button pure-button-primary" ng-click="create()" ng-disabled="formCorpse.$invalid">
						<i class="icon-ok"></i> Guardar
					</button>
					<button class="pure-button" ng-click="cancel()"> <i class="icon-remove"></i> Cancelar </button>

				</div>
			</header>
			<article ng-include="'/template/dashboard/corpse-form.html'"></article>
		</div>
	</div>
</div>