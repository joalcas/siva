<div class="pure-u-1" id="list">
	<form class="pure-form filter-form box-inner" ng-submit="search()">
		<select id="afhom" ng-model="filter" ng-init="filter='active'" ng-change="search()">
			<option value="new">Solicitudes</option>
			<option value="active">Activos</option>
			<option value="deleted">Borrados</option>
		</select>
		<select ng-model="order" id="ofhom" ng-init="order='DESC'" ng-change="search()">
			<option value="">--Orden--</option>
			<option value="DESC">Recientes primero</option>
			<option value="ASC">Antiguos primero</option>
		</select>
		<br>
		<br>
		<input type="text" ng-model="term" placeholder="Buscar" class="pure-input-1">
	</form>
<!-- user list -->
	<div class="pure-menu pure-menu-open">
		<ul>
			<li class="action" ng-class="{'pure-menu-selected':location.search().area == 'new'}">
				<a ng-href="#/users?area=new"><i class="icon-plus"></i> Añadir nuevo usuario</a>
			</li>
			<li bindonce ng-repeat="user in users" ng-class="{'pure-menu-selected':user.id==user_id}">
				<a ng-href="#/users?area=detail&user={{user.id}}">
					<strong bo-text="user.name + ' ' + user.lastname"></strong>
					<i ng-show="user.is_active == '0'" class="icon-asterisk"></i> <br>
					<small> Registrado el <span bo-text="user.created_at | svDate"></span> </small>
				</a>
			</li>
		</ul>
	</div>
</div>
<div class="pure-u-1" id="main">
<div ng-switch on="location.search().area">
<!-- detail -->
		<div ng-switch-when="detail">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1 title="Usuario">{{user.name}} {{user.lastname}}</h1>
					<p class="subtitle"> {{user.email}} - {{user.phone}} </p>
				</div>
				<div class="pure-u-1-2 controls">
					<div class="pure-form">
						<select ng-show="filter=='new'" name="" id="" ng-change="cambiarEstado(action)" ng-model="action" ng-options="o.id as o.description for o in userStatus">
							<option value="">-- Seleccione acction --</option>
						</select>
						<a ng-href="#/users?area=edit&user={{user.id}}" class="pure-button" title="Editar usuario"> <i class="icon-pencil"></i></a>
						<a href="" class="pure-button" ng-click="delete(user,$index)" title="Eliminar usuario"><i class="icon-trash"></i></a>
					</div>
				</div>
			</header>
			<article>
				<table>
					<tbody>
						<tr>
							<th>Permisos</th> <td>{{user.profile.description}}</td>
						</tr>
						<tr>
							<th>Area de investigación</th> <td>{{user.area.name}}</td>
						</tr>
						<tr>
							<th>Ubicación</th> <td>{{user.location.institution}}, {{user.location.city}}, {{user.location.country}}</td>
						</tr>
					</tbody>
				</table>
				<section>
					<h2>Reportes</h2>
					<table class="pure-table pure-table-striped">
						<thead>
							<tr>
								<th>Cadáver</th>
								<th>Estructura</th>
								<th>Descripción</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="found in user.variationfounds">
								<td>{{found.corpse.num_death_certificate}}</td>
								<td>{{found.structure.name}}</td>
								<td>{{found.description | truncate:90}}</td>
							</tr>
						</tbody>
					</table>
				</section>
			</article>
		</div>
		<div ng-switch-when="edit">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1>Modificar usuario</h1>
					<p class="subtitle">{{user.name}} {{user.lastname}}</p>
				</div>
				<div class="pure-u-1-2 controls">
					<button class="pure-button pure-button-primary" ng-disabled="editarUsuario.$invalid" ng-click="update()">
						<i class="icon-ok"></i> Guardar
					</button>
					<button class="pure-button" ng-click="cancel()"><i class="icon-remove"></i> Cancelar</button>
				</div>
			</header>
			<article>
				<div class="pure-u-1-3">
					<form
						name="editarUsuario"
						ng-submit="update(user)"
						class="pure-form pure-form-stacked pure-g-r"
						novalidate
						ng-include="'/template/dashboard/user-form.html'">
					</form>
				</div>
			</article>
		</div>
		<div ng-switch-when="new">
			<header class="pure-g">
				<div class="pure-u-1-2">
					<h1>Registrar nuevo usuario</h1>
					<p class="subtitle">Añadir nuevo usuario al sistema</p>
				</div>
				<div class="pure-u-1-2 controls">
					<button class="pure-button pure-button-primary" ng-disabled="editarUsuario.$invalid" ng-click="create()">
						<i class="icon-ok"></i> Registrar
					</button>
					<button class="pure-button" ng-click="cancel()"><i class="icon-remove"></i> Cancelar</button>
				</div>
			</header>
			<article>
				<div class="pure-u-1-3">
					<form
					name="editarUsuario"
					ng-submit="create()"
					class="pure-form pure-form-stacked pure-g-r"
					novalidate
					ng-include="'/template/dashboard/user-form.html'">
					</form>
				</div>
			</article>
		</div>
</div>