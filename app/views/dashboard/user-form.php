 		<div class="pure-u-1" ng-show="admin">
			<label for="perfuser">Perfil</label>
			<select id="perfuser" class="pure-input-1" ng-model="user.profile_id">
				<option value="">-- Seleccione un perfil --</option>
				<option value="1">Administrador</option>
				<option value="2">Investigador</option>
			</select>
		</div>
		<div class="pure-u-1-2">
			<label for="nuser">Nombres</label>
			<input id="nuser" class="pure-input-1" name="nombre" type="text" ng-model="user.name" required>
			<p class="error" ng-show="errors.name">{{errors.name[0]}}</p>
		</div>
		<div class="pure-u-1-2">
			<label for="lasuser">Apellidos</label>
			<input id="lasuser" class="pure-input-1" name="apellido" type="text" ng-model="user.lastname" required>
			<p class="error" ng-show="errors.lastname">{{errors.lastname[0]}}</p>
		</div>
		<div class="pure-u-1">
			<label for="emuser">Email</label>
			<input ng-disabled="disabled" 
			id="emuser" class="pure-input-1" name="email" type="text" ng-model="user.email" required>
			<p class="error" ng-show="errors.email">{{errors.email[0]}}</p>
		</div>
		<div class="pure-u-1">
			<label for="teluser">Teléfono</label>
			<input id="teluser" class="pure-input-1" name="phone" type="text" ng-model="user.phone" required>
			<p class="error" ng-show="errors.phone">{{errors.phone[0]}}</p>
		</div>
		<div class="pure-u-1-2">
			<label for="tipiduser">Tipo de identificación</label>
			<select  ng-disabled="disabled" id="tipiduser" class="pure-input-1" ng-model="user.typeid_id" required>
				<option value="" selected>-- Tipo de identificación --</option>
				<option value="1">Cédula de ciudadanía</option>
				<option value="2">Cédula de extranjería</option>
				<option value="3">Tarjeta de identidad</option>
				<option value="4">Pasaporte</option>
				<option value="5">NIT</option>
			</select>
			<p class="error" ng-show="errors.typeid_id">{{errors.typeid_id[0]}}</p>
		</div>
		<div class="pure-u-1-2">
			<label for="idnumuser">Nro. de identificación</label>
			<input ng-disabled="disabled" id="idnumuser" class="pure-input-1" name="identificacion" type="text" ng-model="user.id_num" required>
			<p class="error" ng-show="errors.id_num">{{errors.id_num[0]}}</p>
		</div>
		<div class="pure-u-1">
			<label for="univuser">Institución</label>
			<input ng-disabled="disabled" type="text" ng-model="user.location" class="pure-input-1"
			typeahead="i as i.institution +', '+i.city for i in locations | filter:$viewValue | limitTo:8">
			<p class="error" ng-show="errors.location_id">{{errors.location_id[0]}}</p>
		</div>
		<div class="pure-u-1">
			<label for="areauser">Area</label>
			<select ng-disabled="disabled" id="areauser"  class="pure-input-1" name="area" ng-model="user.area_id" ng-options="o.id as o.name for o in areas" required>
				<option value="">-- Area --</option>
			</select>
			<p class="error" ng-show="errors.area_id">{{errors.area_id[0]}}</p>
		</div>
		<div class="pure-u-1" ng-show="user.area_id == 0">
			<label for="newareauser">Nuevo area</label>
			<input ng-disabled="disabled" id="newareauser" class="pure-input-1" type="text" placeholder="Escriba el área de investigación" ng-model="user.otro_area" ng-required="user.area_id == 0">
			<p class="error" ng-show="errors.otro_area">{{errors.otro_area[0]}}</p>	
		</div>