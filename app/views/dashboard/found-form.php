<form name="foundEditor" ng-submit="" class="pure-form pure-u-3-4" novalidate>
	<div class="pure-g-r">
		<div class="pure-u-2-3">
			<input type="text" style="font-size:1.25em;" placeholder="Nombre" class="pure-input-1" ng-model="found.variation_obj" typeahead="var as var.name for var in variations | filter:$viewValue|limitTo:10" ng-disabled="location.search().area == 'edit' && user.profile_id == '2'" required>
			<p class="error" ng-show="errors.variation_id">{{errors.variation_id[0]}}</p>
			<p class="help-block">
				<small>O crea la variación, si aún no existe.<a ng-href="#/variations/new" class="btn"> <i class="icon-plus"></i> Nueva variación</a></small>
			</p>
			<div class="pure-g">
				<div class="pure-u-1-2">
					<select ng-disabled="!found.owner && location.search().area == 'edit'" class="pure-input-1" name="region" id="rdv" ng-model="found.body_region"
					ng-options="region for region in regions">
						<option value="">-- Región --</option>
					</select>
					<p class="error" ng-show="errors.body_region">{{errors.body_region[0]}}</p>
				</div>
				<div class="pure-u-1-2">
					<select ng-disabled="!found.owner && location.search().area == 'edit'" id="estrmdv" class="pure-input-1" required
					ng-options="structure for structure in structures"
					ng-model="found.structure_name">
						<option value="">--Elige la estructura--</option>
					</select>
					<p class="error" ng-show="errors.structure_name">{{errors.structure_name[0]}}</p>
				</div>
			</div>
			<textarea ng-disabled="!found.owner && location.search().area == 'edit'" id="ddv" required
			ng-model="found.description" placeholder="Descripción" class="pure-input-1"></textarea>
			<p class="error" ng-show="errors.description">{{errors.description[0]}}</p>
			<label class="control-label">Lateralidad *</label>
			<label for="izqdv" class="radio">
				<input ng-disabled="!found.owner && location.search().area == 'edit'" id="izqdv" type="radio" required
				ng-model="found.laterality" name="laterality" value="I">
				Izquierda
			</label>
			<label for="derdv" class="radio">
				<input ng-disabled="!found.owner && location.search().area == 'edit'" id="derdv" type="radio" ng-model="found.laterality" name="laterality" value="D">
				Derecha
			</label>
			<p class="error" ng-show="errors.laterality">{{errors.laterality[0]}}</p>
		</div>
		<div class="pure-u-1-3">
			<div class="box-inside">
				<div class="pure-input-grouped">
					<label for="formdv" class="control-label">Forma</label>
					<input ng-disabled="!found.owner && location.search().area == 'edit'" type="text" id="formdv" ng-model="found.form">
				</div>
				<p class="error" ng-show="errors.form">{{errors.form[0]}}</p>
				
				<div class="pure-input-grouped">
					<select ng-disabled="!found.owner && location.search().area == 'edit'" ng-model="field" ng-options="field.value as field.label for field in fields">
					</select>
					<input ng-disabled="!found.owner && location.search().area == 'edit'" type="text" id="diamdv" ng-show="field == 'diameter'" ng-model="found.diameter" placeholder="ej: 9.9">
					<input ng-disabled="!found.owner && location.search().area == 'edit'" type="text" id="diamdv" ng-show="field == 'perimeter'" ng-model="found.perimeter" placeholder="ej: 9.9">
				</div>
				
				<div class="pure-input-grouped">
					<label for="lengdv" class="control-label">Longitud</label>
					<input ng-disabled="!found.owner && location.search().area == 'edit'" type="text" id="lengdv" ng-model="found.length" placeholder="ej: 9.9">
				</div>
				<p class="error" ng-show="errors.length">{{errors.length[0]}}</p>

				<div class="pure-input-grouped">
					<label for="widv" class="control-label">Ancho</label>
					<input ng-disabled="!found.owner && location.search().area == 'edit'" type="text" id="widv" ng-model="found.width" placeholder="ej: 9.99">
				</div>
				<p class="error" ng-show="errors.width">{{errors.width[0]}}</p>

				<div class="pure-input-grouped">
					<label for="gdv" class="control-label">Grosor</label>
					<input ng-disabled="!found.owner && location.search().area == 'edit'" type="text" id="gdv" ng-model="found.thickness" placeholder="ej: 9.9">
				</div>
				<p class="error" ng-show="errors.thickness">{{errors.thickness[0]}}</p>
			</div>
		</div>
	</div>
</form>