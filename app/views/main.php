<!doctype html>
<html lang="en" ng-app="siva">
<head>
	<meta charset="UTF-8">
	<title page-title>Siva</title>
	<link rel="stylesheet" href="/css/screen.css">
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="/css/bootstrap-responsive.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,400italic' rel='stylesheet' type='text/css'>
</head>
<body page-class>
	<div class="fullscreen-box"></div>
	<div class="notification <?php echo (isset($message) ? 'show' : '') ;?>"><?php echo @$message;?></div>
		<header>
			<nav class="navbar">
				<div class="navbar-inner">
					<a ng-href="/" class="brand"><img src="/img/siva.png" alt="siva"></a>
					<ul class="nav">
						@yield('sidebar')
					</ul>
					<ul class="nav">
						<?php if (! Auth::guest()) {?>
							<li class="dropdown">
							  <a class="dropdown-toggle">
							    <?php echo @Auth::user()->name;?>
							  </a>
							  	<ul class="dropdown-menu">
							  		<li>
							  			<a ng-href="/salir">Salir</a>
							  		</li>
							  	</ul>
							</li>
						<?php } else {?>
							<li><a ng-href="/#/registrarme">Crear cuenta</a></li>
							<li><a ng-href="/entrar">Entrar</a></li>
						<?php } ?>
					</ul>
				</div>
			</nav>
		</header>
	<div class="ng-view"></div>
	<script src="js/vendor/angular.min.js"></script>
	<script src="js/vendor/ui-bootstrap.js"></script>
	<script src="js/vendor/angular.resource.min.js"></script>
	<script src="js/services.js"></script>
	<script src="js/directives.js"></script>
	<script src="js/app.js"></script>
</body>
</html>