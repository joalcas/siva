@extends('master')
@section('content')
<div class="pure-u-1" ng-controller="CuentaCtrl">
	<div style="text-align:center;" ng-show="location.search().sent">
		<h1>Gracias por registrarte en SIVA</h1>
		<p class="subtitle">Te informaremos mediante un correo electronico cuando tu solicitud sea aprobada.</p>
	</div>
	<div class="pure-u-3-8"></div>
	<div class="pure-u-1-4" ng-hide="location.search().sent">
		<h1 style="text-align:center;">Registrarme en Siva</h1>
		<form name="nuevaCuenta" ng-submit="create() " class="pure-form pure-form-stacked pure-g" novalidate>
			<div class="pure-u-1-2">
				<label for="nuser">Nombres</label>
				<input id="nuser" class="pure-input-1" name="nombre" type="text" ng-model="user.name" required>
				<p ng-class="{error:error.name}" ng-show="error.name">{{error.name[0]}}</p>
			</div>
			<div class="pure-u-1-2">
				<label for="lasuser">Apellidos</label>
				<input id="lasuser" class="pure-input-1" name="apellido" type="text" ng-model="user.lastname" required>
				<p ng-class="{error:error.lastname}" ng-show="error.lastname">{{error.lastname[0]}}</p>
			</div>
			<div class="pure-u-1">
				<label for="emuser">Email</label>
				<input id="emuser" class="pure-input-1" name="email" type="text" ng-model="user.email" required>
				<p ng-class="{error:error.email}" ng-show="error.email">{{error.email[0]}}</p>
			</div>
			<div class="pure-u-1">
				<label for="teluser">Teléfono</label>
				<input id="teluser" class="pure-input-1" name="phone" type="text" ng-model="user.phone" required>
				<p ng-class="{error:error.phone}" ng-show="error.phone">{{error.phone[0]}}</p>
			</div>
			<div class="pure-u-1-2">
				<label for="tipiduser">Tipo de identificación</label>
				<select  id="tipiduser" class="pure-input-1" ng-model="user.typeid_id" required>
					<option value="" selected>-- Tipo de identificación --</option>
					<option value="1">Cédula de ciudadanía</option>
					<option value="2">Cédula de extranjería</option>
					<option value="3">Tarjeta de identidad</option>
					<option value="4">Pasaporte</option>
					<option value="5">NIT</option>
				</select>
				<p ng-class="{error:error.typeid_id}" ng-show="error.typeid_id">{{error.typeid_id[0]}}</p>
			</div>
			<div class="pure-u-1-2">
				<label for="idnumuser">Nro. de identificación</label>
				<input id="idnumuser" class="pure-input-1" name="identificacion" type="text" ng-model="user.id_num" required>
				<p ng-class="{error:error.id_num}" ng-show="error.id_num">{{error.id_num[0]}}</p>
			</div>
			<div class="pure-u-1">
				<label for="univuser">Institución</label>
				<input type="text" ng-model="user.location" class="pure-input-1"
				typeahead="i as i.institution +', '+i.city for i in locations | filter:$viewValue | limitTo:8">
				<p ng-class="{error:error.location_id}" ng-show="error.location_id">{{error.location_id[0]}}</p>
			</div>
			<div class="pure-u-1">
				<label for="areauser">Area</label>
				<select id="areauser"  class="pure-input-1" name="area" ng-model="user.area_id" ng-options="o.id as o.name for o in areas" required>
					<option value="">-- Area --</option>
				</select>
				<p ng-class="{error:error.area_id}" ng-show="error.area_id">{{error.area_id[0]}}</p>
			</div>
			<div class="pure-u-1" ng-show="user.area_id == 0">
				<label for="newareauser">Nuevo area</label>
				<input id="newareauser" class="pure-input-1" type="text" placeholder="Escriba el área de investigación" ng-model="user.otro_area" ng-required="user.area_id == 0">
				<p ng-class="{error:error.otro_area}" ng-show="error.otro_area">{{error.otro_area[0]}}</p>	
			</div>
			<div class="pure-u-1">
				<button style="margin-top:1em;" type="submit" class="pure-button pure-button-primary" ng-disabled="nuevaCuenta.$invalid">Crear mi cuenta</button>
			</div>
		</form>
		<br>
	</div>
</div>
<div class="space-top"></div>
@stop
@section('scripts')
	@parent
	<script src="/js/services.js"></script>
	<script src="/js/directives.js"></script>
	<script src="/js/register.js"></script>
@stop