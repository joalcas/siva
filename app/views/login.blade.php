@extends('master')
@section('content')
	<div class="pure-u-1" style="text-align:center;">
		<h1>Iniciar sesión en siva</h1>
	</div>
	<div class="pure-u-2-5"></div>
	<div class="pure-u-1-5">
		<form name="login" action="/entrar" method="post" class="pure-form pure-form-stacked">
			<label for="elg" class="control-label">Email</label>
			<input id="elg" name="email" type="text" class="pure-input-1" placeholder="Email" ng-required="true">
			<label for="plg" class="control-label">Clave</label>
			<input id="plg" name="password" type="password" class="pure-input-1" placeholder="Clave" ng-required="true">
			@if (isset($error_login))
				<p class="error">{= $error_login =}</p>
			@endif
			<button class="pure-button pure-button-primary pure-input-1-2" type="submit" ng-disabled="login.$invalid">Entrar</button>
			<br>
			<a href="/password/remind">¿Olvidaste tu contraseña?</a>
		</form>
	</div>
@stop
@section('scripts')
	@parent
	<script src="/js/services.js"></script>
	<script src="/js/directives.js"></script>
	<script src="/js/app.js"></script>
@stop