@extends('master')
@section('content')
	<div class="pure-u-1" style="text-align:center;">
		<h1>Restablecimiento de contraseña</h1>
		@if (Session::has('error'))
		    {{ trans(Session::get('reason')) }}
		@elseif (Session::has('success'))
		    <p>
		    	Hemos enviado un correo con el link de restablecimiento a tu email.
		    </p>
		@endif
		<form name="login" action="/password/remind" method="post" class="pure-form">
			<input type="text" name="email">
			<button class="pure-button pure-button-primary" type="submit">Enviar recordatorio</button>
		</form>
	</div>
@stop
@section('scripts')
	@parent
	<script src="/js/app.js"></script>
	<script src="/js/directives.js"></script>
	<script src="/js/services.js"></script>
@stop