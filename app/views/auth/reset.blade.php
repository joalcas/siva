@extends('master')
@section('content')
	<div class="pure-u-1" style="text-align:center;">
		<h1>Restablecimiento de contraseña</h1>
		@if (Session::has('error'))
		    {= trans(Session::get('reason')) =}
		@endif
	</div>
	<div class="pure-u-2-5"></div>
	<div class="pure-u-1-5">
		<form action="" method="POST" class="pure-form pure-form-stacked">
			<input type="hidden" name="token" value="{= $token =}">

			<label for="email">Email</label>
			<input id="email" type="text" name="email">

			<label for="pass">Contraseña</label>
			<input id="pass" type="password" name="password">

			<label for="confirm">Confirme su contraseña</label>
			<input id="confirm" type="password" name="password_confirmation">

			<button type="submit" class="pure-button pure-button-primary">Guardar</button>
		</form>
	</div>
@stop