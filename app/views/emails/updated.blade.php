<!DOCTYPE html>
<html lang="es-CO">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table width="100%" cellpadding="0" cellspacing="0" border="0" table-layout="fixed" style="background-color:#ddd;">
			<tbody>
				<tr>
					<td align="center" style="overflow:hidden;">
						<div style="padding:1em;">
							<table width="660px" cellpadding="0" cellspacing="0" border="0" style="background-color:#fff; color:#565d64;font-family:'Open Sans', Helvetica, Arial, sans-serif;font-size:16px;">
								<tbody>
									<tr>
										<td>
											<div style="padding:0 1em;font-size:90%;">
												<h1 style="color:#3498DB;font-size:30px;font-weight:normal;">SIVA - Universidad del Valle</h1>
											</div>
											<div style="height:1px;background:#ddd;"></div>
											<div style="padding:0 1em;font-size:90%;">
												<h3 style="font-size:23px;font-weight:normal;">Actualización de datos</h3>
											</div>
											<div style="padding:0 1em;font-size:90%;">
												<p>
													Hola {= $name =} {= $lastname =}  <br />
												</p>
												<p>
													Tus datos han sido modificados, por favor revisa tu cuenta.
												</p>
											</div>
										</td>
									</tr>
								</tbody>	
							</table>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>