DROP SCHEMA IF EXISTS `siva` ;
CREATE SCHEMA IF NOT EXISTS `siva` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci ;
USE `siva` ;

-- -----------------------------------------------------
-- Table `siva`.`locations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`locations` ;

CREATE  TABLE IF NOT EXISTS `siva`.`locations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `region` VARCHAR(100) ,
  `department` VARCHAR(100) ,
  `city` VARCHAR(100) NOT NULL ,
  `institution` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`areas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`areas` ;

CREATE  TABLE IF NOT EXISTS `siva`.`areas` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(200) NOT NULL UNIQUE,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`profiles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`profiles` ;

CREATE  TABLE IF NOT EXISTS `siva`.`profiles` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(45) NOT NULL UNIQUE,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`typeids`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`typeids` ;

CREATE  TABLE IF NOT EXISTS `siva`.`typeids` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `type` VARCHAR(45) NOT NULL UNIQUE,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`users` ;

CREATE  TABLE IF NOT EXISTS `siva`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `id_num` VARCHAR(45) NOT NULL UNIQUE,
  `typeid_id` INT NOT NULL ,
  `profile_id` INT NULL DEFAULT 3,
  `password` VARCHAR(80) NOT NULL ,
  `location_id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `lastname` VARCHAR(45) NOT NULL ,
  `phone` VARCHAR(45) NOT NULL ,
  `area_id` INT NOT NULL ,
  `email` VARCHAR(100) NOT NULL UNIQUE,
  `is_new` TINYINT(1) NOT NULL DEFAULT 0,
  `created_at` DATETIME NOT NULL ,
  `updated_at` DATETIME NULL ,
  `deleted_at` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  FOREIGN KEY (`location_id` )
    REFERENCES `siva`.`locations` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`area_id` )
    REFERENCES `siva`.`areas` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`profile_id` )
    REFERENCES `siva`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`typeid_id` )
    REFERENCES `siva`.`typeids` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`password_reminders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`password_reminders` ;

CREATE TABLE IF NOT EXISTS `siva`.`password_reminders` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL ) 
ENGINE = InnoDB; 


-- -----------------------------------------------------
-- Table `siva`.`ethnicity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`ethnicity` ;

CREATE TABLE IF NOT EXISTS `siva`.`ethnicity` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `description` varchar(45) NOT NULL,
   PRIMARY KEY (`id`) ) 
ENGINE = InnoDB; 

-- -----------------------------------------------------
-- Table `siva`.`corpses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`corpses` ;

CREATE  TABLE IF NOT EXISTS `siva`.`corpses` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `num_death_certificate` VARCHAR(45) NULL UNIQUE,
  `location_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL DEFAULT 'NN' ,
  `lastname` VARCHAR(45) NOT NULL DEFAULT 'NN' ,
  `internal_id` VARCHAR(45) NOT NULL ,
  `height` DOUBLE NULL,
  `weight` DOUBLE NULL,
  `age_years` INT NOT NULL ,
  `age_months` INT NOT NULL DEFAULT 0,
  `genre` CHAR NOT NULL ,
  `ethnicity_id` INT NOT NULL ,
  `body_mass_index` DOUBLE NULL,
  `death_at` DATETIME NULL ,
  `age_range` VARCHAR(20) NULL ,
  `created_at` DATETIME NULL ,
  `updated_at` DATETIME NULL ,
  `deleted_at` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  FOREIGN KEY (`location_id` )
    REFERENCES `siva`.`locations` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`user_id` )
    REFERENCES `siva`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`ethnicity_id` )
    REFERENCES `siva`.`ethnicity` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`variations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`variations` ;

CREATE  TABLE IF NOT EXISTS `siva`.`variations` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(200) NOT NULL UNIQUE,
  `reference` VARCHAR(500) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`structures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`structures` ;

CREATE  TABLE IF NOT EXISTS `siva`.`structures` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(200) NOT NULL UNIQUE,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`varitationfounds`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`variationfounds` ;

CREATE  TABLE IF NOT EXISTS `siva`.`variationfounds` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `corpse_id` INT NOT NULL ,
  `variation_id` INT NOT NULL ,
  `structure_id` INT NOT NULL ,
  `body_region` VARCHAR(200) NULL ,
  `description` TEXT NULL ,
  `thickness` DOUBLE NULL DEFAULT 0.0 ,
  `diameter` DOUBLE NULL DEFAULT 0.0 ,
  `length` DOUBLE NULL DEFAULT 0.0 ,
  `perimeter` DOUBLE NULL DEFAULT 0.0 ,
  `width` DOUBLE NULL DEFAULT 0.0 ,
  `laterality` VARCHAR(10) NULL DEFAULT 'N/A' ,
  `form` VARCHAR(45) NULL DEFAULT 'N/A' ,
  `is_public` TINYINT(1) NULL DEFAULT false ,
  `created_at` DATETIME NULL ,
  `updated_at` DATETIME NULL ,
  `deleted_at` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  FOREIGN KEY (`user_id` )
    REFERENCES `siva`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`variation_id` )
    REFERENCES `siva`.`variations` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`corpse_id` )
    REFERENCES `siva`.`corpses` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  FOREIGN KEY (`structure_id` )
    REFERENCES `siva`.`structures` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  UNIQUE KEY (`corpse_id`, `variation_id`, `structure_id`)
    )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`images` ;

CREATE  TABLE IF NOT EXISTS `siva`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `variationfound_id` INT NOT NULL ,
  `path` VARCHAR(200) NOT NULL UNIQUE,
  `created_at` DATETIME NULL ,
  `updated_at` DATETIME NULL ,
  `deleted_at` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  FOREIGN KEY (`variationfound_id` )
    REFERENCES `siva`.`variationfounds` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`labels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`labels` ;

CREATE  TABLE IF NOT EXISTS `siva`.`labels` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `image_id` INT NOT NULL ,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `top` int(11) DEFAULT NULL,
  `left` int(11) DEFAULT NULL,
  `description` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  FOREIGN KEY (`image_id` )
    REFERENCES `siva`.`images` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `siva`.`password_reminders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `siva`.`password_reminders` ;

CREATE TABLE IF NOT EXISTS `siva`.`password_reminders` (
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB ;

-- -----------------------------------------------------
-- Data for table `siva`.`areas`
-- -----------------------------------------------------
INSERT INTO `siva`.`areas` (`id`, `name`) VALUES (1, 'Morfología');

-- -----------------------------------------------------
-- Data for table `siva`.`profiles`
-- -----------------------------------------------------
INSERT INTO `siva`.`profiles` (`id`, `description`) VALUES (1, 'Administrador');
INSERT INTO `siva`.`profiles` (`id`, `description`) VALUES (2, 'Investigador');
INSERT INTO `siva`.`profiles` (`id`, `description`) VALUES (3, 'Usuario publico');


-- -----------------------------------------------------
-- Data for table `siva`.`typeids`
-- -----------------------------------------------------
INSERT INTO `siva`.`typeids` (`id`, `type`) VALUES (1, 'Cédula de ciudadanía');
INSERT INTO `siva`.`typeids` (`id`, `type`) VALUES (2, 'Cédula de extranjería');
INSERT INTO `siva`.`typeids` (`id`, `type`) VALUES (3, 'Tarjeta de Identidad');
INSERT INTO `siva`.`typeids` (`id`, `type`) VALUES (4, 'Pasaporte');
INSERT INTO `siva`.`typeids` (`id`, `type`) VALUES (5, 'NIT');


-- -----------------------------------------------------
-- Data for table `siva`.`ethnicity`
-- -----------------------------------------------------
INSERT INTO `siva`.`ethnicity` (`id`, `description`) VALUES (1, 'Mestizo-Negroide');
INSERT INTO `siva`.`ethnicity` (`id`, `description`) VALUES (2, 'Mestizo-Mongoloide');
INSERT INTO `siva`.`ethnicity` (`id`, `description`) VALUES (3, 'Mestizo-Caucasoide');