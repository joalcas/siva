<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',function(){
	if (Auth::check()) {
		$rol = Auth::user()->getRole();
		if ('admin' == $rol) {
			return View::make('dashboard.admin');
		} elseif ('researcher' == $rol) {
			return View::make('dashboard.researcher');
		}
	}
	return View::make('home');
});

Route::get('/dashboard',array('before'=>'auth',function(){
}));

Route::get('password/remind', function(){
	return View::make('auth.remind');
});

Route::post('password/remind', function(){
	$credentials = array('email' => Input::get('email'));
	return Password::remind($credentials, function($message, $user){
		$message->subject('SIVA - Restablecimiento de contraseña');
	});
});

Route::get('password/reset/{token}', function($token){
	return View::make('auth.reset')->with('token', $token);
});

Route::post('password/reset/{token}', function(){
	$credentials = array('email' => Input::get('email'), 'password'=>Input::get('password'));
	return Password::reset($credentials, function($user, $password) {
		//Se modifican las reglas de validación para permitir la actualización del usuario.
		User::$rules['id_num'] = User::$rules['id_num'] . ',' . $user->id;
		User::$rules['email'] = 'required|email|unique:users,email,' . $user->id;
		$user->password = $password;
		if ($user->save()) {
			return Redirect::to('/entrar');
		}
		return Response::json($user->errors());
	});
});
Route::get('registrarme', function(){
	return View::make('register');
});
Route::get('entrar', function(){
	return View::make('login');
});
Route::post('entrar', function(){
	$credentials = Input::all();	
	if (Auth::attempt($credentials)) {
		$user = Auth::user();
		if($user->is_new){
			Auth::logout();
			return View::make('login')->with(array('error_login'=>'Su solicitud aun no ha sido respondida, comuniquese con el administrador'));
		}
		return Redirect::to('/');
	}
	$user = User::where('email', '=', $credentials['email'])->first();
	if(!$user){
		return View::make('login')->with(array('error_login'=>'El usuario no se encuentra registrado'));
	}
	return View::make('login')->with(array('error_login'=>'El usuario y la contraseña no coinciden'));
});
Route::get('salir', function(){
	Auth::logout();
	$message = array('message'=>'Sesion cerrada con éxito');
	return Redirect::to('/')->with($message);
});
Route::get('dashboard/{any?}', array('before'=>'auth',function(){
	$user = Auth::user();
	switch ($user->profile_id) {
		case '1':
			# Admin
			return View::make('dashboard.admin');
			break;
		case '2':
			# Researcher
			return View::make('dashboard.researcher');
			break;
		default:
			# code...
			return Redirect::to('/');
			break;
	}
}));

Route::resource('sesion', 'SesionController');
Route::resource('user', 'UserController');
Route::resource('corpse', 'CorpseController');
Route::resource('variation', 'VariationController');
Route::resource('ethnicity', 'EthnicityController');
Route::resource('structure', 'StructureController');
Route::resource('found', 'VariationFoundController');
Route::resource('location', 'LocationController');
Route::resource('area', 'AreaController');
Route::resource('image', 'ImageController');
Route::resource('label', 'LabelController');

Route::resource('rdf', 'RdfController');
Route::resource('report', 'ReportController');
