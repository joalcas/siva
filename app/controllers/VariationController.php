<?php

class VariationController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post', 'get')));

		//Solo los investigadores pueden registrar variaciones
		$this->beforeFilter('researcher', array('only' => array( 'save')));

		//Administrador e investigador pueden consultar, modificar y eliminar variaciones
		$this->beforeFilter('auth.medium', array('only' => array('index', 'update', 'destroy' )));
	}

	/**
	 * Muestra las variaciones registradas
	 * @return Response
	 */
	public function index()	{
		$variations= Variation::all();
		return Response::json($variations->toArray());
	}

	/**
	 * Almacena una nueva Variación en la BD
	 * @return estado:201 variación creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$variation = new Variation($data);

		if($variation->save()){
			return Response::json($variation->toArray(), 201);
		}
		return Response::json($variation->errors(), 400);
		
	}

	/**
	 * Muestra una Variación registrada
	 * @return Response
	 */
	public function show($id)	{
		$variation = Variation::find($id);
		if ($variation) {
			return Response::json($variation->toArray());
		}	
		return Response::json(array('message'=>'La variación no existe'), 400);
	}


	/**
	 * Actualiza una variación en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$variation = Variation::find($id);

		$data = Input::json()->all();
		if($variation){
			//Se modifican las reglas de validación para permitir la actualización de la variacion.
			Variation::$rules['name'] = Variation::$rules['name'] . ',' . $variation->id;

			$variation->fill($data);

			if($variation->save()){
				return Response::json($variation->toArray(), 200);
			}
			return Response::json($variation->errors(), 400);
		}
		return Response::json(array('message'=>'La variación no existe'), 400);
	}

	/**
	 * "Elimina" una variación de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$variation = Variation::find($id);
		if($variation){
			$founds_variation = $variation->founds->toArray();
			if(!$founds_variation){
				if($variation->delete()){
					return Response::json(array('message'=>'La variación fue eliminada'), 200);
				}
				return Response::json(array('message'=>'La variación no se pudo eliminar'), 400);
			}
			return Response::json(array('message'=>'La variación no existe'), 400);
		}
		return Response::json(array('message'=>'Esta variación contiene reportes relacionados'), 400);
	}
	
}