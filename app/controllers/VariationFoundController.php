	<?php

class VariationfoundController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post')));

		//Solo el investigador pude reportar variaciones
		$this->beforeFilter('researcher', array('only' => array('save')));

		//Administrador e investigador pueden consultar, modificar y eliminar reportes de variaciones
		$this->beforeFilter('auth.medium', array('only' => array('update', 'destroy')));
	}

	/**
	 * Muestra los reportes de variaciones registrados
	 * @return Response
	 */
	public function index()	{
		$data = Input::all();
		if (!isset($data['order'])) {
			$data['order'] = 'DESC';
		}

		//Si se quieren obtener datos para llenar los filtros de los reportes
		if (isset($data['report']) And $data['report'] == "true"){
			$result = array();
			$body_regions = DB::select('select distinct body_region As name from variationfounds');
			$result['body_regions'] = $this->aplanar($body_regions);
			
			$structures = DB::select('select distinct s.name from variationfounds as v, structures as s where s.id = v.structure_id');
			$result['structures'] = $this->aplanar($structures);

			$variation_names = DB::select('select distinct w.name from variationfounds as v, variations as w where w.id = v.structure_id');
			$result['variation_names'] = $this->aplanar($variation_names);

			return Response::json($result);
		}	


		//Si se quieren aplicar filtros o búsquedas en la interfaz de variaciones
		if (isset($data['search']) Or isset($data['filter']) Or isset($data['show']) ) {
			$result = $this->aplicarFiltros($data);
			return Response::json($result);
		}

		//Si se pretende obtener un listado de reportes de variaciones en calidad de investigador
		if (Auth::user() And Auth::user()->getRole() == 'researcher' And isset($data['propios'])) {
				$founds = Variationfound::join('variations', 'variationfounds.variation_id', '=', 'variations.id') 
				->join('structures', 'variationfounds.structure_id', '=', 'structures.id')
				->select(array('variationfounds.id','body_region', 'structures.name As structure', 'variations.name', 'description', 'variationfounds.created_at'))
				->where('user_id','=',Auth::user()->id);
		}
		//Si se pretende obtener un listado de reportes de variaciones en calidad de usuario público o de administrador
		else{
			$founds = Variationfound::join('variations', 'variationfounds.variation_id', '=', 'variations.id') 
			->join('structures', 'variationfounds.structure_id', '=', 'structures.id')
			->select(array('variationfounds.id','body_region', 'structures.name As structure', 'variations.name', 'description', 'variationfounds.created_at'));
		}
		
		$collection = $founds->orderBy('variationfounds.created_at', $data['order'])->paginate(15);
		$items = $collection->getCollection();
		$result = array(
			'last' => $collection->getLastPage(),
			'page' => $collection->getCurrentPage(),
			'perPage' => $collection->getPerPage(),
			'total' => $collection->getTotal(),
			'results' => $items->toArray()
		);
		return Response::json($result);
	}

	private function aplanar($array_in){
		$array = array();
		foreach ($array_in as $obj) {
			$array[] = $obj->name;
		}
		return $array;
	}

	/**
	* Devuelve un conjunto de reportes de variaciones que cumplen con los filtros o parámetros de búsqueda establecidos
	* @return array
	*/
	private function aplicarFiltros($data){
		$result = array();
		$founds = array();
		$founds = Variationfound::join('variations', 'variationfounds.variation_id', '=', 'variations.id') 
		->join('corpses', 'variationfounds.corpse_id', '=', 'corpses.id')
		->join('structures', 'variationfounds.structure_id', '=', 'structures.id')
		->join('locations', 'corpses.location_id', '=', 'locations.id')
		->select(array('variationfounds.id','body_region', 'structures.name As structure', 'variations.name', 'variationfounds.created_at'));
		if (isset($data['search'])) {
			$founds->where(function($query) use($data){
				$query->where('variations.name','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('structures.name','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('variationfounds.body_region','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('locations.city','LIKE', '%' . $data['search'] . '%');
				$query->orWhere('locations.institution','LIKE', '%' . $data['search'] . '%');
			});
		}
		if (isset($data['show'])) {
			switch ($data['show']) {
				case 'published':
					# code...
					$founds->where('is_public', '1');
					break;
				case 'mine':
					# code...
					if (Auth::check()) {
						$id = Auth::user()->id;
						$founds->where('variationfounds.user_id', $id);
					}
					break;
				case 'pending':
					# code...
					$founds->where('is_pending', '1');
					break;
				default:
					# code...
					break;
			}
		}
		if (isset($data['filter'])) {
			$filtros = explode(',', $data['filter']);
			foreach ($filtros as $filtro) {
				if ($filtro == 'form'){
					$founds->where($filtro,"!=", "N/A");
				} else {
					$founds->where($filtro,">", "0");
				}
			}
		}

		$collection = $founds->orderBy('variationfounds.created_at', $data['order'])->paginate(15);
		$items = $collection->getCollection();
		$result = array(
			'last' => $collection->getLastPage(),
			'page' => $collection->getCurrentPage(),
			'perPage' => $collection->getPerPage(),
			'total' => $collection->getTotal(),
			'results' => $items->toArray()
		);
		return $result;
	}	

	/**
	 * Muestra un reporte de variación
	 * @return Response
	 */
	public function show($id)	{
		$found = Variationfound::find($id);
		$result = array();
		if ($found) {
			$found->load('user','corpse','variation','structure','images');
			$result['related'] = $this->getRelated($found->structure->name, $id);
			if (Auth::check()) {
				if('admin' === Auth::user()->getRole()){
					$result['found'] = $found->toArray();
					return $result;
				}
				//Solo se pueden ver completamente las variaciones que registra un usuario o las que estan etiquetadas como públicas
				elseif($found->user_id === Auth::user()->id || $found->is_public){
					$result['found'] = $found->toArray();
					if ($found->user_id === Auth::user()->id ) {
						$result['owner'] = true;
					}
					return $result;
				}
			}
			//De las variaciones anatómicas solo se permiten ver la región corporal, la estructura y la descripción para los usuarios públicos
			$found_public = New Variationfound();
			$found_public->body_region = $found->body_region;
			$found_public->structure_id = $found->structure_id;
			$found_public->structure->toArray();
			$found_public->description = $found->description;
			$found_public->variation_id = $found->variation_id;
			$found_public->variation->toArray();
			$result['found'] = $found_public->toArray();

			return Response::json($result);
	
		}
		return Response::json(array('message'=>'La variación no existe'), 400);			
	}

	/**
	 * Almacena un nuevo reporte de variación en la BD
	 * @return estado:201 reporte creado | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$found = new Variationfound($data);
		$found->user_id = Auth::user()->id;
		$found->corpse_id = @$data['corpse_id'];
		$found->variation_id = @$data['variation_id'];

		$structure = Structure::where('name', @$data['structure_name'])->first();
		if($structure){
			$found->structure_id = $structure->id;
		}else{
			return Response::json(array('structure_name'=>array('La estructura no se encuentra en la base de datos')), 400);
		}

		//$corpse = $found->corpse();
		$corpse = Corpse::find($found->corpse_id);
		if (!$corpse) {
			return Response::json(array('message'=>'El cadáver no existe'), 404); 	
		}
		if ($corpse->location_id == Auth::user()->location_id) {
			if($found->save()){
				return Response::json($found->toArray(), 201);
			}
			return Response::json($found->errors(), 400);
		}
		return Response::json(array('message'=>'Solo puede reportar variaciones de cadaveres registrados en su ubicación'), 400); 	
	}
	
	/**
	 * Actualiza un reporte de variación en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$found = Variationfound::find($id);
		$data = Input::json()->all();

		if($found){
			if(Auth::user()->id === $found->user_id){
				$found->fill($data);
				$found->is_pending = @$data['is_pending'];

				$structure = Structure::where('name', @$data['structure_name'])->first();
				if($structure){
					$found->structure_id = $structure->id;
				}else{
					return Response::json(array('message' => 'La estructura no se encuentra en la base de datos'));
				}

				if($found->save()){

					return Response::json($found->toArray(), 200);
				}
				return Response::json($found->errors(), 400);

			} elseif (Auth::user()->getRole() =='admin') {
				if ($found) {
					$user = User::find($found->user_id);
					$found->variation_id = @$data['variation_id'];
					$found->is_public = @$data['is_public'];
					$found->is_pending = @$data['is_pending'];
					if ($found->save()) {
						if (isset($data['reject_message'])) {
							$data['found_id'] = $found->id;
							$data['user'] = $user->name;
							Mail::send('emails.reject_variation', $data, function($message) use($user, $data) {
								$message->from('teblami.siva@gmail.com', 'SIVA');
								$message->to($user->email);
								$message->subject("Publicación de variación #{$data['found_id']}");
							});
						}

						Variation::deleteUnused();
						return Response::json($found->toArray(), 200);
					}
					return Response::json($found->errors(), 400);
				}
			}
			return Response::json(array('message'=>'Solo puedes modificar tus reportes'), 400);
		}
		return Response::json(array('message'=>'El reporte no existe'), 400);
	}

	/**
	 * "Elimina" un reporte variación de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$found = Variationfound::find($id);
		if($found){
			if($found->delete()){
				return Response::json(array('message'=>'El reporte fue eliminado'), 200);
			}
			return Response::json(array('message'=>'El reporte no se pudo eliminar'), 400);
		}
		return Response::json(array('message'=>'El reporte no existe'), 400);
	}	


	private function getRelated($struct_name, $found_id){
		$structures = array();
		$rdf = new Rdf();

		$structures = $rdf->getByRegion($struct_name);
		$structures = array_slice($structures, 0, 20);
		$estructuras = "'" + join($structures, "', '") + "'";
		$sql = "SELECT F.id, V.name, S.name as structure FROM variationfounds AS F, structures AS S, variations AS V
		WHERE S.name in ({$estructuras}) AND F.deleted_at IS NULL and F.variation_id = V.id and F.id != {$found_id} and F.structure_id = S.id ORDER BY S.name LIMIT 20";
		$result = DB::select($sql);
		return $result;
	}
}
