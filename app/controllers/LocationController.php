<?php

class LocationController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post')));

		//El administrador se encarga de gestionar las instituciones
		$this->beforeFilter('admin', array('only' => array('save', 'update', 'destroy')));
	}

	/**
	 * Muestra las instituciones registradas
	 * @return Response
	 */
	public function index()	{
		$data = Input::all();
		if (isset($data['regions']) And $data['regions'] == "true"){
			$result = DB::select('select distinct region from locations order by region asc');
			return Response::json($result);
		}
		if (isset($data['departments']) And $data['departments'] == "true"){
			$result = DB::select('select distinct department from locations order by departments asc');
			return Response::json($result);
		}
		if (isset($data['cities']) And $data['cities'] == "true"){
			$result = DB::select('select distinct city from locations order by city asc');
			return Response::json($result);
		}
		if (isset($data['city']) ){
			$result = DB::select("select distinct institution, id from locations where city='" . $data['city'] . "' order by institution asc");
			return Response::json($result);
		}

		$locations= Location::all();
		return Response::json($locations->toArray());
	}

	/**
	 * Almacena una nueva instituciones en la BD
	 * @return estado:201 institucion creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$location = new Location($data);

		if($location->save()){
			return Response::json($location->toArray(), 201);
		}
		return Response::json($location->errors(), 400);		
	}

	/**
	 * Actualiza una institucion en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$location = Location::find($id);

		$data = Input::json()->all();
		if($location){
			$location->fill($data);

			if($location->save()){
				return Response::json($location->toArray(), 200);
			}
			return Response::json($location->errors(), 400);
		}
		return Response::json(array('message'=>'La institución no existe'), 400);
	}

	/**
	 * "Elimina" una institucion de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$location = Location::find($id);
		if($location){
			if($location->delete()){
				return Response::json(array('message'=>'La institución fue eliminada'), 200);
			}
			return Response::json(array('message'=>'La institución no se pudo eliminar'), 400);
		}
		return Response::json(array('message'=>'La institución no existe'), 400);
	}
	
}