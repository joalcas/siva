<?php

class AreaController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('admin', array('on' => array('put', 'delete', 'post')));
	}

	/**
	 * Muestra las areas registradas
	 * @return Response
	 */
	public function index()	{
		$areas= Area::all();
		return Response::json($areas->toArray());
	}

	/**
	 * Almacena una nueva area en la BD
	 * @return estado:201 area creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$area = new Area($data);

		if($area->save()){
			return Response::json($area->toArray(), 201);
		}
		return Response::json($area->errors(), 400);
	}

	/**
	 * Muestra un area registrada
	 * @return Response
	 */
	public function show($id){
		$area = Area::find($id);
		if ($area) {
			return Response::json($area->toArray());
		}	
		//Aqui cambie algo de prueba
		return Response::json(array('message'=>'El area no existe'), 400);
	}

	/**
	 * Actualiza un area en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$area = Area::find($id);

		$data = Input::json()->all();
		if($area){
			$area->fill($data);

			if($area->save()){
				return Response::json($area->toArray(), 200);
			}
			return Response::json($area->errors(), 400);
		}
		return Response::json(array('message'=>'El area no existe'), 400);
	}

	/**
	 * Elimina un area de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$area = Area::find($id);
		if($area){
			if($area->delete()){
				return Response::json(array('message'=>'El area fue eliminada'), 200);
			}
			return Response::json(array('message'=>'El area no se pudo eliminar'), 400);
		}
		return Response::json(array('message'=>'El area no existe'), 400);
	}	
}