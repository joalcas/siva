<?php

class EthnicityController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('admin', array('on' => array('put', 'delete', 'post')));
	}

	/**
	 * Muestra las etnias registradas
	 * @return Response
	 */
	public function index()	{
		$etnias= Ethnicity::all();
		return Response::json($etnias->toArray());
	}

	/**
	 * Almacena una nueva etnia en la BD
	 * @return estado:201 etnia creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$etnia = new Ethnicity($data);

		if($etnia->save()){
			return Response::json($etnia->toArray(), 201);
		}
		return Response::json($etnia->errors(), 400);
	}

	/**
	 * Muestra una etnia registrada
	 * @return Response
	 */
	public function show($id){
		$etnia = Ethnicity::find($id);
		if ($etnia) {
			return Response::json($etnia->toArray());
		}	
		return Response::json(array('message'=>'La etnia no existe'), 400);
	}

	/**
	 * Actualiza una etnia en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$etnia = Ethnicity::find($id);

		$data = Input::json()->all();
		if($etnia){
			$etnia->fill($data);

			if($etnia->save()){
				return Response::json($etnia->toArray(), 200);
			}
			return Response::json($etnia->errors(), 400);
		}
		return Response::json(array('message'=>'La etnia no existe'), 400);
	}

	/**
	 * Elimina una etnia de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$etnia = Ethnicity::find($id);
		if($etnia){
			if($etnia->delete()){
				return Response::json(array('message'=>'La etnia fue eliminada'), 200);
			}
			return Response::json(array('message'=>'La etnia no se pudo eliminar'), 400);
		}
		return Response::json(array('message'=>'La etnia no existe'), 400);
	}	
}