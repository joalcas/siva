<?php

class LabelController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post')));

		//Solo los investigadores pueden etiquetar imágenes y editar etiquetas
		$this->beforeFilter('researcher', array('only' => array( 'save', 'update')));

		//Administrador e investigador pueden consultar y eliminar etiquetas de imágenes
		$this->beforeFilter('auth.medium', array('only' => array('index', 'destroy' )));
	}

	/**
	 * Muestra las etiquetas registradas
	 * @return Response
	 */
	public function index()	{
		$params = Input::all();
		if (isset($params['image'])) {
			$labels= Label::where('image_id','=',$params['image'])->get();
		} else {
			$labels= Label::all();
		}
		return Response::json($labels->toArray());
	}

	/**
	 * Almacena una nueva etiqueta de imágen en la BD
	 * @return estado:201 etiqueta creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$label = new Label($data);

		$image = Image::find($data['image_id']);
		$found = $image->variationfound->toArray();

		if (Auth::user()->id != $found['user_id']) {
			return Response::json(array('message'=>'Solo puedes agregar imágenes a tus reportes de variación'), 400);
		}


		$label->image_id = $data['image_id'];
		if($label->save()){
			return Response::json($label->toArray(), 201);
		}
		return Response::json($label->errors(), 400);		
	}

	/**
	 * Muestra una etiqueta de una imagen
	 * @return Response
	 */
	public function show($id)	{
		$label = Corpse::find($id);

		if ($image) {
			$label->image->toArray();
			return Response::json($label->toArray());
		}	
		return Response::json(array('message'=>'La etiqueta no existe'), 400);
	}

	/**
	 * Actualiza una etiqueta en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$label = Label::find($id);
		$data = Input::json()->all();

		$found = $label->image->variationfound->toArray();

		if (Auth::user()->id != $found['user_id']) {
			return Response::json(array('message'=>'Solo puedes editar imágenes de tus reportes de variación'), 400);
		}

		$label->fill($data);

		if($label){
			if($label->save()){
				return Response::json($label->toArray(), 200);
			}
			return Response::json($label->errors(), 400);
		}
		return Response::json(array('message'=>'La etiqueta no existe'), 400);
	}

	/**
	 * "Elimina" una etiqueta de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$label = Label::find($id);
		if($label){
			$found = $label->image->variationfound->toArray();

			if (Auth::user()->id != $found['user_id']) {
				return Response::json(array('message'=>'Solo puedes editar imágenes de tus reportes de variación'), 400);
			}
			
			if($label->delete()){
				return Response::json(array('message'=>'La etiqueta fue eliminada'), 200);
			}
			return Response::json(array('message'=>'La etiqueta no se pudo eliminar'), 400);
		}
		return Response::json(array('message'=>'La etiqueta no existe'), 400);
	}
	
}