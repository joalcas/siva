<?php

class ImageController extends \BaseController {

	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post')));

		//Administrador e investigador pueden registrar, consultar y eliminar imagenes
		$this->beforeFilter('auth.medium', array('only' => array('store', 'index', 'destroy' )));
	}

	/**
	 * Almacena una nueva imagen de un reporte de variación en la BD
	 * @return estado:201 imagen creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::all();
		$image = new Image($data);
		$found = $image->variationfound->toArray();

		if (Auth::user()->id != $found['user_id']) {
			return Response::json(array('message'=>'Solo puedes agregar imágenes a tus reportes de variación'), 400);
		}

		$file = Input::file('file'); // your file upload input field in the form should be named 'file'
		$newfolder = str_random(8);
		$destinationPath = 'uploads/'.$newfolder;
		$filename = $file->getClientOriginalName();
		// $extension =$file->getClientOriginalExtension(); //if you need extension of the file
		$uploadSuccess = Input::file('file')->move($destinationPath, $filename);

		if ( $uploadSuccess ) {
			# reducir tamaño de la imagen
	        $im = new Imagick();
	        $im->readImage($destinationPath.'/'.$filename);
	        $im->setImageFormat('jpg');
        	$file_size = getimagesize($destinationPath.'/'.$filename);
	        if ( ($file_size[0] > 960) or ($file_size[1] > 650)) {
	            $im->adaptiveResizeImage(960,650, true);
	        }
	        # Guardamos la imagen postprocesada
	        $fullImage = $destinationPath.'/full.jpg';
	        $im->writeImage($fullImage);
	        unlink($destinationPath.'/'.$filename);
	        # crear miniatura
	        $thumb = new Imagick();
	        $thumb->readImage($fullImage);
	        $thumb->cropThumbnailImage('120', '120');
	        $thumb->writeImage($destinationPath.'/thumb.jpg');

			$image->path = $newfolder;
			if($image->save()){
				return Response::json($image->toArray(), 201);
			}
			return Response::json($image->errors(), 400);
		} else {
		   return Response::json('error', 400);
		}

	}

	/**
	 * Muestra una Imagen registrada
	 * @return Response
	 */
	public function show($id)	{
		$image = Image::find($id);
		$found = $image->variationfound->toArray();
		if (Auth::user()->id != $found['user_id'] Or !$found->is_public) {
			return Response::json(array('message'=>'Solo puedes ver imágenes de tus reportes de variaciones'), 404);
		}

		if ($image) {
			if ($image->labels()) {
				$image->labels->toArray();
			}
			return Response::json($image->toArray());
		}
		return Response::json(array('message'=>'La imagen no existe'), 400);
	}

	/**
	 * "Elimina" una imagen de un reporte variación de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$image = Image::find($id);
		$found = $image->variationfound->toArray();
		if (Auth::user()->id != $found['user_id']) {
			return Response::json(array('message'=>'Solo puedes eliminar imágenes de tus reportes de variaciones'), 404);
		}

		if($image){
			$labels_image = $image->labels;
			foreach ($labels_image as $lbl) {
				$lbl->delete();
			}
			if($image->delete()){
				return Response::json(array('message'=>'La imagen fue eliminada'), 200);
			}
			return Response::json(array('message'=>'La imagen no se pudo eliminar'), 400);
		}
		return Response::json(array('message'=>'La imagen no existe'), 404);
	}
}
