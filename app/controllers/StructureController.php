<?php

class StructureController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post', 'get')));

		//Administrador e investigador pueden controlar la tabla structures
		$this->beforeFilter('auth.medium', array('only' => array('index', 'save', 'update', 'destroy')));
	}

	/**
	 * Muestra las estructuras registradas
	 * @return Response
	 */
	public function index()	{
		$structures= Structure::all();
		return Response::json($structures->toArray());
	}

	/**
	 * Almacena una nueva estructura en la BD
	 * @return estado:201 estructura creada | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$structure = new Structure($data);

		if($structure->save()){
			return Response::json($structure->toArray(), 201);
		}
		return Response::json($structure->errors(), 400);
	}

	/**
	 * Muestra una estructura registrada
	 * @return Response
	 */
	public function show($id){
		$structure = Structure::find($id);
		if ($structure) {
			return Response::json($structure->toArray());
		}	
		return Response::json(array('message'=>'La estructura no existe'), 400);
	}

	/**
	 * Actualiza una estructura en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$structure = Structure::find($id);

		$data = Input::json()->all();
		if($structure){
			$structure->fill($data);

			if($structure->save()){
				return Response::json($structure->toArray(), 200);
			}
			return Response::json($structure->errors(), 400);
		}
		return Response::json(array('message'=>'La estructura no existe'), 400);
	}

	/**
	 * "Elimina" una estructura de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$structure = Structure::find($id);
		if($structure){
			$founds_structure = $structure->founds->toArray();
			if(!$founds_structure){
				if($structure->delete()){
					return Response::json(array('message'=>'La estructura fue eliminada'), 200);
				}
				return Response::json(array('message'=>'La estructura no se pudo eliminar'), 400);
			}
			return Response::json(array('message'=>'La estructura no existe'), 400);
		}
		return Response::json(array('message'=>'Esta estructura contiene reportes relacionados'), 400);
	}

}