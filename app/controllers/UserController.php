<?php

class UserController extends \BaseController {

	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete')));
		$this->beforeFilter('admin', array('only' => array('index', 'destroy')));
	}

	/**
	 * Muestra los usuarios registrados
	 * @return Response
	 */
	public function index()	{
		$data = Input::all();

		//Si se quieren aplicar filtros o búsquedas en la interfaz de variaciones
		if (!isset($data['order'])) {
			$data['order'] = 'DESC';
		}
		if (isset($data['search']) Or isset($data['filter']) ) {
			$result = $this->aplicarFiltros($data);
			return Response::json($result);
		}

		//Se devuelven los activos por defecto
		$data['filter'] = 'active';
		return Response::json($this->aplicarFiltros($data));
	}

	/**
	* Devuelve un conjunto de reportes de variaciones que cumplen con los filtros o parámetros de búsqueda establecidos
	* @return array
	*/
	private function aplicarFiltros($data){
		$result = array();
		$users = array();
		$users = User::join('locations', 'users.location_id', '=', 'locations.id')
		->select(array('users.id','users.name', 'users.lastname', 'users.created_at'));

		if (isset($data['search'])) {
			$users->where(function($query) use($data){
				$query->where('name','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('lastname','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('id_num','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('email','LIKE', "%" . $data['search'] . "%");
				$query->orWhere('locations.city','LIKE', '%' . $data['search'] . '%');
				$query->orWhere('locations.institution','LIKE', '%' . $data['search'] . '%');
			});

		}
		if (@$data['filter']){
			switch ($data['filter']) {
				case 'deleted':
					$users = $users->onlyTrashed();
					break;
				case 'new':
					$users = $users->where('is_new',"=", "1");
					break;
				default:
					$users = $users->where('is_new','0')->whereNull('deleted_at');
					break;
			}
		}
		$users = $users->orderBy('created_at', $data['order'])->paginate(15);
        $items = $users->getCollection();
		$result = array(
			'last' => $users->getLastPage(),
			'page' => $users->getCurrentPage(),
			'perPage' => $users->getPerPage(),
			'total' => $users->getTotal(),
			'results' => $items->toArray()
		);
        return $result;
	}

	/**
	 * Muestra un usuario con sus reportes
	 * @return Response
	 */
	public function show($id){
		$user = User::with('variationfounds', 'variationfounds.structure' , 'variationfounds.corpse', 'profile', 'location', 'area')
		->withTrashed()->find($id);
		if ($user) {
			return Response::json($user->toArray(), 200);
		}
		return Response::json(array('message'=>'Usuario no encontrado'), 404);
	}

	/**
	 * Almacena un Usuario nuevo en la BD
	 * @return estado:201 usuario creado | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$user = new User($data);
		$area = new Area();

		$user->id_num = $data['id_num'];
		$user->email = $data['email'];
		if (isset($data['otro_area'])) {
			$area->name = $data['otro_area'];
				$area = Area::create(array('name' => $data['otro_area']));
			$user->area_id = $area->id;
		}else{
			$user->area_id = $data['area_id'];
		}

		$user->location_id = $data['location_id'];
		$user->typeid_id = $data['typeid_id'];
		//Valida si hay una sesion abierta
		if(Auth::check()){
			//Si es administrador, obtiene los campos correspondientes y almacena el usuario
			if('admin' === Auth::user()->getRole()){
				$user->profile_id = $data['profile_id'];
				$user->password = $data['id_num'];
				$user->is_new = false;
				if($user->save()){
					//Se envía un correo al usuario
					Mail::send('emails.register', $data, function($message) use($user){
					    $message->from('teblami.siva@gmail.com', 'SIVA');
					    $message->to($user->email);
					    $message->subject('SIVA - Confirmación de registro');
					});					
					return Response::json($user->toArray(), 201);
				}
				$area->delete();
				return Response::json($user->errors(), 400);
			}
			//Si no es administrador muestra un error
			else{
				$user = null;
				return Response::json(array('message' => 'Solo el administrador puede registrar usuarios'), 401);
			}
		}
		//Si no hay una sesion abierta, el perfil de usuario se establece a público, solo el administrador podrá cambiarlo
		else{
			$user->profile_id = "3";
			$user->is_new = true;
			$user->password =  $data['id_num'];
			$tipeid_user = $user->typeid->toArray();
			$tmp_usr = $user->toArray();
			$tmp_usr['tipoid'] = $tipeid_user['type'];
			$tmp_usr['link'] = url('/').'#/users?area=detail&user='.$user->id;
			if($user->save()){				
				//Se envía un correo electronico al administrador
				Mail::send('emails.registered', $tmp_usr, function($message) {
				    $message->to('teblami.siva@gmail.com');
				    $message->subject('SIVA - Solicitud de activación');
				});
				return Response::json(array('message' => 'Pronto recibiras tu confirmación de registro'), 201);
			}
			$area->delete();
			return Response::json($user->errors(), 400);			
		}
	}

	/**
	 * Actualiza un usuario en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$user = User::find($id);
		$data = Input::json()->all();
		//Se modifican las reglas de validación para permitir la actualización del usuario.
		User::$rules['id_num'] = User::$rules['id_num'] . ',' . $user->id;
		User::$rules['email'] = 'required|email|unique:users,email,' . $user->id;

		$user->fill($data);

		if('admin' === Auth::user()->getRole()){
			if ($user->is_new) {
				if(!($user->password)){
					$user->password = $data['id_num'];
				}
				$user->is_new = false;				
				$user->profile_id = $data['profile_id'];				
				if($user->save()){
					//Se envía un correo al usuario
					$data['email'] = $user->email;
					$data['id_num'] = $user->id_num;
					Mail::send('emails.register', $data, function($message) use($user) {
					    $message->to($user->email);
					    $message->subject('SIVA - Confirmación de registro');
					});					
					return Response::json($user->toArray(), 201);
				}
				return Response::json($user->errors(), 400);
			}else{
				$user->is_new = false;				
				$user->profile_id = $data['profile_id'];				
				if($user->save()){
					if ($user->id == Auth::user()->id) {
						return Response::json($user->toArray(), 201);
					}else{
						//Se envía un correo al usuario
						$data['email'] = $user->email;
						$data['id_num'] = $user->id_num;
						Mail::send('emails.updated', $data, function($message) use($user) {
						    $message->to($user->email);
						    $message->subject('SIVA - Actualización de datos');
						});					
						return Response::json($user->toArray(), 201);
					}
				}
				return Response::json($user->errors(), 400);
			}
		}
		if ($user->id === Auth::user()->id) {
			if($user->save()){
				return Response::json($user->toArray(), 200);
			}
			return Response::json($user->errors(), 400);
		}
		return Response::json(array('message'=>'Acceso denegado'), 403);

	}

	/**
	 * "Elimina" un usuario de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		if('admin' === Auth::user()->getRole()){
			$user = User::find($id);
			if($user){
				if($user->delete()){
					return Response::json(array('message'=>'El usuario fue eliminado'), 200);
				}
				return Response::json(array('message'=>'El usuario no se pudo eliminar'), 400);
			}
			return Response::json(array('message'=>'El usuario no existe'), 400);
		}else{
			return Response::json(array('message' => 'Necesitas ser administrador'), 403);
		}
	}
}