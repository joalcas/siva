<?php


class ReportController extends \BaseController {

	/**
	* Toma los datos recibidos para crear la consulta SQL que dará origen al reporte.
	**/
	public function index(){
		$data = Input::all();
		$filter_anatomy = "";
		$anatomy = "";
		//Para crear reportes se debe definir inicialmente una variación, una región corporal o una estructura
        if (isset($data['variation'])) {
                $filter_anatomy = " And W.name = '" . $data['variation'] . "' ";
                $anatomy = "Variación: " . $data['variation'];
        }elseif (isset($data['structure'])) {
                $filter_anatomy = " And S.name = '" . $data['structure'] . "' ";
                $anatomy = "Estructura: " . $data['structure'];
        }elseif (isset($data['body_region'])){
                $filter_anatomy = " And V.body_region = '" . $data['body_region'] . "' ";
                $anatomy = "Región corporal: " . $data['body_region'];
        } else {
                return Response::json(array('message'=>'Debes definir un valor en los filtros anatómicos'), 400);
        }
       
        $report = $this->makeReport($data, $filter_anatomy, $anatomy);
        if (@$report['message']) {
                return Response::json($report, $report['code']);
        }
        return Response::json($report);
	}

	private function makeReport($data, $filter_anatomy, $anatomy){
		$filter_location = "";
		$location = "";
		//Se verifica que hayan definido un parámetro en los filtros de geografía
		if (!isset($data['city']) And !isset($data['natural_region'])) {
			return array('message'=>'Debes definir un valor en el parámetro Ciudad', 'code' => 400);
		}

		if (isset($data['city'])) {
			$filter_location = " And L.city = '" . $data['city'] ."' ";
			$location = "Ciudad: " . $data['city'];
		}
		elseif (isset($data['natural_region'])) {
			if ($data['natural_region'] == "Todas") {
				//Si el reporte incluye todas las regiones naturales del pais, se llama al metodo que los genera.
				return $this->makeReportByRegions($data, $filter_anatomy, $anatomy);
			}else{
				$filter_location = " And L.region = '" . $data['natural_region'] ."' ";
				$location = "Región natural: " . $data['natural_region'];
			}
		}

		return $this->makeReportWithLocation($data, $filter_anatomy, $anatomy, $filter_location, $location);
	}

	private function makeReportByRegions($data, $filter_anatomy, $anatomy){
		$result = array();
		$filter_location = "";
		$location = "";
		$regions = DB::select('SELECT DISTINCT L.region 
			FROM locations As L, variationfounds As V, corpses As C 
			WHERE V.corpse_id = C.id And C.location_id = L.id ORDER BY L.region ASC');

		foreach ($regions as $region) {
			$filter_location = " And L.region = '" . $region->region . "' ";
			$location = "Región natural: " . $region->region;
			$report = $this->makeReportWithLocation($data, $filter_anatomy, $anatomy, $filter_location, $location);
			if (!isset($report['message'])) {
				$result[$region->region] = 	$report[0];
			}
		}
		return $result;
	}

	private function makeReportWithLocation($data, $filter_anatomy, $anatomy, $filter_location, $location){
		$title = "";
		$labels = array();
		$data_in = array();

		$dataset = array();

		$filter_demography = "";

		$query = "";
		$select = " SELECT ";
		$from = " FROM corpses As C, structures As S, locations As L, variationfounds As V, variations As W ";
		$where = " WHERE (V.structure_id = S.id And V.variation_id = W.id And V.corpse_id = C.id And C.location_id = L.id) ";
		$group_by = " GROUP BY ";
		$order_by = " ORDER BY ";

		//Si el reporte incluye todos los datos demográficos
		if (!@$data['sex'] And !isset($data['age_range'])) {
			//se generan un arreglo de hombres y mujeres por rangos de edad
			$select .= " C.age_range, C.genre, COUNT(*) As n ";
			$group_by .= " C.age_range, C.genre ";
			$order_by .= " C.age_range ";

			$query = $select . $from . $where . $filter_anatomy . $filter_location . $group_by . $order_by;
			
			//Este arreglo contiene los rango de edad y géneros, con el conteo de cada uno
			$result_query = DB::select($query);
			if (count($result_query) == 0) {
				return array('message' => 'No hay datos que cumplan con los filtros establecidos', 'code' => 404);	
			}

			$query = " SELECT COUNT(*) As N " . $from . $where . $filter_anatomy . $filter_location ;
			
			//Este array contiene el total de registros de variaciones por sexo
			$N = DB::select($query);

			//Se calcula la prevalencia de cada dato
			foreach ($result_query as $res ) {
				$res->n = ( $res->n /  $N[0]->N ) * 100.00;
			}

			//Construyendo el reporte que se enviará a la interfaz gráfica
			foreach ($result_query as $data) {
				$labels[] = $data->age_range;				
			}
			$labels	= array_unique($labels);
			$labels = array_values($labels);
			$n_labels = count($labels);

			$hombres = array();
			$hombres = array_pad($hombres, $n_labels, 0);

			$mujeres = array();
			$mujeres = array_pad($mujeres, $n_labels, 0);

			for ($i=0; $i < $n_labels; $i++) { 
				$count_lbl = 0;

				foreach ($result_query as $data) {

					if ($labels[$i] == $data->age_range) {
						$count_lbl += 1;
						if ($data->genre == "F") {
							$mujeres[$i] = $data->n;
						}else{
							$hombres[$i] = $data->n;
						}
					}
					//----Condiciones de salida del ciclo que recorre todo el arreglo $result_query
					//Sale si ya encontro los datos de un mismo label (mujeres y hombres)
					if ($count_lbl == 2) {
						break 1;				
					}
				}
			}
			$title = "Prevalencia en hombres y mujeres por rangos de edad \n (" . $anatomy . " | " . $location .")";
			$data_in = array();
			$data_in["mujeres"] = array('title' => 'Mujeres', 'data' => $mujeres, 'fillColor'=>'#e74c3c');
			$data_in["hombres"] = array('title' => 'Hombres', 'data' => $hombres, 'fillColor'=>'#00f');
		}

		//Si el reporte incluye todos los rangos de edad de un sexo definido
		elseif (isset($data['sex'])  And !isset($data['age_range'])) {

			//se genera un arreglo por rangos de edad del sexo escogido
			$select .= " C.age_range, COUNT(*) As n ";
			$group_by .= " C.age_range ";
			$order_by .= " C.age_range ";
			$filter_demography = " And C.genre = '" . $data['sex'] . "' ";

			$query = $select . $from . $where . $filter_anatomy . $filter_location . $filter_demography . 
					 $group_by . $order_by;

			//Este arreglo contiene los rangos de edad con el conteo de cada uno
			$result_query = DB::select($query);
			if (count($result_query) == 0) {
				return array('message' => 'No hay datos que cumplan con los filtros establecidos', 'code' => 404);
			}

			$query = " SELECT COUNT(*) As N " . $from . $where . $filter_anatomy . 
					$filter_location . $filter_demography;
			
			//Este array contiene el total de registros de variaciones del sexo escogido
			$N = DB::select($query);

			//Se calcula la prevalencia de cada dato
			foreach ($result_query as $res ) {
				$res->n = ( $res->n /  $N[0]->N ) * 100.00;
			}

			//Construyendo el reporte que se enviará a la interfaz gráfica
			$mujeres = array();
			foreach ($result_query as $row) {
				$labels[] = $row->age_range;	
				if ($data['sex'] == 'F') {
					$mujeres[] = $row->n;
				}else{
					$hombres[] = $row->n;
				}			
			}
			$data_in = array();
			if ($data['sex'] == 'F') {
				$data_in["mujeres"] = array('title' => 'Mujeres', 'data' => $mujeres, 'fillColor'=>'#e74c3c');
				$title = "Prevalencia en mujeres por rangos de edad (" . $anatomy . " | " . $location .")";
			}else{
				$data_in["hombres"] = array('title' => 'Hombres', 'data' => $hombres, 'fillColor'=>'#00f');
				$title = "Prevalencia en hombres por rangos de edad (" . $anatomy . " | " . $location .")";
			}
		}

		//Si el reporte incluye ambos sexos con un rango de edad definido
		elseif (!isset($data['sex']) And isset($data['age_range'])) {
			//se genera un arreglo de hombres y mujeres en el rango de edad definido
			$select .= " C.genre, COUNT(*) As n ";
			$group_by .= " C.genre ";
			$order_by .= " C.genre ";
			$filter_demography = " And C.age_range = '" . $data['age_range'] ."' ";

			$query = $select . $from . $where . $filter_anatomy . $filter_location . $filter_demography . 
					 $group_by . $order_by;

			//Este arreglo contiene ambos sexos con el conteo de cada uno
			$result_query = DB::select($query);
			if (count($result_query) == 0) {
				return array('message' => 'No hay datos que cumplan con los filtros establecidos', 'code' => 404);
			}

			$query = " SELECT COUNT(*) As N " . $from . $where . $filter_anatomy . 
					$filter_location . $filter_demography . "GROUP BY C.age_range";
			//Este array contiene el total de registros de variaciones del rango de edad seleccionado
			$N = DB::select($query);

			//Se calcula la prevalencia de cada dato
			foreach ($result_query as $res ) {
				$res->n = ( $res->n /  $N[0]->N ) * 100.00;
			}

			//Construyendo el reporte que se enviará a la interfaz gráfica
			$labels[] = $data['age_range'];
			$hombres = array();
			$hombres = array_pad($hombres, 1, 0);

			$mujeres = array();
			$mujeres = array_pad($mujeres, 1, 0);
			foreach ($result_query as $row) {				
				if ($row->genre == 'F') {
					$mujeres[0] = $row->n;
				}else{
					$hombres[0] = $row->n;
				}			
			}
			$data_in = array();
			$data_in["mujeres"] = array('title' => 'Mujeres', 'data' => $mujeres, 'fillColor'=>'#e74c3c');
			$data_in["hombres"] = array('title' => 'Hombres', 'data' => $hombres, 'fillColor'=>'#00f');
			$title = "Prevalencia en hombres y mujeres (" . $anatomy . " | " . "Rango de edad: " . $data['age_range'] . " | " . $location .")";

		}else{
			return array('message'=>'Debes definir solo un valor en los filtros demográficos', 'code' => 400);
		}

		$dataset["title"] = $title;
		$dataset["labels"] = $labels;
		$dataset["datasets"] = $data_in;	
		return array($dataset);
	}
}