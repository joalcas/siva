<?php

class CorpseController extends \BaseController {
	
	public function __construct(){
		$this->beforeFilter('auth', array('on' => array('put', 'delete', 'post')));

		//Solo los investigadores pueden registrar la información de los cadaveres
		$this->beforeFilter('researcher', array('only' => array( 'save')));

		//Administrador e investigador pueden consultar, modificar y eliminar información de cadáveres
		$this->beforeFilter('auth.medium', array('only' => array('update', 'destroy' )));
	}

	/**
	 * Muestra los cadaveres registrados
	 * @return Response
	 */
	public function index()	{
		$data = Input::all();

		//Si se quieren obtener datos para llenar los filtros de los reportes
		if (@$data['report']){
			$result = array();
			$age_ranges = DB::select('select distinct age_range from corpses order by age_years asc');
			$result['age_ranges'] = $age_ranges;

			$locations = DB::select('select distinct l.city from variationfounds as v, corpses as c, locations as l where v.corpse_id = c.id and c.location_id = l.id');
			$result['locations'] = $locations;

			$regions = DB::select('select distinct l.region from variationfounds as v, corpses as c, locations as l where c.id = v.corpse_id and c.location_id = l.id');
			$result['regions'] = $regions;

			return Response::json($result);
		}

		//Opciones para usuarios identificados en el sistema
		if (Auth::user()) {
			//Si se quieren aplicar filtros o búsquedas en la interfaz de cadáveres
			if (!isset($data['order'])) {
				$data['order'] = 'DESC';
			}
			if (isset($data['search'])) {
				$result = $this->aplicarFiltros($data);
				return Response::json($result);
			}

			if('admin' === Auth::user()->getRole()){
				$corpses= Corpse::orderBy('created_at', $data['order'])->paginate(15);
			}else{
				//El investigador solo puede ver los cadaveres registrados por él y los que están en su misma ubicación
				$user = Auth::user();

				$corpses = Corpse::where('user_id',$user->id)
				->orWhere('location_id',$user->location_id)
				->orderBy('created_at', $data['order'])->paginate(15);
			}

			$items = $corpses->getCollection();
			$result = array(
					'last' => $corpses->getLastPage(),
					'page' => $corpses->getCurrentPage(),
					'perPage' => $corpses->getPerPage(),
					'total' => $corpses->getTotal(),
					'results' => $items->toArray()
				);
			return Response::json($result);
		}else{
			return Response::json(array('message'=>'Debes identificarte para tener acceso a la información de los cadáveres'), 404);
		}		
	}


	/**
	* Devuelve un conjunto de cadáveres que cumplen con los filtros o parámetros de búsqueda establecidos
	* @return array
	*/
	private function aplicarFiltros($data){
		$result = array();

		$corpses = Corpse::select(array('corpses.id','corpses.name', 'corpses.lastname', 'corpses.created_at'))
		->join('locations', 'corpses.location_id', '=', 'locations.id')
        -> where('num_death_certificate','LIKE', '%' . $data['search'] . '%')
		->orWhere('name','LIKE', "%" . $data['search'] . "%")
		->orWhere('lastname','LIKE', '%' . $data['search'] . '%')
		->orWhere('internal_id','LIKE', '%' . $data['search'] . '%')
		->orWhere('locations.city','LIKE', '%' . $data['search'] . '%')
		->orWhere('locations.institution','LIKE', '%' . $data['search'] . '%')
		->orderBy('created_at', $data['order'])->paginate(15);

        $items = $corpses->getCollection();
		$result = array(
			'last' => $corpses->getLastPage(),
			'page' => $corpses->getCurrentPage(),
			'perPage' => $corpses->getPerPage(),
			'total' => $corpses->getTotal(),
			'results' => $items->toArray()
		);

        return $result;
	}

	/**
	 * Muestra un Cadaver registrado
	 * @return Response
	 */
	public function show($id)	{
		$data = Input::all();
		$corpse = Corpse::with('variationfounds','variationfounds.variation','variationfounds.structure','ethnicity', 'location', 'user')->find($id);
		if ($corpse) {
			if('admin' === Auth::user()->getRole()){
				return Response::json($corpse->toArray());
			}else{
				//El investigador solo puede ver los cadaveres registrados en su ubicación o los registrados por él
				$user = Auth::user();
				if ( ($user->location_id == $corpse->location_id) Or ($user->id == $corpse->user->id) ) {
					if ($user->id == $corpse->user->id) {
						$corpse->owner = true;
					}
					return Response::json($corpse->toArray());
				}
				return Response::json(array('message'=>'Esta información no esta disponible'), 400);
			}
		}	
		return Response::json(array('message'=>'El cadaver no existe'), 400);		
	}


	/**
	 * Almacena un Cadaver nuevo en la BD
	 * @return estado:201 cadaver creado | 400: errores de validación | 500: error en el servidor
	 */
	public function store(){
		$data = Input::json()->all();
		$corpse = new Corpse($data);
		$corpse->num_death_certificate = @$data['num_death_certificate'];		

		//Obtiene el usuario
		$user = Auth::user();
		$corpse->location_id = $user->location_id;
		$corpse->user_id = $user->id;

		//Se establece el rango de edad
		if($corpse->age_years >= 0 And $corpse->age_years < 10){
			$corpse->age_range = "0 - 9";
		}elseif($corpse->age_years >= 10 And $corpse->age_years < 20){
			$corpse->age_range = "10 - 19";
		}elseif($corpse->age_years >= 20 And $corpse->age_years < 30){
			$corpse->age_range = "20 - 29";
		}elseif($corpse->age_years >= 30 And $corpse->age_years < 40){
			$corpse->age_range = "30 - 39";
		}elseif($corpse->age_years >= 40 And $corpse->age_years < 50){
			$corpse->age_range = "40 - 49";
		}elseif($corpse->age_years >= 50 And $corpse->age_years < 60){
			$corpse->age_range = "50 - 59";
		}elseif($corpse->age_years >= 60 And $corpse->age_years < 70){
			$corpse->age_range = "60 - 69";
		}elseif($corpse->age_years >= 70 And $corpse->age_years < 80){
			$corpse->age_range = "70 - 79";
		}elseif($corpse->age_years >= 80 And $corpse->age_years < 90){
			$corpse->age_range = "80 - 89";
		}elseif($corpse->age_years >= 90 And $corpse->age_years < 100){
			$corpse->age_range = "90 - 99";
		}elseif($corpse->age_years >= 100 And $corpse->age_years < 110){
			$corpse->age_range = "100 - 109";
		}elseif($corpse->age_years >= 110 And $corpse->age_years < 120){
			$corpse->age_range = "110 - 119";
		}elseif($corpse->age_years >= 120 And $corpse->age_years < 130){
			$corpse->age_range = "120 - 129";
		}

		if($corpse->save()){
			return Response::json($corpse->toArray(), 201);
		}
		return Response::json($corpse->errors(), 400);
	}

	/**
	 * Actualiza un cadaver en la BD
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
		$corpse = Corpse::find($id);
		if ($corpse) {
			if('researcher' === Auth::user()->getRole()){
				$location_user = Auth::user()->location_id;
				$location_corpse = $corpse->location_id;
				if ($location_user <> $location_corpse) {
					return Response::json(array('message'=>'No posee permisos para modificar este cadaver'), 400);
				}
			}
			$data = Input::json()->all();
			//Se modifican las reglas de validación para permitir la actualización del cadaver.
			Corpse::$rules['num_death_certificate'] = Corpse::$rules['num_death_certificate'] . ',' . $corpse->id;

			$corpse->fill($data);

			if($corpse->save()){
				return Response::json($corpse->toArray(), 200);
			}
			return Response::json($corpse->errors(), 400);
		}	
		return Response::json(array('message'=>'El cadaver no existe'), 400);	
	}

	/**
	 * "Elimina" un cadaver de la bd
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$corpse = Corpse::find($id);
		if($corpse){
			$founds = $corpse->variationfounds->toArray();
			if($founds){
				return Response::json(array('message'=>'Este cadáver tiene reportes relacionados y no puede eliminarse'), 400);
			}else{
				if($corpse->user_id == Auth::user()->id){
					if($corpse->delete()){
						return Response::json(array('message'=>'El cadaver fue eliminado'), 200);
					}else{
						return Response::json(array('message'=>'El cadaver no se pudo eliminar'), 400);
					}
				}else{
					return Response::json(array('message'=>'No posee permisos para modificar este cadaver'), 400);
				}			
			}
		}
		return Response::json(array('message'=>'El cadaver no existe'), 400);
	}	
}