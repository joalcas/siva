<?php


class RdfController extends \BaseController {

	/**
	 * Muestra la lista de nombres de las estructuras 
	 * que se relaciona con la estructura $struct
	 * @return Response, array con la lista de nombres. Vacío si no encuentra la estructura
	 */
	public function show($struct){
		$data = Input::all();
		$result = array();
		$rdf = new Rdf();
		
		if (isset($data['relationship'])){
			switch ($data['relationship']) {
				//En este caso se buscan las estructuras relacionadas como "hermanos" de la estructura actual con sus respectivos subarboles
				case 'level':
					$result = $rdf->getByLevel($struct);
					break;
				
				case 'region':
					$result = $rdf->getByRegion($struct);
					break;

				case 'name':
					$result = $rdf->getByName($struct);
					break;

				case 'substructures':
					$result = $rdf->getSubstructures($struct);
					break;
			}

		}else{
			$result = $rdf->getSubstructures($struct);	
		}

		return Response::json($result);		
	}

	
	
}	
	